﻿namespace Beast.Encryption {
	public class EncryptionServiceConfig
	{
		public string AppApiKey { get; set; }
		public string Partition { get; set; }
		public string AsRole { get; set; }
	}
}