﻿using System.IO;
using System.Threading.Tasks;

namespace Beast.Encryption
{
	public interface IEncryptionService
	{
		Task<T> Encrypt<T>(T toEncrypt);
		Task<T> Decrypt<T>(T toDecrypt);
		Task<string> GetEncryptedSearchPrefix(string toEncrypt);
		Task EncryptStream(Stream toEncrypt, Stream encryptedStreamOut);
		Task DecryptStream(Stream toDecrypt, Stream decryptedStreamOut);

		Task EncryptStream(Stream toEncrypt, Stream encryptedStreamOut, bool compressOutStream);
	}
}
