﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Crypteron;
using Crypteron.CipherObject;
using Crypteron.CipherStor;

namespace Beast.Encryption
{
	public class EncryptionService : IEncryptionService
	{
		private readonly CipherStorClient _CipherStor;
		private static readonly ReaderWriterLockSlim _Lock;

		static EncryptionService()
		{
			_Lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
		}

		public EncryptionService(EncryptionServiceConfig config)
		{
			if (config == null)
			{
				throw new ArgumentNullException(nameof(config));
			}

			if (string.IsNullOrWhiteSpace(config.AppApiKey))
			{
				throw new ArgumentException("AppApiKey cannot be be null, empty or whitespace", nameof(config));
			}

			_CipherStor = new CipherStorClient(config.Partition ?? "", config.AsRole ?? "default");

			_Lock.EnterWriteLock();

			try
			{
				CrypteronConfig.Config.MyCrypteronAccount.AppSecret = config.AppApiKey;
			}
			finally
			{
				_Lock.ExitWriteLock();
			}
		}

		public async Task<T> Encrypt<T>(T toEncrypt)
		{
			return await Task.FromResult(toEncrypt.Seal());
		}

		public async Task<T> Decrypt<T>(T toDecrypt)
		{
			return await Task.FromResult(toDecrypt.Unseal());
		}

		public async Task<string> GetEncryptedSearchPrefix(string toEncrypt)
		{
			return await Task.FromResult(SecureSearch.GetPrefix(toEncrypt));
		}

		public async Task EncryptStream(Stream toEncrypt, Stream encryptedStreamOut)
		{
			await EncryptStream(toEncrypt, encryptedStreamOut, false);
		}

		public async Task EncryptStream(Stream toEncrypt, Stream encryptedStreamOut, bool compressOutStream)
		{
			_CipherStor.EncryptStream(toEncrypt, encryptedStreamOut);

			await Task.FromResult(0);
		}

		public async Task DecryptStream(Stream toDecrypt, Stream decryptedStreamOut)
		{
			_CipherStor.DecryptStream(toDecrypt, decryptedStreamOut);

			await Task.FromResult(0);
		}
	}
}