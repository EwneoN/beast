﻿using Crypteron;

namespace Beast.Encryption
{
	public class EncryptedAttribute : SecureAttribute
	{
		public EncryptedAttribute(string mask = null) : base(Opt.None, mask) { }
	}
}
