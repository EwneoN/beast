﻿using Crypteron;

namespace Beast.Encryption
{ 
	public class EncryptedSearchableAttribute : SecureAttribute
	{
		public EncryptedSearchableAttribute(string mask = null) : base(Opt.Search, mask) { }
	}
}