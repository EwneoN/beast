﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using Beast.EntityFramework;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework.ModelConfigurations {
	public class GroupConfiguration : EntityConfiguration<Group>
	{
		public GroupConfiguration()
			: base(DefaultSchema)
		{
			ToTable($"{Schema}.{nameof(Group)}");

			HasKey(u => u.Id);

			Property(u => u.Name)
				.IsRequired()
				.HasMaxLength(512)
				.HasColumnAnnotation(IndexAnnotation.AnnotationName,
				                     new IndexAnnotation(new IndexAttribute($"IX_{nameof(Group)}_Name") { IsUnique = true }));

			HasMany(u => u.Claims)
				.WithRequired(uc => uc.Group)
				.HasForeignKey(uc => uc.GroupId);

			HasMany(u => u.Users)
				.WithRequired(uc => uc.Group)
				.HasForeignKey(uc => uc.GroupId);
		}
	}
}