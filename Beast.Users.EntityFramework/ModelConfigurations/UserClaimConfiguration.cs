﻿using Beast.EntityFramework;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework.ModelConfigurations
{
	public class UserClaimConfiguration : EntityConfiguration<UserClaim>
	{
		public UserClaimConfiguration()
			: base(DefaultSchema)
		{
			ToTable($"{Schema}.{nameof(UserClaim)}");

			HasKey(uc => uc.Id);

			HasRequired(uc => uc.User)
				.WithMany(u => u.Claims)
				.HasForeignKey(uc => uc.UserId);
			HasRequired(uc => uc.Claim)
				.WithMany(u => u.UserClaims)
				.HasForeignKey(uc => uc.ClaimId);

			HasIndex(uc => uc.UserId);
			HasIndex(uc => uc.ClaimId);

			HasIndex(uc => new { uc.ClaimId, uc.UserId })
				.IsUnique();
		}
	}
}