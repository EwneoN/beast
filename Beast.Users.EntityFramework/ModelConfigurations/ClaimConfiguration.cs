﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using Beast.EntityFramework;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework.ModelConfigurations
{
	public class ClaimConfiguration : EntityConfiguration<Claim>
	{
		public ClaimConfiguration()
			: base(DefaultSchema)
		{
			ToTable($"{Schema}.{nameof(Claim)}");

			HasKey(c => c.Id);

			Property(c => c.Name)
				.IsRequired()
				.HasMaxLength(256)
				.HasColumnAnnotation(IndexAnnotation.AnnotationName,
				                     new IndexAnnotation(new IndexAttribute($"IX_{nameof(Group)}_Name") { IsUnique = true }));

			HasMany(c => c.UserClaims)
				.WithRequired(uc => uc.Claim)
				.HasForeignKey(uc => uc.ClaimId);
		}
	}
}