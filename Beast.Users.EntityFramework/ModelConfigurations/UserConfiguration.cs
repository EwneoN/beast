﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using Beast.EntityFramework;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework.ModelConfigurations
{
	public class UserConfiguration : EntityConfiguration<User>
	{
		public UserConfiguration()
			: base(DefaultSchema)
		{
			ToTable($"{Schema}.{nameof(User)}");

			HasKey(u => u.Id);

			Property(u => u.Username)
				.IsRequired()
				.HasMaxLength(64)
				.HasColumnAnnotation(IndexAnnotation.AnnotationName,
				                     new IndexAnnotation(new IndexAttribute($"IX_{nameof(User)}_Username") { IsUnique = true }));
			Property(u => u.DisplayName)
				.HasMaxLength(512)
				.IsRequired();
			Property(u => u.EmailAddress)
				.IsRequired()
				.HasMaxLength(256)
				.HasColumnAnnotation(IndexAnnotation.AnnotationName,
				                     new IndexAnnotation(new IndexAttribute($"IX_{nameof(User)}_EmailAddress") { IsUnique = true }));

			HasMany(u => u.Claims)
				.WithRequired(uc => uc.User)
				.HasForeignKey(uc => uc.UserId);

			HasMany(u => u.Groups)
				.WithRequired(uc => uc.User)
				.HasForeignKey(uc => uc.UserId);
		}
	}
}