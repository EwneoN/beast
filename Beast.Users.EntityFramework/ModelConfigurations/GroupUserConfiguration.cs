﻿using Beast.EntityFramework;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework.ModelConfigurations {
	public class GroupUserConfiguration : EntityConfiguration<GroupUser>
	{
		public GroupUserConfiguration()
			: base(DefaultSchema)
		{
			ToTable($"{Schema}.{nameof(GroupUser)}");

			HasKey(uc => uc.Id);

			HasRequired(uc => uc.Group)
				.WithMany(u => u.Users)
				.HasForeignKey(uc => uc.GroupId);
			HasRequired(uc => uc.User)
				.WithMany(u => u.Groups)
				.HasForeignKey(uc => uc.UserId);

			HasIndex(uc => uc.GroupId);
			HasIndex(uc => uc.UserId);

			HasIndex(uc => new { uc.UserId, uc.GroupId })
				.IsUnique();
		}
	}
}