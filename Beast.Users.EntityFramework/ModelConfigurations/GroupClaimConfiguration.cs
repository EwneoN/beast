﻿using Beast.EntityFramework;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework.ModelConfigurations
{
	public class GroupClaimConfiguration : EntityConfiguration<GroupClaim>
	{
		public GroupClaimConfiguration()
			: base(DefaultSchema)
		{
			ToTable($"{Schema}.{nameof(GroupClaim)}");

			HasKey(uc => uc.Id);

			HasRequired(uc => uc.Group)
				.WithMany(u => u.Claims)
				.HasForeignKey(uc => uc.GroupId);
			HasRequired(uc => uc.Claim)
				.WithMany(u => u.GroupClaims)
				.HasForeignKey(uc => uc.ClaimId);

			HasIndex(uc => uc.GroupId);
			HasIndex(uc => uc.ClaimId);

			HasIndex(uc => new { uc.ClaimId, uc.GroupId })
				.IsUnique();
		}
	}
}