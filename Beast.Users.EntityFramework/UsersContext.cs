﻿using System.Data.Entity;
using Beast.EntityFramework;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework
{
	public class UsersContext : BeastDbContext, IUsersContext
	{
		public DbSet<Claim> Claims { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<UserClaim> UserClaims { get; set; }
		public DbSet<Group> Groups { get; set; }
		public DbSet<GroupClaim> GroupClaims { get; set; }
		public DbSet<GroupUser> GroupUsers { get; set; }

		public UsersContext(IIdService idService, ITimeService timeService, BeastDbContextConfig config) 
			: base(idService, timeService, config) { }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			OnModelCreating(modelBuilder, typeof(UsersContext).Assembly);
		}
	}
}
