﻿using System.Data.Entity;
using Beast.EntityFramework;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework
{
	public interface IUsersContext : IBeastDbContext
	{
		DbSet<Claim> Claims { get; set; }
		DbSet<User> Users { get; set; }
		DbSet<UserClaim> UserClaims { get; set; }
		DbSet<Group> Groups { get; set; }
		DbSet<GroupClaim> GroupClaims { get; set; }
		DbSet<GroupUser> GroupUsers { get; set; }
	}
}