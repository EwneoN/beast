﻿using System.Data.Entity;
using Beast.Encryption;
using Beast.EntityFramework;
using Beast.EntityFramework.Encryption;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.Models;

namespace Beast.Users.EntityFramework
{
	public class EncryptedUsersContext : EncryptedDbContext, IUsersContext
	{
		public virtual DbSet<Claim> Claims { get; set; }
		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<UserClaim> UserClaims { get; set; }
		public virtual DbSet<Group> Groups { get; set; }
		public virtual DbSet<GroupClaim> GroupClaims { get; set; }
		public virtual DbSet<GroupUser> GroupUsers { get; set; }

		public EncryptedUsersContext(IIdService idService, ITimeService timeService,
		                             IEncryptionService encryptionService,
		                             BeastDbContextConfig config) 
			: base(idService, timeService, config, encryptionService) { }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			OnModelCreating(modelBuilder, typeof(UsersContext).Assembly);
		}
	}
}