﻿using System;
using Beast.Validation.Properties;
using FluentValidation;

namespace Beast.Validation
{
	public static class Extensions
	{
		public static IRuleBuilder<TRequest, T> RequiredObject<TRequest, T>(this IRuleBuilder<TRequest, T> ruleBuilder)
			where T : class 
		{
			return ruleBuilder.NotEmpty().WithLocalizedMessage(typeof(Resources), ValidationMessages.MustNotBeEmpty);
		}

		public static IRuleBuilder<TRequest, DateTimeOffset?> Required<TRequest>(this IRuleBuilder<TRequest, DateTimeOffset?> ruleBuilder)
		{
			return ruleBuilder.NotEmpty().WithLocalizedMessage(typeof(Resources), ValidationMessages.MustNotBeEmpty);
		}

		public static IRuleBuilder<TRequest, DateTimeOffset> Required<TRequest>(this IRuleBuilder<TRequest, DateTimeOffset> ruleBuilder)
		{
			return ruleBuilder.NotEmpty().WithLocalizedMessage(typeof(Resources), ValidationMessages.MustNotBeEmpty);
		}

		public static IRuleBuilder<TRequest, long?> Required<TRequest>(this IRuleBuilder<TRequest, long?> ruleBuilder)
		{
			return ruleBuilder.NotEmpty().WithLocalizedMessage(typeof(Resources), ValidationMessages.MustNotBeEmpty);
		}

		public static IRuleBuilder<TRequest, long> Required<TRequest>(this IRuleBuilder<TRequest, long> ruleBuilder)
		{
			return ruleBuilder.NotEmpty().WithLocalizedMessage(typeof(Resources), ValidationMessages.MustNotBeEmpty);
		}

		public static IRuleBuilder<TRequest, string> Required<TRequest>(this IRuleBuilder<TRequest, string> ruleBuilder)
		{
			return ruleBuilder.NotEmpty().WithLocalizedMessage(typeof(Resources), ValidationMessages.MustNotBeEmpty);
		}

		public static IRuleBuilder<TRequest, string> RequiredEmailAddress<TRequest>(this IRuleBuilder<TRequest, string> ruleBuilder)
		{
			return DefaultValidatorExtensions.EmailAddress(ruleBuilder
				                     .Required()).WithLocalizedMessage(typeof(Resources), ValidationMessages.EmailAddressMustBeValid);
		}
	}
}
