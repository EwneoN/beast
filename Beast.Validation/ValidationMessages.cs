﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beast.Validation
{
	public class ValidationMessages
	{
		public const string EmailAddressMustBeValid = "EmailAddressMustBeValid";
		public const string MustNotBeEmpty = "MustNotBeEmpty";
	}
}
