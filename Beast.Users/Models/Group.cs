﻿using System.Collections.Generic;
using Beast.Models;

namespace Beast.Users.Models
{
	public class Group : BeastModel
	{
		public string Name { get; set; }
		public long? ParentId { get; set; }

		public Group Parent { get; set; }
		public ICollection<GroupClaim> Claims { get; set; } = new HashSet<GroupClaim>();
		public ICollection<GroupUser> Users { get; set; } = new HashSet<GroupUser>();
	}
}
