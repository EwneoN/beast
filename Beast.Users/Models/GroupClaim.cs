﻿using Beast.Models;

namespace Beast.Users.Models
{
	public class GroupClaim : BeastModel
	{
		public long GroupId { get; set; }
		public long ClaimId { get; set; }

		public Group Group { get; set; }
		public Claim Claim { get; set; }
	}
}
