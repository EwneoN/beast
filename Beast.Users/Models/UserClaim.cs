﻿using Beast.Models;

namespace Beast.Users.Models
{
	public class UserClaim : BeastModel
	{
		public long UserId { get; set; }
		public long ClaimId { get; set; }

		public User User { get; set; }
		public Claim Claim { get; set; }
	}
}
