﻿using Beast.Models;

namespace Beast.Users.Models
{
	public class GroupUser : BeastModel
	{
		public long GroupId { get; set; }
		public long UserId { get; set; }

		public Group Group { get; set; }
		public User User { get; set; }
	}
}
