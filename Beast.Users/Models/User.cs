﻿using System;
using System.Collections.Generic;
using Beast.Encryption;
using Beast.Models;

namespace Beast.Users.Models
{
	public class User : BeastModel
	{
		public string AuthId { get; set; }
		public string Username { get; set; }
		public string DisplayName { get; set; }
		public string EmailAddress { get; set; }
		[EncryptedSearchable]
		public string MobilePhoneNumber { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		[EncryptedSearchable]
		public string EmployeeId { get; set; }
		public string JobTitle { get; set; }
		[EncryptedSearchable]
		public string Branch { get; set; }
		[EncryptedSearchable]
		public string BranchPostCode { get; set; }
		public bool IsLocalAccount { get; set; }
		public DateTimeOffset DateOfBirth { get; set; }

		public ICollection<UserClaim> Claims { get; set; } = new HashSet<UserClaim>();
		public ICollection<GroupUser> Groups { get; set; } = new HashSet<GroupUser>();
	}
}
