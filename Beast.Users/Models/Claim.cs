﻿using System.Collections.Generic;
using Beast.Models;

namespace Beast.Users.Models
{
	public class Claim : EnumModel
	{
		public const string Separator = "|";

		public string Module { get; set; }
		public string Resource { get; set; }
		public long? BaseClaimId { get; set; }
		public bool IsBaseClaim { get; set; }

		public Claim BaseClaim { get; set; }
		public ICollection<GroupClaim> GroupClaims { get; set; } = new HashSet<GroupClaim>();
		public ICollection<UserClaim> UserClaims { get; set; } = new HashSet<UserClaim>();
	}
}