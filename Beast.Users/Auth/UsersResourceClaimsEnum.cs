﻿using Beast.Models;
using Beast.Users.Models;

namespace Beast.Users.Auth
{
	public enum UsersResourceClaimsEnum
	{
		[EnumMetadata(typeof(User), Module = Module.Name, Resource = Resources.Users, Name = "Unknown", Description = "Represents an unset user value")]
		Unknown = 0,							 
		[EnumMetadata(typeof(User), Module = Module.Name, Resource = Resources.Users, Name = "Create", Description = "Create a new User")]
		Create = 1,								
		[EnumMetadata(typeof(User), Module = Module.Name, Resource = Resources.Users, Name = "Read", Description = "Read Users")]
		Read = 2,									 
		[EnumMetadata(typeof(User), Module = Module.Name, Resource = Resources.Users, Name = "Update", Description = "Update Users")]
		Update = 4,								 
		[EnumMetadata(typeof(User), Module = Module.Name, Resource = Resources.Users, Name = "Delete", Description = "Delete a User")]
		Delete = 8
	}
}
