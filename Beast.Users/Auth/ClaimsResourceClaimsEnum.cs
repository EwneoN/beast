﻿using Beast.Models;
using Beast.Users.Models;

namespace Beast.Users.Auth
{
	public enum ClaimsResourceClaimsEnum
	{
		[EnumMetadata(typeof(Claim), Module = Module.Name, Resource = Resources.Claims, Name = "Unknown", Description = "Represents an unset claim value")]
		Unknown = 0,
		[EnumMetadata(typeof(Claim), Module = Module.Name, Resource = Resources.Claims, Name = "Create", Description = "Create a new Claim")]
		Create = 1,
		[EnumMetadata(typeof(Claim), Module = Module.Name, Resource = Resources.Claims, Name = "Read", Description = "Read Claims")]
		Read = 2,
		[EnumMetadata(typeof(Claim), Module = Module.Name, Resource = Resources.Claims, Name = "Update", Description = "Update Claims")]
		Update = 4,
		[EnumMetadata(typeof(Claim), Module = Module.Name, Resource = Resources.Claims, Name = "Delete", Description = "Delete a Claim")]
		Delete = 8
	}
}