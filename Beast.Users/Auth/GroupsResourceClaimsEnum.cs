﻿using Beast.Models;
using Beast.Users.Models;

namespace Beast.Users.Auth
{
	public enum GroupsResourceClaimsEnum
	{
		[EnumMetadata(typeof(Group), Module = Module.Name, Resource = Resources.Groups, Name = "Unknown", Description = "Represents an unset group value")]
		Unknown = 0,
		[EnumMetadata(typeof(Group), Module = Module.Name, Resource = Resources.Groups, Name = "Create", Description = "Create a new Group")]
		Create = 1,
		[EnumMetadata(typeof(Group), Module = Module.Name, Resource = Resources.Groups, Name = "Read", Description = "Read Groups")]
		Read = 2,
		[EnumMetadata(typeof(Group), Module = Module.Name, Resource = Resources.Groups, Name = "Update", Description = "Update Groups")]
		Update = 4,
		[EnumMetadata(typeof(Group), Module = Module.Name, Resource = Resources.Groups, Name = "Delete", Description = "Delete a Group")]
		Delete = 8
	}
}