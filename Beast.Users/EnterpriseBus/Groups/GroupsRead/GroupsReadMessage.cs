﻿using System;
using Beast.Collections;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Groups.GroupsRead
{
	public class GroupsReadMessage : EnterpiseBusMessage, IGroupsReadMessage
	{
		public IPagedQueryParams QueryParams { get; set; }

		public GroupsReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp,
		                         IPagedQueryParams queryParams)
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			QueryParams = queryParams ?? throw new ArgumentNullException(nameof(queryParams));
		}
	}
}