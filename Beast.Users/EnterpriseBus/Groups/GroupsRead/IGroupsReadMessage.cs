﻿using Beast.Collections;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.EnterpriseBus.Groups.GroupsRead
{
	public interface IGroupsReadMessage : IEnterpriseBusMessage
	{
		IPagedQueryParams QueryParams { get; set; }
	}
}