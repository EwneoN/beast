﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Groups;
using Beast.Users.DTOs.Users;

namespace Beast.Users.EnterpriseBus.Groups.GroupUpdated
{
	public interface IGroupUpdatedMessage : IEnterpriseBusMessage
	{
		IGroupInfo GroupInfo { get; set; }
	}
}
