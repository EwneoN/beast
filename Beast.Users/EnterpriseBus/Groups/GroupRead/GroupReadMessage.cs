﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Groups.GroupRead
{
	public class GroupReadMessage : EnterpiseBusMessage, IGroupReadMessage
	{
		public long GroupId { get; set; }

		public GroupReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long groupId) :
			base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupId = groupId;
		}
	}
}