﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Groups.GroupRead
{
	public interface IGroupReadMessage : IEnterpriseBusMessage
	{
		long GroupId { get; set; }
	}
}