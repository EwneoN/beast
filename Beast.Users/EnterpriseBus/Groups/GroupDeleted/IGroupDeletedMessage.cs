﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Groups.GroupDeleted
{
	public interface IGroupDeletedMessage : IEnterpriseBusMessage
	{
		long GroupId { get; set; }
	}
}