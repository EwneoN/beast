﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Groups.GroupDeleted
{
	public class GroupDeletedMessage : EnterpiseBusMessage, IGroupDeletedMessage
	{
		public long GroupId { get; set; }

		public GroupDeletedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long groupId) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupId = groupId;
		}
	}
}