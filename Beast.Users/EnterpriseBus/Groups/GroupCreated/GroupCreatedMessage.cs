﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Groups;
using Beast.Users.DTOs.Users;

namespace Beast.Users.EnterpriseBus.Groups.GroupCreated
{
	public class GroupCreatedMessage : EnterpiseBusMessage, IGroupCreatedMessage
	{
		public IGroupInfo GroupInfo { get; set; }

		public GroupCreatedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IGroupInfo groupInfo) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupInfo = groupInfo ?? throw new ArgumentNullException(nameof(groupInfo));
		}
	}
}