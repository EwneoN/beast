﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Groups;
using Beast.Users.DTOs.Users;

namespace Beast.Users.EnterpriseBus.Groups.GroupCreated
{
	public interface IGroupCreatedMessage : IEnterpriseBusMessage
	{
		IGroupInfo GroupInfo { get; set; }
	}
}
