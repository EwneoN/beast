﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Claims.ClaimsRead
{
	public interface IClaimsReadMessage : IEnterpriseBusMessage
	{
	}
}
