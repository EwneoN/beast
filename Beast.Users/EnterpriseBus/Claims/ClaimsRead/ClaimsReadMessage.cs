﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Claims.ClaimsRead
{
	public class ClaimsReadMessage : EnterpiseBusMessage, IClaimsReadMessage
	{

		public ClaimsReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
		}
	}
}