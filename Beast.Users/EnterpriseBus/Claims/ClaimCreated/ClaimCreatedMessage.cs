﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Claims;

namespace Beast.Users.EnterpriseBus.Claims.ClaimCreated
{
	public class ClaimCreatedMessage : EnterpiseBusMessage, IClaimCreatedMessage
	{
		public IClaimInfo ClaimInfo { get; set; }

		public ClaimCreatedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IClaimInfo claimInfo) :
			base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			ClaimInfo = claimInfo;
		}
	}
}