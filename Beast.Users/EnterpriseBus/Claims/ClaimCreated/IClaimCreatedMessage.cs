﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Claims;

namespace Beast.Users.EnterpriseBus.Claims.ClaimCreated
{
	public interface IClaimCreatedMessage : IEnterpriseBusMessage
	{
		IClaimInfo ClaimInfo { get; set; }
	}
}