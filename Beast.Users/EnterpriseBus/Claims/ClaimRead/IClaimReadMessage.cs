﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Claims.ClaimRead
{
	public interface IClaimReadMessage : IEnterpriseBusMessage
	{
		long ClaimId { get; set; }
	}
}