﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Claims.ClaimRead
{
	public class ClaimReadMessage : EnterpiseBusMessage, IClaimReadMessage
	{
		public long ClaimId { get; set; }

		public ClaimReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long claimId) :
			base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			ClaimId = claimId;
		}
	}
}