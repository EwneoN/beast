﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Claims.ClaimDeleted
{
	public interface IClaimDeletedMessage : IEnterpriseBusMessage
	{
		long ClaimId { get; set; }
	}
}