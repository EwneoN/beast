﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Claims.ClaimDeleted
{
	public class ClaimDeletedMessage : EnterpiseBusMessage, IClaimDeletedMessage
	{
		public long ClaimId { get; set; }

		public ClaimDeletedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long claimId) :
			base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			ClaimId = claimId;
		}
	}
}