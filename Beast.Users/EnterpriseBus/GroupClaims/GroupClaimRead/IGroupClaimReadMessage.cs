﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimRead
{
	public interface IGroupClaimReadMessage : IEnterpriseBusMessage
	{
		long GroupClaimId { get; set; }
	}
}