﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimRead
{
	public class GroupClaimReadMessage : EnterpiseBusMessage, IGroupClaimReadMessage
	{
		public long GroupClaimId { get; set; }

		public GroupClaimReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long groupClaimId) :
			base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupClaimId = groupClaimId;
		}
	}
}