﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimDeleted
{
	public interface IGroupClaimDeletedMessage : IEnterpriseBusMessage
	{
		long GroupClaimId { get; set; }
	}
}