﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimDeleted
{
	public class GroupClaimDeletedMessage : EnterpiseBusMessage, IGroupClaimDeletedMessage
	{
		public long GroupClaimId { get; set; }

		public GroupClaimDeletedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long groupClaimId) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupClaimId = groupClaimId;
		}
	}
}