﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimCreated
{
	public interface IGroupClaimCreatedMessage : IEnterpriseBusMessage
	{
		IGroupClaimInfo GroupClaimInfo { get; set; }
	}
}
