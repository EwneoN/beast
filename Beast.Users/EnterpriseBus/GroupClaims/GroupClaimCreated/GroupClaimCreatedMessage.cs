﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimCreated
{
	public class GroupClaimCreatedMessage : EnterpiseBusMessage, IGroupClaimCreatedMessage
	{
		public IGroupClaimInfo GroupClaimInfo { get; set; }

		public GroupClaimCreatedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IGroupClaimInfo groupClaimInfo) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupClaimInfo = groupClaimInfo ?? throw new ArgumentNullException(nameof(groupClaimInfo));
		}
	}
}