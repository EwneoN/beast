﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimsRead
{
	public class GroupClaimsReadMessage : EnterpiseBusMessage, IGroupClaimsReadMessage
	{
		public long GroupId { get; set; }

		public GroupClaimsReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long groupId) : base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupId = groupId;
		}
	}
}