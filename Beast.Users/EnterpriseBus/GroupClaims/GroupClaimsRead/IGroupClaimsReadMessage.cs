﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupClaims.GroupClaimsRead
{
	public interface IGroupClaimsReadMessage : IEnterpriseBusMessage
	{
		long GroupId { get; set; }
	}
}
