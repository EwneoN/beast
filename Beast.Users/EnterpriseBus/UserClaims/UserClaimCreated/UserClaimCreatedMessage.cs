﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimCreated
{
	public class UserClaimCreatedMessage : EnterpiseBusMessage, IUserClaimCreatedMessage
	{
		public IUserClaimInfo UserClaimInfo { get; set; }

		public UserClaimCreatedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IUserClaimInfo userClaimInfo) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserClaimInfo = userClaimInfo ?? throw new ArgumentNullException(nameof(userClaimInfo));
		}
	}
}