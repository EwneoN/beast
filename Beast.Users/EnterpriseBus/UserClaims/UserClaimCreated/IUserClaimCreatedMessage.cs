﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimCreated
{
	public interface IUserClaimCreatedMessage : IEnterpriseBusMessage
	{
		IUserClaimInfo UserClaimInfo { get; set; }
	}
}
