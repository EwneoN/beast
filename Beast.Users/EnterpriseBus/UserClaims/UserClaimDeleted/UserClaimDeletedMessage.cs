﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimDeleted
{
	public class UserClaimDeletedMessage : EnterpiseBusMessage, IUserClaimDeletedMessage
	{
		public long UserClaimId { get; set; }

		public UserClaimDeletedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long userClaimId) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserClaimId = userClaimId;
		}
	}
}