﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimDeleted
{
	public interface IUserClaimDeletedMessage : IEnterpriseBusMessage
	{
		long UserClaimId { get; set; }
	}
}