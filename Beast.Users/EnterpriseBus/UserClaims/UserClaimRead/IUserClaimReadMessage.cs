﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimRead
{
	public interface IUserClaimReadMessage : IEnterpriseBusMessage
	{
		long UserClaimId { get; set; }
	}
}