﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimRead
{
	public class UserClaimReadMessage : EnterpiseBusMessage, IUserClaimReadMessage
	{
		public long UserClaimId { get; set; }

		public UserClaimReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long userClaimId) :
			base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserClaimId = userClaimId;
		}
	}
}