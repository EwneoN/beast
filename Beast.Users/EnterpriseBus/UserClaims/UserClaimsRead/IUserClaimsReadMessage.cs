﻿using System.Collections.Generic;
using System.Text;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimsRead
{
	public interface IUserClaimsReadMessage : IEnterpriseBusMessage
	{
		long UserId { get; set; }
	}
}
