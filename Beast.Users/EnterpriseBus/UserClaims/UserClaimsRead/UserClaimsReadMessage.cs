﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.UserClaims.UserClaimsRead {
	public class UserClaimsReadMessage : EnterpiseBusMessage, IUserClaimsReadMessage
	{
		public long UserId { get; set; }

		public UserClaimsReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long userId) : base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserId = userId;
		}
	}
}