﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Users.UserDeleted
{
	public interface IUserDeletedMessage : IEnterpriseBusMessage
	{
		long UserId { get; set; }
	}
}