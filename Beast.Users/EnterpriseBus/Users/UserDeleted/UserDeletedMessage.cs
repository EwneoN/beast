﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Users.UserDeleted
{
	public class UserDeletedMessage : EnterpiseBusMessage, IUserDeletedMessage
	{
		public long UserId { get; set; }

		public UserDeletedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long userId) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserId = userId;
		}
	}
}