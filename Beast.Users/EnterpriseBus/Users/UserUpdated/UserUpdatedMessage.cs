﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Users;

namespace Beast.Users.EnterpriseBus.Users.UserUpdated
{
	public class UserUpdatedMessage : EnterpiseBusMessage, IUserUpdatedMessage
	{
		public IUserInfo UserInfo { get; set; }

		public UserUpdatedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IUserInfo userInfo) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserInfo = userInfo ?? throw new ArgumentNullException(nameof(userInfo));
		}
	}
}