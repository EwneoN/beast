﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Users;

namespace Beast.Users.EnterpriseBus.Users.UserUpdated
{
	public interface IUserUpdatedMessage : IEnterpriseBusMessage
	{
		IUserInfo UserInfo { get; set; }
	}
}
