﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Users;

namespace Beast.Users.EnterpriseBus.Users.UserCreated
{
	public interface IUserCreatedMessage : IEnterpriseBusMessage
	{
		IUserInfo UserInfo { get; set; }
	}
}
