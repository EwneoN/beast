﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Users;

namespace Beast.Users.EnterpriseBus.Users.UserCreated
{
	public class UserCreatedMessage : EnterpiseBusMessage, IUserCreatedMessage
	{
		public IUserInfo UserInfo { get; set; }

		public UserCreatedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IUserInfo userInfo) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserInfo = userInfo ?? throw new ArgumentNullException(nameof(userInfo));
		}
	}
}