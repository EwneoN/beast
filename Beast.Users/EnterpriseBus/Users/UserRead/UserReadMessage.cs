﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Users.UserRead
{
	public class UserReadMessage : EnterpiseBusMessage, IUserReadMessage
	{
		public long UserId { get; set; }

		public UserReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long userId) :
			base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			UserId = userId;
		}
	}
}