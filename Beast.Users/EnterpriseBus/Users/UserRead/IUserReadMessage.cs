﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Users.UserRead
{
	public interface IUserReadMessage : IEnterpriseBusMessage
	{
		long UserId { get; set; }
	}
}