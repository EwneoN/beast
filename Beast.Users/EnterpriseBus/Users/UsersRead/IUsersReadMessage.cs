﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Users.UsersRead
{
	public interface IUsersReadMessage : IPagedCollectionMessage { }
}