﻿using System;
using Beast.Collections;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.Users.UsersRead
{
	public class UsersReadMessage : PagedCollectionMessage, IUsersReadMessage
	{
		public UsersReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IPagedQueryParams queryParams) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp, queryParams) { }
	}
}