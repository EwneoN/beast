﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.DTOs.GroupUsers;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.EnterpriseBus.GroupUsers.GroupUserCreated
{
	public class GroupUserCreatedMessage : EnterpiseBusMessage, IGroupUserCreatedMessage
	{
		public IGroupUserInfo GroupUserInfo { get; set; }

		public GroupUserCreatedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IGroupUserInfo groupUserInfo) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupUserInfo = groupUserInfo ?? throw new ArgumentNullException(nameof(groupUserInfo));
		}
	}
}