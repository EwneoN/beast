﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.DTOs.GroupUsers;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.EnterpriseBus.GroupUsers.GroupUserCreated
{
	public interface IGroupUserCreatedMessage : IEnterpriseBusMessage
	{
		IGroupUserInfo GroupUserInfo { get; set; }
	}
}
