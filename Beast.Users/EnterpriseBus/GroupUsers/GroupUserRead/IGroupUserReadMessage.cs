﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupUsers.GroupUserRead
{
	public interface IGroupUserReadMessage : IEnterpriseBusMessage
	{
		long GroupUserId { get; set; }
	}
}