﻿using System;
using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.EnterpriseBus.GroupUsers.GroupUsersRead
{
	public class GroupUsersReadMessage : EnterpiseBusMessage, IGroupUsersReadMessage
	{
		public IGroupPagedQueryParams QueryParams { get; set; }

		public GroupUsersReadMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IGroupPagedQueryParams queryParams) : base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			QueryParams = queryParams;
		}
	}
}