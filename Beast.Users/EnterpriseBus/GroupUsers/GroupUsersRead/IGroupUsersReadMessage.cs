﻿using Beast.EnterpriseBus.Messages;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.EnterpriseBus.GroupUsers.GroupUsersRead
{
	public interface IGroupUsersReadMessage : IEnterpriseBusMessage
	{
		IGroupPagedQueryParams QueryParams { get; set; }
	}
}
