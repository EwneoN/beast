﻿using System;
using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupUsers.GroupUserDeleted
{
	public class GroupUserDeletedMessage : EnterpiseBusMessage, IGroupUserDeletedMessage
	{
		public long GroupUserId { get; set; }

		public GroupUserDeletedMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, long groupUserId) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			GroupUserId = groupUserId;
		}
	}
}