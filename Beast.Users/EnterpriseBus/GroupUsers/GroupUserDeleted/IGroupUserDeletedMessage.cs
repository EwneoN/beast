﻿using Beast.EnterpriseBus.Messages;

namespace Beast.Users.EnterpriseBus.GroupUsers.GroupUserDeleted
{
	public interface IGroupUserDeletedMessage : IEnterpriseBusMessage
	{
		long GroupUserId { get; set; }
	}
}