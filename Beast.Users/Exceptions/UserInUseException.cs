﻿using System;

namespace Beast.Users.Exceptions
{
	public class UserInUseException : Exception
	{
		public UserInUseException(long userId)
			: base($"{userId} is already in use") { }

		public UserInUseException(long userId, Exception innerException)
			: base($"{userId} is already in use", innerException) { }
	}
}