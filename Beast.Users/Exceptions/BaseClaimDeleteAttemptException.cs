﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beast.Users.Exceptions
{
	public class BaseClaimDeleteAttemptException : Exception
	{
		public BaseClaimDeleteAttemptException(string claimName) 
			: base($"Cannot delete claim \"{claimName}\" because it is a built in claim") { }
	}
}
