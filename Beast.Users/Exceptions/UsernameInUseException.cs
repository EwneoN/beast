﻿using System;

namespace Beast.Users.Exceptions
{
	public class UsernameInUseException : Exception
	{
		public UsernameInUseException(string userName) 
			: base($"{userName} is already in use") { }
		public UsernameInUseException(string userName, Exception innerException) 
			: base($"{userName} is already in use", innerException) { }
	}
}
