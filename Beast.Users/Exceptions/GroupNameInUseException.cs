﻿using System;

namespace Beast.Users.Exceptions
{
	public class GroupNameInUseException : Exception
	{
		public GroupNameInUseException(string groupName)
			: base($"{groupName} is already in use") { }

		public GroupNameInUseException(string groupName, Exception innerException)
			: base($"{groupName} is already in use", innerException) { }
	}
}