﻿using System;

namespace Beast.Users.Exceptions
{
	public class ClaimInUseException : Exception
	{
		public ClaimInUseException(long claimId)
			: base($"{claimId} is already in use") { }

		public ClaimInUseException(long claimId, Exception innerException)
			: base($"{claimId} is already in use", innerException) { }
	}
}