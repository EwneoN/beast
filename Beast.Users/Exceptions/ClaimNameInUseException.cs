﻿using System;

namespace Beast.Users.Exceptions
{
	public class ClaimNameInUseException : Exception
	{
		public ClaimNameInUseException(string claimName)
			: base($"{claimName} is already in use") { }

		public ClaimNameInUseException(string claimName, Exception innerException)
			: base($"{claimName} is already in use", innerException) { }
	}
}