﻿namespace Beast.Users.Utils
{
	public interface IDisplayNameFactory
	{
		string MakeDisplayName(string firstName, string middleName, string lastName);
	}
}