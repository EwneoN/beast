﻿namespace Beast.Users.Utils
{
	public class DisplayNameFactory : IDisplayNameFactory
	{
		public string MakeDisplayName(string firstName, string middleName, string lastName)
		{
			if (string.IsNullOrWhiteSpace(firstName) && string.IsNullOrWhiteSpace(lastName))
			{
				return null;
			}

			return $"{firstName ?? "???"}" +
			       $"{(!string.IsNullOrWhiteSpace(middleName) ? $" {middleName[0]} " : " ")}" +
			       $"{lastName ?? "???"}";
		}
	}
}