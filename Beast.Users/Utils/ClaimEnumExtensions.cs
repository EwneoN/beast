﻿using System;
using Beast.Models;
using Beast.Users.Models;

namespace Beast.Users.Utils
{
	public static class ClaimEnumExtensions
	{
		public static string ToClaimString<T>(this T claimEnum)
			where T : Enum
		{
			var metaData = claimEnum.GetAttribute<T, EnumMetadataAttribute>();

			return metaData != null ? $"{metaData.Module}{Claim.Separator}{metaData.Resource}{Claim.Separator}{metaData.Name}" : null;
		}
	}
}