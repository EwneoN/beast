﻿using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Repositories;
using Beast.Users.DTOs.Users;

namespace Beast.Users.Repositories
{
	public interface IUsersRepository : IRepository
	{
		Task<bool> DoesUserExist(long userId, CancellationToken cancellationToken);
		Task<bool> DoesUserExist(string username, CancellationToken cancellationToken);
		Task<IUserInfo> GetUserByAuthId(string authId, CancellationToken cancellationToken);
		Task<IUserInfo> GetUserByUsername(string username, CancellationToken cancellationToken);
		Task<long> GetUserId(string username, CancellationToken cancellationToken);
		Task<string> GetUserAuthId(long userId, CancellationToken cancellationToken);
		Task<string> GetUsername(long userId, CancellationToken cancellationToken);
		Task<IUserInfo> CreateNewUser(INewUser newUser, CancellationToken cancellationToken);
		Task<IPagedCollection<IUserInfo>> GetUsers(IPagedQueryParams queryParams, CancellationToken cancellationToken);
		Task<IUserInfo> GetUser(long userId, CancellationToken cancellationToken);
		Task<IUserInfo> UpdateUser(IUserUpdate update, CancellationToken cancellationToken);
		Task DeleteUser(long userId, CancellationToken cancellationToken);
		Task SetUserAuthId(long userId, string authId, CancellationToken cancellationToken);
	}
}