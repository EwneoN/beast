﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Repositories;
using Beast.Users.DTOs.Claims;

namespace Beast.Users.Repositories
{
	public interface IClaimsRepository : IRepository
	{
		Task<bool> DoesClaimExist(long claimId, CancellationToken cancellationToken);
		Task<bool> DoesClaimExist(string name, CancellationToken cancellationToken);
		Task<IClaimInfo> CreateNewClaim(INewClaim newClaim, CancellationToken cancellationToken);
		Task<ICollection<IClaimInfo>> GetAllClaims(CancellationToken cancellationToken);
		Task<IClaimInfo> GetClaim(long claimId, CancellationToken cancellationToken);
		Task<IClaimInfo> GetClaimByName(string name, CancellationToken cancellationToken);
		Task DeleteClaim(long claimId, CancellationToken cancellationToken);
	}
}
