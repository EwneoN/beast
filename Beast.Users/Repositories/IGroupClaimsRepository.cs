﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.Repositories
{
	public interface IGroupClaimsRepository
	{
		Task<bool> DoesGroupClaimExist(long groupClaimId, CancellationToken cancellationToken);
		Task<bool> DoesGroupClaimExist(long groupId, string  claimName, CancellationToken cancellationToken);
		Task<bool> DoesGroupClaimExist(long groupId, long claimId, CancellationToken cancellationToken);
		Task<ICollection<IGroupClaimInfo>> GetGroupClaims(long groupId, CancellationToken cancellationToken);
		Task<IGroupClaimInfo> GetGroupClaim(long groupClaimId, CancellationToken cancellationToken);
		Task<IGroupClaimInfo> GetGroupClaimByName(long groupId, string claimName, CancellationToken cancellationToken);
		Task<IGroupClaimInfo> CreateNewGroupClaim(INewGroupClaim newGroupClaim, CancellationToken cancellationToken);
		Task DeleteGroupClaim(long groupClaimId, CancellationToken cancellationToken);
	}
}