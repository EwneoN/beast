﻿using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.Repositories
{
	public interface IGroupsRepository
	{
		Task<bool> DoesGroupExist(long groupId, CancellationToken cancellationToken);
		Task<bool> DoesGroupExist(string groupName, CancellationToken cancellationToken);
		Task<IPagedCollection<IGroupInfo>> GetGroups(IPagedQueryParams queryParams, CancellationToken cancellationToken);
		Task<IGroupInfo> GetGroup(long groupId, CancellationToken cancellationToken);
		Task<IGroupInfo> GetGroupByName(string groupName, CancellationToken cancellationToken);
		Task<IGroupInfo> CreateNewGroup(INewGroup newGroup, CancellationToken cancellationToken);
		Task<IGroupInfo> UpdateGroup(IGroupUpdate update, CancellationToken cancellationToken);
		Task DeleteGroup(long groupId, CancellationToken cancellationToken);
	}
}