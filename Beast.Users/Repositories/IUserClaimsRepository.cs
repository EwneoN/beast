﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.Repositories
{
	public interface IUserClaimsRepository
	{
		Task<bool> DoesUserClaimExist(long userClaimId, CancellationToken cancellationToken);
		Task<bool> DoesUserClaimExist(long userId, string claimName, CancellationToken cancellationToken);
		Task<bool> DoesUserClaimExist(long userId, long claimId, CancellationToken cancellationToken);
		Task<ICollection<IUserClaimInfo>> GetUserClaims(long userId, CancellationToken cancellationToken);
		Task<IUserClaimInfo> GetUserClaim(long userClaimId, CancellationToken cancellationToken);
		Task<IUserClaimInfo> GetUserClaimByName(long userId, string claimName, CancellationToken cancellationToken);
		Task<IUserClaimInfo> CreateNewUserClaim(INewUserClaim newUserClaim, CancellationToken cancellationToken);
		Task DeleteUserClaim(long userClaimId, CancellationToken cancellationToken);
	}
}
