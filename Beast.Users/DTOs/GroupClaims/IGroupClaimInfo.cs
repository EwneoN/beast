﻿using System;

namespace Beast.Users.DTOs.GroupClaims
{
	public interface IGroupClaimInfo
	{
		long GroupClaimId { get; set; }
		long GroupId { get; set; }
		long ClaimId { get; set; }
		string Name { get; set; }
		DateTimeOffset Timestamp { get; set; }
	}
}
