﻿namespace Beast.Users.DTOs.GroupClaims
{
	public interface INewGroupClaim
	{
		long GroupId { get; set; }
		long ClaimId { get; set; }
	}
}
