﻿using System;

namespace Beast.Users.DTOs.GroupClaims
{
	public class GroupClaimInfo : IGroupClaimInfo
	{
		public long GroupClaimId { get; set; }
		public long GroupId { get; set; }
		public long ClaimId { get; set; }
		public string Name { get; set; }
		public DateTimeOffset Timestamp { get; set; }
	}
}