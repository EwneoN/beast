﻿namespace Beast.Users.DTOs.GroupClaims
{
	public class NewGroupClaim: INewGroupClaim
	{
		public long GroupId { get; set; }
		public long ClaimId { get; set; }
	}
}