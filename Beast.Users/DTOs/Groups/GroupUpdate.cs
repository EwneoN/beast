﻿namespace Beast.Users.DTOs.Groups
{
	public class GroupUpdate : IGroupUpdate
	{
		public long GroupId { get; set; }
		public string Name { get; set; }
		public long? ParentGroupId { get; set; }
	}
}