﻿namespace Beast.Users.DTOs.Groups
{
	public interface INewGroup
	{
		string Name { get; set; }
		long? ParentGroupId { get; set; }
	}
}
