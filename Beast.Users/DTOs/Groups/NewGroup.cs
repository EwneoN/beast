﻿namespace Beast.Users.DTOs.Groups
{
	public class NewGroup : INewGroup
	{
		public string Name { get; set; }
		public long? ParentGroupId { get; set; }
	}
}