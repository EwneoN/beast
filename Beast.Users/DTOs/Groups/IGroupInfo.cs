﻿using System;
using System.Collections.Generic;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.DTOs.Groups
{
	public interface IGroupInfo
	{
		long GroupId { get; set; }
		long? ParentGroupId { get; set; }
		string Name { get; set; }
		string ParentGroupName { get; set; }
		ICollection<IGroupClaimInfo> Claims { get; set; }
		DateTimeOffset Timestamp { get; set; }
	}
}