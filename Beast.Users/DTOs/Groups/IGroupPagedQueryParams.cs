﻿using Beast.Collections;

namespace Beast.Users.DTOs.Groups
{
	public interface IGroupPagedQueryParams : IPagedQueryParams
	{
		long GroupId { get; set; }
	}
}
