﻿using Beast.Collections;

namespace Beast.Users.DTOs.Groups
{
	public class GroupPagedQueryParams : PagedQueryParams, IGroupPagedQueryParams
	{
		public long GroupId { get; set; }
	}
}