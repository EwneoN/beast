﻿using System;
using System.Collections.Generic;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.DTOs.Groups
{
	public class GroupInfo : IGroupInfo
	{
		public long GroupId { get; set; }
		public long? ParentGroupId { get; set; }
		public string Name { get; set; }
		public string ParentGroupName { get; set; }
		public ICollection<IGroupClaimInfo> Claims { get; set; }
		public DateTimeOffset Timestamp { get; set; }
	}
}