﻿namespace Beast.Users.DTOs.Groups
{
	public interface IGroupUpdate
	{
		long GroupId { get; set; }
		string Name { get; set; }
		long? ParentGroupId { get; set; }
	}
}