﻿using System;

namespace Beast.Users.DTOs.UserClaims
{
	public class UserClaimInfo : IUserClaimInfo
	{
		public long UserClaimId { get; set; }
		public long UserId { get; set; }
		public long ClaimId { get; set; }
		public string Module { get; set; }
		public string Resource { get; set; }
		public string Name { get; set; }
		public DateTimeOffset Timestamp { get; set; }
	}
}