﻿using System;

namespace Beast.Users.DTOs.UserClaims
{
	public interface IUserClaimInfo
	{
		long UserClaimId { get; set; }
		long UserId { get; set; }
		long ClaimId { get; set; }
		string Name { get; set; }
		string Module { get; set; }
		string Resource { get; set; }
		DateTimeOffset Timestamp { get; set; }
	}
}
