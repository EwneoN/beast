﻿namespace Beast.Users.DTOs.UserClaims
{
	public interface INewUserClaim
	{
		long UserId { get; set; }
		long ClaimId { get; set; }
	}
}
