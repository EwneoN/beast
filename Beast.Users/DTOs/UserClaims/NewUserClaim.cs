﻿namespace Beast.Users.DTOs.UserClaims
{
	public class NewUserClaim: INewUserClaim
	{
		public long UserId { get; set; }
		public long ClaimId { get; set; }
	}
}