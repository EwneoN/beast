﻿using System;

namespace Beast.Users.DTOs.Claims
{
	public class ClaimInfo : IClaimInfo
	{
		public long ClaimId { get; set; }
		public string Name { get; set; }
		public string Resource { get; set; }
		public string Description { get; set; }
		public bool IsBaseClaim { get; set; }
		public DateTimeOffset Timestamp { get; set; }
	}
}