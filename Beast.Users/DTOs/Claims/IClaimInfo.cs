﻿using System;

namespace Beast.Users.DTOs.Claims
{
	public interface IClaimInfo
	{
		long ClaimId { get; set; }
		string Name { get; set; }
		string Resource { get; set; }
		string Description { get; set; }
		DateTimeOffset Timestamp { get; set; }
		bool IsBaseClaim { get; set; }
	}
}
