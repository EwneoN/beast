﻿namespace Beast.Users.DTOs.Claims
{
	public class NewClaim : INewClaim
	{
		public string Name { get; set; }
		public long BaseClaimId { get; set; }
	}
}