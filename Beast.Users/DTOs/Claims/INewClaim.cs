﻿namespace Beast.Users.DTOs.Claims
{
	public interface INewClaim
	{
		string Name { get; set; }
		long BaseClaimId { get; set; }
	}
}