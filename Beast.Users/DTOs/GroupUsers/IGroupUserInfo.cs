﻿using System;

namespace Beast.Users.DTOs.GroupUsers
{
	public interface IGroupUserInfo
	{
		long GroupUserId { get; set; }
		long GroupId { get; set; }
		long UserId { get; set; }
		string Name { get; set; }
		DateTimeOffset Timestamp { get; set; }
	}
}
