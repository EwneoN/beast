﻿namespace Beast.Users.DTOs.GroupUsers
{
	public interface INewGroupUser
	{
		long GroupId { get; set; }
		long UserId { get; set; }
	}
}
