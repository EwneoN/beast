﻿namespace Beast.Users.DTOs.GroupUsers
{
	public class NewGroupUser: INewGroupUser
	{
		public long GroupId { get; set; }
		public long UserId { get; set; }
	}
}