﻿using System;

namespace Beast.Users.DTOs.GroupUsers
{
	public class GroupUserInfo : IGroupUserInfo
	{
		public long GroupUserId { get; set; }
		public long GroupId { get; set; }
		public long UserId { get; set; }
		public string Name { get; set; }
		public DateTimeOffset Timestamp { get; set; }
	}
}