﻿using System;

namespace Beast.Users.DTOs.Users {
	public class SignedUpUser : ISignedUpUser
	{
		public string AuthId { get; set; }
		public string EmailAddress { get; set; }
		public string MobilePhoneNumber { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public DateTimeOffset DateOfBirth { get; set; }
		public string EmployeeId { get; set; }
		public string JobTitle { get; set; }
		public string Branch { get; set; }
		public string BranchPostCode { get; set; }
		public string SocialAccountIssuer { get; set; }
		public string SocialAccountIssuerUserId { get; set; }
		public UserIdentityType AccountType { get; set; }
		public bool RequiresPasswordResetNextLogin { get; set; }
	}
}