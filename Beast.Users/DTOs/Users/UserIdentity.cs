﻿namespace Beast.Users.DTOs.Users
{
	public class UserIdentity : IUserIdentity
	{
		public long UserId { get; set; }
		public string AuthId { get; set; }
		public string Username { get; set; }
		public string DisplayName { get; set; }
		public string EmailAddress { get; set; }
		public string MobilePhoneNumber { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string EmployeeId { get; set; }
		public string JobTitle { get; set; }
		public string Branch { get; set; }
		public string BranchPostCode { get; set; }
		public bool IsLocalAccount { get; set; }
		public bool IsSyncedWithDirectoryAccount { get; set; }
	}
}
