﻿using System.Collections.Generic;
using Beast.Users.DTOs.Claims;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.DTOs.Users
{
	public interface IUserInfo
	{
		long UserId { get; set; }
		string AuthId { get; set; }
		ICollection<IUserClaimInfo> Claims { get; set; }
	}
}