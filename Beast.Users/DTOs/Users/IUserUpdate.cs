﻿using System;

namespace Beast.Users.DTOs.Users
{
	public interface IUserUpdate
	{
		long UserId { get; set; }
		string AuthId { get; set; }
		string Username { get; }
		string DisplayName { get; }
		string EmailAddress { get; set; }
		string MobilePhoneNumber { get; set; }
		string FirstName { get; set; }
		string MiddleName { get; set; }
		string LastName { get; set; }
		DateTimeOffset DateOfBirth { get; set; }
		string EmployeeId { get; set; }
		string JobTitle { get; set; }
		string Branch { get; set; }
		string BranchPostCode { get; set; }
	}
}