﻿namespace Beast.Users.DTOs.Users
{
	public class PasswordChange : IPasswordChange
	{
		public long UserId { get; set; }
		public string AuthId { get; set; }
		public string NewPassword { get; set; }
		public bool RequiresPasswordResetNextLogin { get; set; }
	}
}
