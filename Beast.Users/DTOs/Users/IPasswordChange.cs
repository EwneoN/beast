﻿namespace Beast.Users.DTOs.Users
{
	public interface IPasswordChange
	{
		long UserId { get; set; }
		string AuthId { get; set; }
		string NewPassword { get; set; }
		bool RequiresPasswordResetNextLogin { get; set; }
	}
}