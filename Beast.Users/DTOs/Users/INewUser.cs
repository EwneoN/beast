﻿using System;

namespace Beast.Users.DTOs.Users
{
	public interface INewUser
	{
		string Username { get; }
		string DisplayName { get; }
		string EmailAddress { get; set; }
		string MobilePhoneNumber { get; set; }
		string FirstName { get; set; }
		string MiddleName { get; set; }
		string LastName { get; set; }
		DateTimeOffset DateOfBirth { get; set; }
		string EmployeeId { get; set; }
		string JobTitle { get; set; }
		string Branch { get; set; }
		string BranchPostCode { get; set; }
		string Password { get; set; }
		string SocialAccountIssuer { get; set; }
		string SocialAccountIssuerUserId { get; set; }
		UserIdentityType AccountType { get; set; }
		bool RequiresPasswordResetNextLogin { get; set; }
	}
}