﻿namespace Beast.Users.DTOs.Users
{
	public enum UserIdentityType
	{
		Unknown = 0,
		LocalAccount = 1,
		WorkOrSchoolAccount = 2,
		SocialAccount = 3,
		Other = 4
	}
}