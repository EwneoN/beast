﻿namespace Beast.Users.DTOs.Users {
	public interface IUserIdentity
	{
		long UserId { get; set; }
		string AuthId { get; set; }
		string Username { get; set; }
		string DisplayName { get; set; }
		string EmailAddress { get; set; }
		string MobilePhoneNumber { get; set; }
		string FirstName { get; set; }
		string LastName { get; set; }
		string EmployeeId { get; set; }
		string JobTitle { get; set; }
		string Branch { get; set; }
		string BranchPostCode { get; set; }
		bool IsLocalAccount { get; set; }
		bool IsSyncedWithDirectoryAccount { get; set; }
	}
}