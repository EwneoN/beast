﻿using System.Collections.Generic;
using Beast.Users.DTOs.Claims;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.DTOs.Users
{
	public class UserInfo : IUserInfo
	{
		public long UserId { get; set; }
		public string AuthId { get; set; }
		public ICollection<IUserClaimInfo> Claims { get; set; }

	}
}