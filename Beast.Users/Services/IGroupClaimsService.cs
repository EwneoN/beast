﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.Services
{
	public interface IGroupClaimsService
	{
		event BeastEventHandler<long> GroupClaimRead;
		event BeastEventHandler<long> GroupClaimsRead;
		event BeastEventHandler<INewGroupClaim, IGroupClaimInfo> GroupClaimCreated;
		event BeastEventHandler<long> GroupClaimDeleted;

		Task<bool> DoesGroupClaimExist(long groupClaimId, CancellationToken cancellationToken);
		Task<bool> DoesGroupClaimExist(long groupId, string claimName, CancellationToken cancellationToken);
		Task<bool> DoesGroupClaimExist(long groupId, long claimId, CancellationToken cancellationToken);
		Task<ICollection<IGroupClaimInfo>> GetGroupClaims(long groupId, CancellationToken cancellationToken);
		Task<IGroupClaimInfo> GetGroupClaim(long groupClaimId, CancellationToken cancellationToken);
		Task<IGroupClaimInfo> GetGroupClaimByName(long groupClaimId, string claimName, CancellationToken cancellationToken);
		Task<IGroupClaimInfo> CreateNewGroupClaim(INewGroupClaim newGroupClaim, CancellationToken cancellationToken);
		Task DeleteGroupClaim(long groupClaimId, CancellationToken cancellationToken);
	}
}