﻿using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Events;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.Services
{
	public interface IGroupsService
	{
		event BeastEventHandler<INewGroup, IGroupInfo> GroupCreated;
		event BeastEventHandler<long> GroupRead;
		event BeastEventHandler<IPagedQueryParams> GroupsRead;
		event BeastEventHandler<IGroupUpdate, IGroupInfo> GroupUpdated;
		event BeastEventHandler<long> GroupDeleted;

		Task<bool> DoesGroupExist(long groupId, CancellationToken cancellationToken);
		Task<bool> DoesGroupExist(string groupName, CancellationToken cancellationToken);
		Task<IPagedCollection<IGroupInfo>> GetGroups(IPagedQueryParams queryParams, CancellationToken cancellationToken);
		Task<IGroupInfo> GetGroup(long groupId, CancellationToken cancellationToken);
		Task<IGroupInfo> GetGroupByName(string groupName, CancellationToken cancellationToken);
		Task<IGroupInfo> CreateNewGroup(INewGroup newGroup, CancellationToken cancellationToken);
		Task<IGroupInfo> UpdateGroup(IGroupUpdate groupUpdate, CancellationToken cancellationToken);
		Task DeleteGroup(long groupId, CancellationToken cancellationToken);
	}
}