﻿using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Events;
using Beast.Users.DTOs.Groups;
using Beast.Users.DTOs.GroupUsers;

namespace Beast.Users.Services
{
	public interface IGroupUsersService
	{
		event BeastEventHandler<long> GroupUserRead;
		event BeastEventHandler<IGroupPagedQueryParams> GroupUsersRead;
		event BeastEventHandler<INewGroupUser, IGroupUserInfo> GroupUserCreated;
		event BeastEventHandler<long> GroupUserDeleted;

		Task<bool> DoesGroupUserExist(long groupUserId, CancellationToken cancellationToken);
		Task<bool> DoesGroupUserExist(long groupId, long userId, CancellationToken cancellationToken);
		Task<bool> DoesGroupUserExist(long groupId, string username, CancellationToken cancellationToken);
		Task<IPagedCollection<IGroupUserInfo>> GetGroupUsers(IGroupPagedQueryParams queryParams, CancellationToken cancellationToken);
		Task<IGroupUserInfo> GetGroupUser(long groupUserId, CancellationToken cancellationToken);
		Task<IGroupUserInfo> GetGroupUserByUsername(long groupId, string username, CancellationToken cancellationToken);
		Task<IGroupUserInfo> CreateNewGroupUser(INewGroupUser newGroup, CancellationToken cancellationToken);
		Task DeleteGroupUser(long groupUserId, CancellationToken cancellationToken);
	}
}