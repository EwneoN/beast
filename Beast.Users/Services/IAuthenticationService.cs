﻿using System.Threading;
using System.Threading.Tasks;
using Beast.Users.DTOs.Users;

namespace Beast.Users.Services
{
	public interface IAuthenticationService
	{
		Task<IUserIdentity> CreateNewUser(INewUser newUser, CancellationToken cancellationToken);
		Task<IUserIdentity> GetUser(string authId, CancellationToken cancellationToken);
		Task<IUserIdentity> UpdateUser(IUserUpdate update, CancellationToken cancellationToken);
		Task DeleteUser(string authId, CancellationToken cancellationToken);
		Task ResetPassword(IPasswordChange passwordChange, CancellationToken cancellationToken);
	}
}