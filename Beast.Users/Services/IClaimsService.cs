﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.DTOs.Claims;

namespace Beast.Users.Services
{
	public interface IClaimsService
	{
		event BeastEventHandler ClaimsRead;
		event BeastEventHandler<long> ClaimRead;
		event BeastEventHandler<INewClaim, IClaimInfo> ClaimCreated;
		event BeastEventHandler<long> ClaimDeleted;

		Task<bool> DoesClaimExist(long claimId, CancellationToken cancellationToken);
		Task<bool> DoesClaimExist(string name, CancellationToken cancellationToken);
		Task<IClaimInfo> CreateNewClaim(INewClaim newClaim, CancellationToken cancellationToken);
		Task<ICollection<IClaimInfo>> GetAllClaims(CancellationToken cancellationToken);
		Task<IClaimInfo> GetClaim(long claimId, CancellationToken cancellationToken);
		Task<IClaimInfo> GetClaimByName(string name, CancellationToken cancellationToken);
		Task DeleteClaim(long claimId, CancellationToken cancellationToken);
	}
}
