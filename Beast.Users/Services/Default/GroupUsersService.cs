﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Events;
using Beast.Exceptions;
using Beast.Users.DTOs.Groups;
using Beast.Users.DTOs.GroupUsers;
using Beast.Users.Exceptions;
using Beast.Users.Repositories;

namespace Beast.Users.Services.Default
{
	public class GroupUsersService : IGroupUsersService
	{
		public event BeastEventHandler<long> GroupUserRead;
		public event BeastEventHandler<IGroupPagedQueryParams> GroupUsersRead;
		public event BeastEventHandler<INewGroupUser, IGroupUserInfo> GroupUserCreated;
		public event BeastEventHandler<long> GroupUserDeleted;

		private readonly IGroupUsersRepository _GroupUsersRepository;
		private readonly IGroupsRepository _GroupsRepository;
		private readonly IUsersRepository _UsersRepository;

		public GroupUsersService(IGroupUsersRepository groupUsersRepository, IGroupsRepository groupsRepository, IUsersRepository usersRepository)
		{
			_GroupUsersRepository = groupUsersRepository ?? throw new ArgumentNullException(nameof(groupUsersRepository));
			_GroupsRepository = groupsRepository ?? throw new ArgumentNullException(nameof(groupsRepository));
			_UsersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
		}

		public async Task<bool> DoesGroupUserExist(long groupUserId, CancellationToken cancellationToken)
		{
			return await _GroupUsersRepository.DoesGroupUserExist(groupUserId, cancellationToken);
		}

		public async Task<bool> DoesGroupUserExist(long groupId, long userId, CancellationToken cancellationToken)
		{
			return await _GroupUsersRepository.DoesGroupUserExist(groupId, userId, cancellationToken);
		}

		public async Task<bool> DoesGroupUserExist(long groupId, string username, CancellationToken cancellationToken)
		{
			return await _GroupUsersRepository.DoesGroupUserExist(groupId, username, cancellationToken);
		}

		public async Task<IPagedCollection<IGroupUserInfo>> GetGroupUsers(IGroupPagedQueryParams queryParams, CancellationToken cancellationToken)
		{
			var groupUsers = await _GroupUsersRepository.GetGroupUsers(queryParams, cancellationToken);

			if (GroupUsersRead != null)
			{
				await GroupUsersRead(this, new BeastEventArgs<IGroupPagedQueryParams>(queryParams));
			}

			return groupUsers;
		}

		public async Task<IGroupUserInfo> GetGroupUser(long groupUserId, CancellationToken cancellationToken)
		{
			var groupUser = await _GroupUsersRepository.GetGroupUser(groupUserId, cancellationToken);

			if (GroupUserRead != null)
			{
				await GroupUserRead(this, new BeastEventArgs<long>(groupUserId));
			}

			return groupUser;
		}

		public async Task<IGroupUserInfo> GetGroupUserByUsername(long groupId, string username,
		                                                         CancellationToken cancellationToken)
		{
			var groupUser = await _GroupUsersRepository.GetGroupUserByUsername(groupId, username, cancellationToken);

			if (GroupUserRead != null)
			{
				await GroupUserRead(this, new BeastEventArgs<long>(groupUser.GroupUserId));
			}

			return groupUser;
		}

		public async Task<IGroupUserInfo> CreateNewGroupUser(INewGroupUser newGroupUser, CancellationToken cancellationToken)
		{
			if (newGroupUser == null)
			{
				throw new ArgumentNullException(nameof(newGroupUser));
			}

			if (!await _GroupsRepository.DoesGroupExist(newGroupUser.GroupId, cancellationToken))
			{
				throw new RecordNotFoundException("Group", newGroupUser.GroupId);
			}

			if (!await _UsersRepository.DoesUserExist(newGroupUser.UserId, cancellationToken))
			{
				throw new RecordNotFoundException("User", newGroupUser.UserId);
			}

			if (await _GroupUsersRepository.DoesGroupUserExist(newGroupUser.GroupId, newGroupUser.UserId, cancellationToken))
			{
				throw new UserInUseException(newGroupUser.UserId);
			}

			var groupUser = await _GroupUsersRepository.CreateNewGroupUser(newGroupUser, cancellationToken);

			if (GroupUserCreated != null)
			{
				await GroupUserCreated(this, new BeastEventArgs<INewGroupUser, IGroupUserInfo>(newGroupUser, groupUser));
			}

			return groupUser;
		}

		public async Task DeleteGroupUser(long groupUserId, CancellationToken cancellationToken)
		{
			if (!await _GroupUsersRepository.DoesGroupUserExist(groupUserId, cancellationToken))
			{
				throw new RecordNotFoundException("GroupUser", groupUserId);
			}

			await _GroupUsersRepository.DeleteGroupUser(groupUserId, cancellationToken);

			if (GroupUserDeleted != null)
			{
				await GroupUserDeleted(this, new BeastEventArgs<long>(groupUserId));
			}
		}
	}
}