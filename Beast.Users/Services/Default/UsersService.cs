﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Events;
using Beast.Exceptions;
using Beast.Users.DTOs.Users;
using Beast.Users.Exceptions;
using Beast.Users.Repositories;

namespace Beast.Users.Services.Default
{
	public class UsersService : IUsersService
	{
		public event BeastEventHandler<INewUser, IUserInfo> UserCreated;
		public event BeastEventHandler<IPagedQueryParams> UsersRead;
		public event BeastEventHandler<long> UserRead;
		public event BeastEventHandler<IUserUpdate, IUserInfo> UserUpdated;
		public event BeastEventHandler<long> UserDeleted;

		private readonly IAuthenticationService _AuthenticationService;
		private readonly IUsersRepository _UsersRepository;

		public UsersService(IAuthenticationService authenticationService, IUsersRepository usersRepository)
		{
			_AuthenticationService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
			_UsersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
		}

		public async Task<bool> DoesUserExist(long userId, CancellationToken cancellationToken)
		{
			return await _UsersRepository.DoesUserExist(userId, cancellationToken);
		}

		public async Task<bool> DoesUserExist(string username, CancellationToken cancellationToken)
		{
			return await _UsersRepository.DoesUserExist(username, cancellationToken);
		}

		public async Task<IPagedCollection<IUserInfo>> GetUsers(IPagedQueryParams queryParams,
		                                                        CancellationToken cancellationToken)
		{
			var results = await _UsersRepository.GetUsers(queryParams, cancellationToken);

			if (UsersRead != null)
			{
				await UsersRead(this, new BeastEventArgs<IPagedQueryParams>(queryParams));
			}

			return results;
		}

		public async Task<IUserInfo> GetUser(long userId, CancellationToken cancellationToken)
		{
			IUserInfo user = await _UsersRepository.GetUser(userId, cancellationToken);

			if (user == null)
			{
				throw new RecordNotFoundException("User", userId);
			}

			if (UserRead != null)
			{
				await UserRead(this, new BeastEventArgs<long>(userId));
			}

			return user;
		}

		public async Task<IUserInfo> GetUserByAuthId(string authId, CancellationToken cancellationToken)
		{
			if (string.IsNullOrWhiteSpace(authId))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(authId));
			}

			IUserInfo user = await _UsersRepository.GetUserByAuthId(authId, cancellationToken);

			if (user == null)
			{
				throw new RecordNotFoundException("User", "AuthId", authId);
			}

			if (UserRead != null)
			{
				await UserRead(this, new BeastEventArgs<long>(user.UserId));
			}

			return user;
		}

		public async Task<IUserInfo> GetUserByUsername(string username, CancellationToken cancellationToken)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
			}

			IUserInfo user = await _UsersRepository.GetUserByUsername(username, cancellationToken);

			if (user == null)
			{
				throw new RecordNotFoundException("User", "Username", username);
			}

			if (UserRead != null)
			{
				await UserRead(this, new BeastEventArgs<long>(user.UserId));
			}

			return user;
		}

		public async Task<long> GetUserId(string username, CancellationToken cancellationToken)
		{
			long userId = await _UsersRepository.GetUserId(username, cancellationToken);

			if (userId == default(long))
			{
				throw new RecordNotFoundException("User", "Username", username);
			}

			return userId;
		}

		public async Task<string> GetUserAuthId(long userId, CancellationToken cancellationToken)
		{
			string authId = await _UsersRepository.GetUserAuthId(userId, cancellationToken);

			if (authId == null)
			{
				throw new RecordNotFoundException("User", userId);
			}

			return authId;
		}

		public async Task<string> GetUsername(long userId, CancellationToken cancellationToken)
		{
			string username = await _UsersRepository.GetUsername(userId, cancellationToken);

			if (username == null)
			{
				throw new RecordNotFoundException("User", userId);
			}

			return username;
		}

		public async Task<IUserInfo> CreateNewUser(INewUser newUser, CancellationToken cancellationToken)
		{
			if (newUser == null)
			{
				throw new ArgumentNullException(nameof(newUser));
			}

			if (await _UsersRepository.DoesUserExist(newUser.EmailAddress, cancellationToken))
			{
				throw new UsernameInUseException(newUser.Username);
			}

			await _UsersRepository.BeginTransaction(cancellationToken);

			try
			{
				IUserInfo userInfo = await _UsersRepository.CreateNewUser(newUser, cancellationToken);
				IUserIdentity authUserInfo = await _AuthenticationService.CreateNewUser(newUser, cancellationToken);
				await _UsersRepository.SetUserAuthId(userInfo.UserId, authUserInfo.AuthId, cancellationToken);
				await _UsersRepository.CommitTransaction(cancellationToken);

				if (UserCreated != null)
				{
					await UserCreated(this, new BeastEventArgs<INewUser, IUserInfo>(newUser, userInfo));
				}

				return userInfo;
			}
			catch (Exception)
			{
				await _UsersRepository.RollbackTransaction(cancellationToken);
				throw;
			}
		}

		public async Task<IUserInfo> SaveSignedUpUser(ISignedUpUser signedUpUser, CancellationToken cancellationToken)
		{
			if (signedUpUser == null)
			{
				throw new ArgumentNullException(nameof(signedUpUser));
			}

			if (await _UsersRepository.DoesUserExist(signedUpUser.EmailAddress, cancellationToken))
			{
				throw new UsernameInUseException(signedUpUser.EmailAddress);
			}

			await _UsersRepository.BeginTransaction(cancellationToken);

			try
			{
				INewUser newUser = new NewUser
				{
					AccountType = signedUpUser.AccountType,
					EmailAddress = signedUpUser.EmailAddress,
					FirstName = signedUpUser.FirstName,
					MiddleName = signedUpUser.MiddleName,
					LastName = signedUpUser.LastName,
					DateOfBirth = signedUpUser.DateOfBirth,
					Branch = signedUpUser.Branch,
					BranchPostCode = signedUpUser.BranchPostCode,
					EmployeeId = signedUpUser.EmployeeId,
					JobTitle = signedUpUser.JobTitle,
					MobilePhoneNumber = signedUpUser.MobilePhoneNumber,
					RequiresPasswordResetNextLogin = signedUpUser.RequiresPasswordResetNextLogin,
					SocialAccountIssuer = signedUpUser.SocialAccountIssuer,
					SocialAccountIssuerUserId = signedUpUser.SocialAccountIssuerUserId
				};
				IUserInfo userInfo = await _UsersRepository.CreateNewUser(newUser, cancellationToken);
				await _UsersRepository.SetUserAuthId(userInfo.UserId, signedUpUser.AuthId, cancellationToken);
				await _UsersRepository.CommitTransaction(cancellationToken);

				if (UserCreated != null)
				{
					await UserCreated(this, new BeastEventArgs<INewUser, IUserInfo>(newUser, userInfo));
				}

				return userInfo;
			}
			catch (Exception)
			{
				await _UsersRepository.RollbackTransaction(cancellationToken);
				throw;
			}
		}

		public async Task<IUserInfo> UpdateUser(IUserUpdate update, CancellationToken cancellationToken)
		{
			if (update == null)
			{
				throw new ArgumentNullException(nameof(update));
			}

			if (!await _UsersRepository.DoesUserExist(update.UserId, cancellationToken))
			{
				throw new RecordNotFoundException("User", update.UserId);
			}

			await _UsersRepository.BeginTransaction(cancellationToken);

			try
			{
				IUserInfo userInfo = await _UsersRepository.UpdateUser(update, cancellationToken);
				await _AuthenticationService.UpdateUser(update, cancellationToken);

				await _UsersRepository.CommitTransaction(cancellationToken);

				if (UserUpdated != null)
				{
					await UserUpdated(this, new BeastEventArgs<IUserUpdate, IUserInfo>(update, userInfo));
				}

				return userInfo;
			}
			catch (Exception)
			{
				await _UsersRepository.RollbackTransaction(cancellationToken);
				throw;
			}
		}

		public async Task DeleteUser(long userId, CancellationToken cancellationToken)
		{
			if (!await _UsersRepository.DoesUserExist(userId, cancellationToken))
			{
				throw new RecordNotFoundException("User", userId);
			}

			string authId = await _UsersRepository.GetUserAuthId(userId, cancellationToken);

			if (authId == null)
			{
				throw new RecordNotFoundException("User", userId);
			}

			await _UsersRepository.BeginTransaction(cancellationToken);

			try
			{
				await _UsersRepository.DeleteUser(userId, cancellationToken);
				await _AuthenticationService.DeleteUser(authId, cancellationToken);

				await _UsersRepository.CommitTransaction(cancellationToken);

				if (UserDeleted != null)
				{
					await UserDeleted(this, new BeastEventArgs<long>(userId));
				}
			}
			catch (Exception)
			{
				await _UsersRepository.RollbackTransaction(cancellationToken);
				throw;
			}
		}
	}
}
