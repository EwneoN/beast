﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Exceptions;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.Exceptions;
using Beast.Users.Repositories;

namespace Beast.Users.Services.Default
{
	public class GroupClaimsService : IGroupClaimsService
	{
		public event BeastEventHandler<long> GroupClaimRead;
		public event BeastEventHandler<long> GroupClaimsRead;
		public event BeastEventHandler<INewGroupClaim, IGroupClaimInfo> GroupClaimCreated;
		public event BeastEventHandler<long> GroupClaimDeleted;

		private readonly IGroupClaimsRepository _GroupClaimsRepository;
		private readonly IGroupsRepository _GroupsRepository;
		private readonly IClaimsRepository _ClaimsRepository;

		public GroupClaimsService(IGroupClaimsRepository userClaimsRepository, IGroupsRepository usersRepository, IClaimsRepository claimsRepository)
		{
			_GroupClaimsRepository = userClaimsRepository ?? throw new ArgumentNullException(nameof(userClaimsRepository));
			_GroupsRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
			_ClaimsRepository = claimsRepository ?? throw new ArgumentNullException(nameof(claimsRepository));
		}

		public async Task<ICollection<IGroupClaimInfo>> GetGroupClaims(long groupId, CancellationToken cancellationToken)
		{
			var groupClaims = await _GroupClaimsRepository.GetGroupClaims(groupId, cancellationToken);

			if (GroupClaimsRead != null)
			{
				await GroupClaimsRead(this, new BeastEventArgs<long>(groupId));
			}

			return groupClaims;
		}

		public async Task<bool> DoesGroupClaimExist(long groupClaimId, CancellationToken cancellationToken)
		{
			return await _GroupClaimsRepository.DoesGroupClaimExist(groupClaimId, cancellationToken);
		}

		public async Task<bool> DoesGroupClaimExist(long groupId, string claimName, CancellationToken cancellationToken)
		{
			return await _GroupClaimsRepository.DoesGroupClaimExist(groupId, claimName, cancellationToken);
		}

		public async Task<bool> DoesGroupClaimExist(long groupId, long claimId, CancellationToken cancellationToken)
		{
			return await _GroupClaimsRepository.DoesGroupClaimExist(groupId, claimId, cancellationToken);
		}

		public async Task<IGroupClaimInfo> GetGroupClaim(long groupClaimId, CancellationToken cancellationToken)
		{
			var groupClaim = await _GroupClaimsRepository.GetGroupClaim(groupClaimId, cancellationToken);

			if (GroupClaimRead != null)
			{
				await GroupClaimRead(this, new BeastEventArgs<long>(groupClaimId));
			}

			return groupClaim;
		}

		public async Task<IGroupClaimInfo> GetGroupClaimByName(long groupClaimId, string claimName, CancellationToken cancellationToken)
		{
			var groupClaim = await _GroupClaimsRepository.GetGroupClaimByName(groupClaimId, claimName, cancellationToken);

			if (GroupClaimRead != null)
			{
				await GroupClaimRead(this, new BeastEventArgs<long>(groupClaim.GroupClaimId));
			}

			return groupClaim;
		}

		public async Task<IGroupClaimInfo> CreateNewGroupClaim(INewGroupClaim newGroupClaim, CancellationToken cancellationToken)
		{
			if (newGroupClaim == null)
			{
				throw new ArgumentNullException(nameof(newGroupClaim));
			}

			if (!await _GroupsRepository.DoesGroupExist(newGroupClaim.GroupId, cancellationToken))
			{
				throw new RecordNotFoundException("Group", newGroupClaim.GroupId);
			}

			if (!await _ClaimsRepository.DoesClaimExist(newGroupClaim.ClaimId, cancellationToken))
			{
				throw new RecordNotFoundException("Claim", newGroupClaim.ClaimId);
			}

			if (await _GroupClaimsRepository.DoesGroupClaimExist(newGroupClaim.GroupId, newGroupClaim.ClaimId, cancellationToken))
			{
				throw new ClaimInUseException(newGroupClaim.ClaimId);
			}

			var groupClaim = await _GroupClaimsRepository.CreateNewGroupClaim(newGroupClaim, cancellationToken);

			if (GroupClaimCreated != null)
			{
				await GroupClaimCreated(this, new BeastEventArgs<INewGroupClaim, IGroupClaimInfo>(newGroupClaim, groupClaim));
			}

			return groupClaim;
		}

		public async Task DeleteGroupClaim(long groupClaimId, CancellationToken cancellationToken)
		{
			if (!await _GroupClaimsRepository.DoesGroupClaimExist(groupClaimId, cancellationToken))
			{
				throw new RecordNotFoundException("GroupClaim", groupClaimId);
			}

			await _GroupClaimsRepository.DeleteGroupClaim(groupClaimId, cancellationToken);

			if (GroupClaimDeleted != null)
			{
				await GroupClaimDeleted(this, new BeastEventArgs<long>(groupClaimId));
			}
		}
	}
}