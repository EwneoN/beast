﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Exceptions;
using Beast.Users.DTOs.Claims;
using Beast.Users.Exceptions;
using Beast.Users.Repositories;

namespace Beast.Users.Services.Default
{
	public class ClaimsService : IClaimsService
	{
		public event BeastEventHandler ClaimsRead;
		public event BeastEventHandler<long> ClaimRead;
		public event BeastEventHandler<INewClaim, IClaimInfo> ClaimCreated;
		public event BeastEventHandler<long> ClaimDeleted;

		private readonly IClaimsRepository _ClaimsRepository;

		public ClaimsService(IClaimsRepository claimsRepository)
		{
			_ClaimsRepository = claimsRepository ?? throw new ArgumentNullException(nameof(claimsRepository));
		}

		public async Task<bool> DoesClaimExist(long claimId, CancellationToken cancellationToken)
		{
			return await _ClaimsRepository.DoesClaimExist(claimId, cancellationToken);
		}

		public async Task<bool> DoesClaimExist(string name, CancellationToken cancellationToken)
		{
			return await _ClaimsRepository.DoesClaimExist(name, cancellationToken);
		}

		public async Task<IClaimInfo> CreateNewClaim(INewClaim newClaim, CancellationToken cancellationToken)
		{
			if (await _ClaimsRepository.DoesClaimExist(newClaim.Name, cancellationToken))
			{
				throw new ClaimNameInUseException(newClaim.Name);
			}

			var result = await _ClaimsRepository.CreateNewClaim(newClaim, cancellationToken);

			if (ClaimCreated != null)
			{
				await ClaimCreated(this, new BeastEventArgs<INewClaim, IClaimInfo>(newClaim, result));
			}

			return result;
		}

		public async Task<ICollection<IClaimInfo>> GetAllClaims(CancellationToken cancellationToken)
		{
			var results = await _ClaimsRepository.GetAllClaims(cancellationToken);

			if (ClaimsRead != null)
			{
				await ClaimsRead(this, new BeastEventArgs());
			}

			return results;
		}

		public async Task<IClaimInfo> GetClaim(long claimId, CancellationToken cancellationToken)
		{
			if (ClaimRead != null)
			{
				await ClaimRead(this, new BeastEventArgs<long>(claimId));
			}

			return await _ClaimsRepository.GetClaim(claimId, cancellationToken);
		}

		public async Task<IClaimInfo> GetClaimByName(string name, CancellationToken cancellationToken)
		{
			var result = await _ClaimsRepository.GetClaimByName(name, cancellationToken);

			if (ClaimRead != null)
			{
				await ClaimRead(this, new BeastEventArgs<long>(result.ClaimId));
			}

			return result;
		}

		public async Task DeleteClaim(long claimId, CancellationToken cancellationToken)
		{
			if (!await _ClaimsRepository.DoesClaimExist(claimId, cancellationToken))
			{
				throw new RecordNotFoundException("Claim", claimId);
			}

			await _ClaimsRepository.DeleteClaim(claimId, cancellationToken);

			if (ClaimDeleted != null)
			{
				await ClaimDeleted(this, new BeastEventArgs<long>(claimId));
			}
		}
	}
}
