﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Exceptions;
using Beast.Users.DTOs.UserClaims;
using Beast.Users.Exceptions;
using Beast.Users.Repositories;

namespace Beast.Users.Services.Default
{
	public class UserClaimsService : IUserClaimsService
	{
		public event BeastEventHandler<long> UserClaimRead;
		public event BeastEventHandler<long> UserClaimsRead;
		public event BeastEventHandler<INewUserClaim, IUserClaimInfo> UserClaimCreated;
		public event BeastEventHandler<long> UserClaimDeleted;

		private readonly IUserClaimsRepository _UserClaimsRepository;
		private readonly IUsersRepository _UsersRepository;
		private readonly IClaimsRepository _ClaimsRepository;

		public async Task<bool> DoesUserClaimExist(long userClaimId, CancellationToken cancellationToken)
		{
			return await _UserClaimsRepository.DoesUserClaimExist(userClaimId, cancellationToken);
		}

		public async Task<bool> DoesUserClaimExist(long userId, long claimId, CancellationToken cancellationToken)
		{
			return await _UserClaimsRepository.DoesUserClaimExist(userId, claimId, cancellationToken);
		}

		public async Task<bool> DoesUserClaimExist(long userId, string claimName, CancellationToken cancellationToken)
		{
			return await _UserClaimsRepository.DoesUserClaimExist(userId, claimName, cancellationToken);
		}

		public UserClaimsService(IUserClaimsRepository userClaimsRepository, IUsersRepository usersRepository, IClaimsRepository claimsRepository)
		{
			_UserClaimsRepository = userClaimsRepository ?? throw new ArgumentNullException(nameof(userClaimsRepository));
			_UsersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
			_ClaimsRepository = claimsRepository ?? throw new ArgumentNullException(nameof(claimsRepository));
		}

		public async Task<ICollection<IUserClaimInfo>> GetUserClaims(long userId, CancellationToken cancellationToken)
		{
			var userClaims = await _UserClaimsRepository.GetUserClaims(userId, cancellationToken);

			if (UserClaimsRead != null)
			{
				await UserClaimsRead(this, new BeastEventArgs<long>(userId));
			}

			return userClaims;
		}

		public async Task<IUserClaimInfo> GetUserClaim(long userClaimId, CancellationToken cancellationToken)
		{
			var userClaim = await _UserClaimsRepository.GetUserClaim(userClaimId, cancellationToken);

			if (UserClaimRead != null)
			{
				await UserClaimRead(this, new BeastEventArgs<long>(userClaimId));
			}

			return userClaim;
		}

		public async Task<IUserClaimInfo> GetUserClaimByName(long userId, string claimName, CancellationToken cancellationToken)
		{
			var userClaim = await _UserClaimsRepository.GetUserClaimByName(userId, claimName, cancellationToken);

			if (UserClaimRead != null)
			{
				await UserClaimRead(this, new BeastEventArgs<long>(userId));
			}

			return userClaim;
		}

		public async Task<IUserClaimInfo> CreateNewUserClaim(INewUserClaim newUserClaim, CancellationToken cancellationToken)
		{
			if (newUserClaim == null)
			{
				throw new ArgumentNullException(nameof(newUserClaim));
			}

			if (!await _UsersRepository.DoesUserExist(newUserClaim.UserId, cancellationToken))
			{
				throw new RecordNotFoundException("User", newUserClaim.UserId);
			}

			if (!await _ClaimsRepository.DoesClaimExist(newUserClaim.ClaimId, cancellationToken))
			{
				throw new RecordNotFoundException("Claim", newUserClaim.ClaimId);
			}

			if (await _UserClaimsRepository.DoesUserClaimExist(newUserClaim.UserId, newUserClaim.ClaimId, cancellationToken))
			{
				throw new ClaimInUseException(newUserClaim.ClaimId);
			}

			var userClaim = await _UserClaimsRepository.CreateNewUserClaim(newUserClaim, cancellationToken);

			if (UserClaimCreated != null)
			{
				await UserClaimCreated(this, new BeastEventArgs<INewUserClaim, IUserClaimInfo>(newUserClaim, userClaim));
			}

			return userClaim;
		}

		public async Task DeleteUserClaim(long userClaimId, CancellationToken cancellationToken)
		{
			if (!await _UserClaimsRepository.DoesUserClaimExist(userClaimId, cancellationToken))
			{
				throw new RecordNotFoundException("UserClaim", userClaimId);
			}

			await _UserClaimsRepository.DeleteUserClaim(userClaimId, cancellationToken);

			if (UserClaimDeleted != null)
			{
				await UserClaimDeleted(this, new BeastEventArgs<long>(userClaimId));
			}
		}
	}
}