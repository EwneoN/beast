﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Events;
using Beast.Exceptions;
using Beast.Users.DTOs.Groups;
using Beast.Users.Exceptions;
using Beast.Users.Repositories;

namespace Beast.Users.Services.Default
{
	public class GroupsService : IGroupsService
	{
		public event BeastEventHandler<INewGroup, IGroupInfo> GroupCreated;
		public event BeastEventHandler<long> GroupRead;
		public event BeastEventHandler<IPagedQueryParams> GroupsRead;
		public event BeastEventHandler<IGroupUpdate, IGroupInfo> GroupUpdated;
		public event BeastEventHandler<long> GroupDeleted;

		private readonly IGroupsRepository _GroupsRepository;

		public GroupsService(IGroupsRepository groupsRepository)
		{
			_GroupsRepository = groupsRepository ?? throw new ArgumentNullException(nameof(groupsRepository));
		}

		public async Task<bool> DoesGroupExist(long groupId, CancellationToken cancellationToken)
		{
			return await _GroupsRepository.DoesGroupExist(groupId, cancellationToken);
		}

		public async Task<bool> DoesGroupExist(string groupName, CancellationToken cancellationToken)
		{
			return await _GroupsRepository.DoesGroupExist(groupName, cancellationToken);
		}

		public async Task<IPagedCollection<IGroupInfo>> GetGroups(IPagedQueryParams queryParams,
		                                                          CancellationToken cancellationToken)
		{
			if (queryParams == null)
			{
				throw new ArgumentNullException(nameof(queryParams));
			}

			var groups = await _GroupsRepository.GetGroups(queryParams, cancellationToken);

			if (GroupsRead != null)
			{
				await GroupsRead(this, new BeastEventArgs<IPagedQueryParams>(queryParams));
			}

			return groups;
		}

		public async Task<IGroupInfo> GetGroup(long groupId, CancellationToken cancellationToken)
		{
			var group = await _GroupsRepository.GetGroup(groupId, cancellationToken);

			if (group == null)
			{
				throw new RecordNotFoundException("Group", groupId);
			}

			if (GroupRead != null)
			{
				await GroupRead(this, new BeastEventArgs<long>(groupId));
			}

			return group;
		}

		public async Task<IGroupInfo> GetGroupByName(string groupName, CancellationToken cancellationToken)
		{
			if (string.IsNullOrWhiteSpace(groupName))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(groupName));
			}

			var group = await _GroupsRepository.GetGroupByName(groupName, cancellationToken);

			if (group == null)
			{
				throw new RecordNotFoundException("Group", "Name", groupName);
			}

			if (GroupRead != null)
			{
				await GroupRead(this, new BeastEventArgs<long>(group.GroupId));
			}

			return group;
		}

		public async Task<IGroupInfo> CreateNewGroup(INewGroup newGroup, CancellationToken cancellationToken)
		{
			if (newGroup == null)
			{
				throw new ArgumentNullException(nameof(newGroup));
			}

			if (await _GroupsRepository.DoesGroupExist(newGroup.Name, cancellationToken))
			{
				throw new GroupNameInUseException(newGroup.Name);
			}

			var groupInfo = await _GroupsRepository.CreateNewGroup(newGroup, cancellationToken);

			if (GroupCreated != null)
			{
				await GroupCreated(this, new BeastEventArgs<INewGroup, IGroupInfo>(newGroup, groupInfo));
			}

			return groupInfo;
		}

		public async Task<IGroupInfo> UpdateGroup(IGroupUpdate groupUpdate, CancellationToken cancellationToken)
		{
			if (groupUpdate == null)
			{
				throw new ArgumentNullException(nameof(groupUpdate));
			}

			if (!await _GroupsRepository.DoesGroupExist(groupUpdate.GroupId, cancellationToken))
			{
				throw new RecordNotFoundException("Group", groupUpdate.GroupId);
			}

			var groupInfo = await _GroupsRepository.UpdateGroup(groupUpdate, cancellationToken);

			if (GroupUpdated != null)
			{
				await GroupUpdated(this, new BeastEventArgs<IGroupUpdate, IGroupInfo>(groupUpdate, groupInfo));
			}

			return groupInfo;
		}

		public async Task DeleteGroup(long groupId, CancellationToken cancellationToken)
		{
			if (!await _GroupsRepository.DoesGroupExist(groupId, cancellationToken))
			{
				throw new RecordNotFoundException("Group", groupId);
			}

			await _GroupsRepository.DeleteGroup(groupId, cancellationToken);

			if (GroupDeleted != null)
			{
				await GroupDeleted(this, new BeastEventArgs<long>(groupId));
			}
		}
	}
}