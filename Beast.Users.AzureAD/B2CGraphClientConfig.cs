﻿namespace AzureADGraphClient
{
	public class B2CGraphClientConfig
	{
		public string ClientId { get; set; }
		public string ClientSecret { get; set; }
		public string Tenant { get; set; }
		public string Version { get; set; }
	}
}