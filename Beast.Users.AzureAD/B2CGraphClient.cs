﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;

namespace AzureADGraphClient
{
	public class B2CGraphClient
	{
		private const string AdInstance = "https://login.microsoftonline.com/";
		private const string AdGraphResourceId = "https://graph.windows.net/";
		private const string AdGraphEndpoint = "https://graph.windows.net/";
		private const string AdGraphVersion = "api-version=1.6";

		private readonly string _Tenant;
		private readonly string _Version;
		private readonly AuthenticationContext _AuthContext;
		private readonly ClientCredential _Credential;

		public B2CGraphClient(B2CGraphClientConfig config)
		{
			if (config == null)
			{
				throw new ArgumentNullException(nameof(config));
			}

			_Tenant = config.Tenant;
			_Version = string.IsNullOrWhiteSpace(config.Version) ? AdGraphVersion : config.Version;
			_AuthContext = new AuthenticationContext(AdInstance + config.Tenant);
			_Credential = new ClientCredential(config.ClientId, config.ClientSecret);
		}

		public async Task<string> GetUserByObjectId(string objectId, CancellationToken cancellationToken)
		{
			return await SendGraphGetRequest("/users/" + objectId, null, cancellationToken);
		}

		public async Task<string> GetAllUsers(string query, CancellationToken cancellationToken)
		{
			return await SendGraphGetRequest("/users", query, cancellationToken);
		}

		public async Task<string> CreateUser(string json, CancellationToken cancellationToken)
		{
			return await SendGraphPostRequest("/users", json, cancellationToken);
		}

		public async Task<string> UpdateUser(string objectId, string json, CancellationToken cancellationToken)
		{
			return await SendGraphPatchRequest("/users/" + objectId, json, cancellationToken);
		}

		public async Task<string> DeleteUser(string objectId, CancellationToken cancellationToken)
		{
			return await SendGraphDeleteRequest("/users/" + objectId, cancellationToken);
		}

		public async Task<string> RegisterExtension(string objectId, string body, CancellationToken cancellationToken)
		{
			return await SendGraphPostRequest("/applications/" + objectId + "/extensionProperties", body, cancellationToken);
		}

		public async Task<string> DeregisterExtension(string appObjectId, string extensionObjectId, CancellationToken cancellationToken)
		{
			return await SendGraphDeleteRequest("/applications/" + appObjectId + "/extensionProperties/" + extensionObjectId, cancellationToken);
		}

		public async Task<string> GetExtensions(string appObjectId, CancellationToken cancellationToken)
		{
			return await SendGraphGetRequest("/applications/" + appObjectId + "/extensionProperties", null, cancellationToken);
		}

		public async Task<string> GetApplications(string query, CancellationToken cancellationToken)
		{
			return await SendGraphGetRequest("/applications", query, cancellationToken);
		}

		private async Task<string> SendGraphDeleteRequest(string api, CancellationToken cancellationToken)
		{
			AuthenticationResult result = await _AuthContext.AcquireTokenAsync(AdGraphResourceId, _Credential);
			HttpClient http = new HttpClient();
			string url = AdGraphEndpoint + _Tenant + api + "?" + _Version;
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, url);
			request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", result.AccessToken);
			HttpResponseMessage response = await http.SendAsync(request, cancellationToken);

			if (response.IsSuccessStatusCode)
			{
				return await response.Content.ReadAsStringAsync();
			}

			string error = await response.Content.ReadAsStringAsync();
			object formatted = JsonConvert.DeserializeObject(error);
			throw new WebException("Error Calling the Graph API: \n" + JsonConvert.SerializeObject(formatted, Formatting.Indented));
		}

		private async Task<string> SendGraphPatchRequest(string api, string json, CancellationToken cancellationToken)
		{
			AuthenticationResult result = await _AuthContext.AcquireTokenAsync(AdGraphResourceId, _Credential);
			HttpClient http = new HttpClient();
			string url = AdGraphEndpoint + _Tenant + api + "?" + _Version;
			
			HttpRequestMessage request = new HttpRequestMessage(new HttpMethod("PATCH"), url);
			request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", result.AccessToken);
			request.Content = new StringContent(json, Encoding.UTF8, "application/json");
			HttpResponseMessage response = await http.SendAsync(request, cancellationToken);

			if (response.IsSuccessStatusCode)
			{
				return await response.Content.ReadAsStringAsync();
			}

			string error = await response.Content.ReadAsStringAsync();
			object formatted = JsonConvert.DeserializeObject(error);
			throw new WebException("Error Calling the Graph API: \n" + JsonConvert.SerializeObject(formatted, Formatting.Indented));
		}

		private async Task<string> SendGraphPostRequest(string api, string json, CancellationToken cancellationToken)
		{
			AuthenticationResult result = await _AuthContext.AcquireTokenAsync(AdGraphResourceId, _Credential);
			HttpClient http = new HttpClient();
			string url = AdGraphEndpoint + _Tenant + api + "?" + _Version;
			
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);
			request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", result.AccessToken);
			request.Content = new StringContent(json, Encoding.UTF8, "application/json");
			HttpResponseMessage response = await http.SendAsync(request, cancellationToken);

			if (response.IsSuccessStatusCode)
			{
				return await response.Content.ReadAsStringAsync();
			}

			string error = await response.Content.ReadAsStringAsync();
			object formatted = JsonConvert.DeserializeObject(error);
			throw new WebException("Error Calling the Graph API: \n" + JsonConvert.SerializeObject(formatted, Formatting.Indented));
		}

		private async Task<string> SendGraphGetRequest(string api, string query, CancellationToken cancellationToken)
		{
			AuthenticationResult result = await _AuthContext.AcquireTokenAsync(AdGraphEndpoint, _Credential);

			HttpClient http = new HttpClient();
			string url = AdGraphEndpoint + _Tenant + api + "?" + _Version;
			if (!string.IsNullOrEmpty(query))
			{
				url += "&" + query;
			}
			
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url);
			request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", result.AccessToken);
			HttpResponseMessage response = await http.SendAsync(request, cancellationToken);

			if (response.IsSuccessStatusCode)
			{
				return await response.Content.ReadAsStringAsync();
			}

			string error = await response.Content.ReadAsStringAsync();
			object formatted = JsonConvert.DeserializeObject(error);
			throw new WebException("Error Calling the Graph API: \n" + JsonConvert.SerializeObject(formatted, Formatting.Indented));
		}
	}
}