﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AzureADGraphClient;
using Beast.Users.DTOs.Users;
using Beast.Users.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Beast.Users.AzureAD.Services
{
	public class AzureB2CService : IAuthenticationService
	{
		private readonly B2CGraphClient _GraphClient;

		public AzureB2CService(B2CGraphClient graphClient)
		{
			_GraphClient = graphClient ?? throw new ArgumentNullException(nameof(graphClient));
		}

		public async Task<IUserIdentity> CreateNewUser(INewUser newUser, CancellationToken cancellationToken)
		{
			var request = new
			{
				accountEnabled = true,
				creationType = newUser.AccountType == UserIdentityType.LocalAccount ? "LocalAccount" : null,
				displayName = newUser.DisplayName,
				employeeId = newUser.EmployeeId,
				givenName = newUser.FirstName,
				jobTitle = newUser.JobTitle,
				mail = newUser.EmailAddress,
				mailNickname = newUser.EmailAddress,
				mobile = newUser.MobilePhoneNumber,
				passwordPolicies = "DisablePasswordExpiration",
				passwordProfile = new
				{
					password = newUser.Password,
					forceChangePasswordNextLogin = newUser.RequiresPasswordResetNextLogin
				},
				physicalDeliveryOfficeName = newUser.Branch,
				postalCode = newUser.BranchPostCode,
				signInNames = newUser.AccountType != UserIdentityType.LocalAccount
					              ? null
					              : new[]
					              {
						              new
						              {
							              type = "emailAddress",
							              value = newUser.EmailAddress
						              }
					              },
				surname = newUser.LastName,
				userIdentities = newUser.AccountType != UserIdentityType.SocialAccount
					                 ? null
					                 : new[]
					                 {
						                 new
						                 {
							                 issuer = newUser.SocialAccountIssuer,
							                 issuerUserId = newUser.SocialAccountIssuerUserId
						                 }
					                 },
				userPrincipalName = newUser.EmailAddress
			};

			string requestString = JsonConvert.SerializeObject(request);
			string responseString = await _GraphClient.CreateUser(requestString, cancellationToken);

			return ParseUserIdentity(responseString);
		}

		public async Task<IUserIdentity> GetUser(string authId, CancellationToken cancellationToken)
		{
			string responseString = await _GraphClient.GetUserByObjectId(authId, cancellationToken);

			return ParseUserIdentity(responseString);
		}

		public async Task<IUserIdentity> UpdateUser(IUserUpdate update, CancellationToken cancellationToken)
		{
			var request = new
			{
				accountEnabled = true,
				displayName = $"{update.FirstName}" +
				              $"{(!string.IsNullOrWhiteSpace(update.MiddleName) ? $" {update.MiddleName[0]} " : " ")}" +
				              $"{update.LastName}",
				employeeId = update.EmployeeId,
				givenName = update.FirstName,
				jobTitle = update.JobTitle,
				mail = update.EmailAddress,
				mailNickname = update.EmailAddress,
				mobile = update.MobilePhoneNumber,
				physicalDeliveryOfficeName = update.Branch,
				postalCode = update.BranchPostCode,
				surname = update.LastName,
				userPrincipalName = update.EmailAddress
			};

			string requestString = JsonConvert.SerializeObject(request);
			string responseString = await _GraphClient.UpdateUser(update.AuthId, requestString, cancellationToken);

			return ParseUserIdentity(responseString);
		}

		public async Task DeleteUser(string authId, CancellationToken cancellationToken)
		{
			await _GraphClient.DeleteUser(authId, cancellationToken);
		}

		public async Task ResetPassword(IPasswordChange passwordChange, CancellationToken cancellationToken)
		{
			var request = new
			{
				passwordProfile = new
				{
					password = passwordChange.NewPassword,
					forceChangePasswordNextLogin = passwordChange.RequiresPasswordResetNextLogin
				}
			};

			string requestString = JsonConvert.SerializeObject(request);
			await _GraphClient.UpdateUser(passwordChange.AuthId, requestString, cancellationToken);
		}

		private static UserIdentity ParseUserIdentity(string jsonString)
		{
			JObject response = JObject.Parse(jsonString);

			return new UserIdentity
			{
				AuthId = response["objectId"].Value<string>(),
				Username = response["userPrincipalName"].Value<string>(),
				DisplayName = response["displayName"].Value<string>(),
				EmailAddress = response["mail"].Value<string>(),
				MobilePhoneNumber = response["mobile"].Value<string>(),
				FirstName = response["givenName"].Value<string>(),
				LastName = response["surname"].Value<string>(),
				Branch = response["physicalDeliveryOfficeName"].Value<string>(),
				BranchPostCode = response["postalCode"].Value<string>(),
				EmployeeId = response["employeeId"].Value<string>(),
				JobTitle = response["jobTitle"].Value<string>(),
				IsLocalAccount = response.ContainsKey("creationType") && response["creationType"].Value<string>() == "LocalAccount",
				IsSyncedWithDirectoryAccount = response.ContainsKey("dirSyncEnabled") && response["dirSyncEnabled"].Value<bool>(),
			};
		}
	}
}