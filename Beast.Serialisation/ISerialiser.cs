﻿using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Beast.Serialisation
{
	public interface ISerialiser
	{
		Task<string> SerialiseToString<T>(T dataToSerialise);
		Task<byte[]> SerialiseToByteArray<T>(T dataToSerialise);
		Task<T> DeserialiseFromString<T>(string dataToDeserialise);
		Task<T> DeserialiseFromByteArray<T>(byte[] dataToDeserialise);
	}
}
