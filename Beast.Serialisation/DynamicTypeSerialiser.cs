﻿using System;
using System.Threading.Tasks;
using Beast.Dynamics;

namespace Beast.Serialisation
{
	public abstract class DynamicTypeSerialiser : ISerialiser
	{
		private readonly IDynamicTypeGenerator _DynamicTypeGenerator;

		protected DynamicTypeSerialiser(IDynamicTypeGenerator dynamicTypeGenerator)
		{
			_DynamicTypeGenerator = dynamicTypeGenerator ?? throw new ArgumentNullException(nameof(dynamicTypeGenerator));
		}

		public abstract Task<string> SerialiseToString<T>(T dataToSerialise);
		public abstract Task<byte[]> SerialiseToByteArray<T>(T dataToSerialise);

		public async Task<T> DeserialiseFromString<T>(string dataToDeserialise)
		{
			Type dynamicType = _DynamicTypeGenerator.GetDynamicType<T>();

			return await Deserialise<T>(dataToDeserialise, dynamicType);
		}

		public async Task<T> DeserialiseFromByteArray<T>(byte[] dataToDeserialise)
		{
			Type dynamicType = _DynamicTypeGenerator.GetDynamicType<T>();

			return await Deserialise<T>(dataToDeserialise, dynamicType);
		}

		protected abstract Task<T> Deserialise<T>(string toSerialise, Type dynamicType);
		protected abstract Task<T> Deserialise<T>(byte[] toSerialise, Type dynamicType);
	}
}