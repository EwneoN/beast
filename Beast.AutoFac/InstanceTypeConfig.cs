﻿namespace Beast.AutoFac
{
	public class InstanceTypeConfig
	{
		public string InstanceType { get; set; }
		public string InstanceConfigType { get; set; }
	}
}