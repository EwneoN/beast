﻿using System;
using Autofac;
using Beast.Services.Ids;
using Beast.Services.Time;
using MediatR;
using Microsoft.Extensions.Configuration;
using Module = Autofac.Module;

namespace Beast.AutoFac
{
	public abstract class BeastModule<TIdService, TTimeService> : Module
		where TIdService : IIdService
		where TTimeService : ITimeService
	{
		public static Type[] MediatrHandlerInterfaces => new[]
		{
			typeof(IRequestHandler<>),
			typeof(IRequestHandler<,>),
			typeof(INotificationHandler<>)
		};

		public IConfiguration Configuration { get; }

		protected BeastModule(IConfiguration configuration)
		{
			Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
		}

		protected override void Load(ContainerBuilder builder)
		{
			if (builder == null)
			{
				throw new ArgumentNullException(nameof(builder));
			}

			//single instance
			builder.RegisterType<TIdService>()
			       .As<IIdService>()
			       .InstancePerLifetimeScope();
			builder.RegisterType<TTimeService>()
			       .As<ITimeService>()
			       .InstancePerLifetimeScope();
		}
	}
}