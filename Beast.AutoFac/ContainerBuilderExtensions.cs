﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Beast.Dynamics;
using Beast.Exceptions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Beast.AutoFac
{
	public static class ContainerBuilderExtensions
	{
		public static IRegistrationBuilder <T, SimpleActivatorData, SingleRegistrationStyle>
			RegisterConfigurableInstanceAs<T>(this ContainerBuilder containerBuilder, string instanceName, IConfiguration configuration)
			where T : class
		{
			if (string.IsNullOrWhiteSpace(instanceName))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(instanceName));
			}

			return containerBuilder
				.Register(context => GetConfigurableInstance<T>(context, instanceName, configuration))
				.As<T>();
		}

		private static T GetConfigurableInstance<T>(IComponentContext context, string instanceName, IConfiguration configuration)
			where T : class 
		{
			string instanceConfigJson = configuration[$"{instanceName}"];
			string configJson = configuration[$"{instanceName}Config"];

			if (string.IsNullOrWhiteSpace(instanceConfigJson))
			{
				throw new ConfigurationMissingException($"{instanceName}Config");
			}

			if (string.IsNullOrWhiteSpace(configJson))
			{
				throw new ConfigurationMissingException($"{instanceName}");
			}

			InstanceTypeConfig instanceTypeConfig = JsonConvert.DeserializeObject<InstanceTypeConfig>(instanceConfigJson);

			ITypeResolver typeResolver = context.Resolve<ITypeResolver>() ?? throw new Exception("Failed to resolve an instance of ITypeResolver");

			Type configType = typeResolver.GetType(instanceTypeConfig.InstanceConfigType);
			Type instanceType = typeResolver.GetType(instanceTypeConfig.InstanceType);

			object config = JsonConvert.DeserializeObject(configJson, configType);

			List<ParameterInfo> instanceParamTypes = instanceType.GetConstructors()
			                                                     .First()
			                                                     .GetParameters()
			                                                     .ToList();

			List<object> instanceParams = instanceParamTypes
			                              .Select(t => t.ParameterType == configType ? config : context.Resolve(t.ParameterType))
			                              .ToList();

			var instance =  Activator.CreateInstance(instanceType, instanceParams.ToArray());

			return instance as T;
		}
	}
}
