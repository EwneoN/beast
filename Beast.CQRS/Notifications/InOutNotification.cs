﻿using MediatR;

namespace Beast.CQRS.Notifications
{
	public class InOutNotification<TIn, TOut> : INotification
	{
		public TIn In { get; }
		public TOut Out { get; }

		public InOutNotification(TIn @in, TOut @out)
		{
			In = @in;
			Out = @out;
		}
	}
}
