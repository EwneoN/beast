﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Services.Ids;
using Beast.Services.Time;
using MediatR;

namespace Beast.CQRS.Behaviours
{
	public class DefaultRequestBehaviour<TRequest, TResponse> : IPipelineBehaviour<TRequest, TResponse>
		where TRequest : Requests.IRequest<TResponse> 
		where TResponse : IResponse
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;

		public DefaultRequestBehaviour(IIdService idService, ITimeService timeService)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
		}

		public async Task<TResponse> 
			Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
		{
			request.RequestId = await _IdService.GetNewId();
			request.Received = _TimeService.UtcNow;
			return await next();
		}
	}
}
