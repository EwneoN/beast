﻿using Beast.CQRS.Requests;

namespace Beast.CQRS.Behaviours
{
	public interface IPipelineBehaviour<in TRequest, TResponse> : MediatR.IPipelineBehavior<TRequest, TResponse>
		where TRequest : IRequest<TResponse>
		where TResponse : IResponse
	{ }
}
