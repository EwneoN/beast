﻿using System;

namespace Beast.CQRS.Requests
{
	public interface IRequest
	{
		long RequestId { get; set; }
		DateTimeOffset Received { get; set; }
	}

	public interface IRequest<out TResponse> 
		: MediatR.IRequest<TResponse>, IRequest where TResponse : IResponse { }
}