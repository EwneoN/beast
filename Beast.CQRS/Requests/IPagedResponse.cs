﻿namespace Beast.CQRS.Requests
{
	public interface IPagedResponse<out T>
	{
		int PageNumber { get; }
		int PageSize { get; }
		int TotalItems { get; }
		int TotalPages { get; }
		T[] Items { get; }
	}
}