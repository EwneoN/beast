﻿using System;
using System.Linq;
using Beast.Collections;

namespace Beast.CQRS.Requests
{
	public class PagedResponse<T> : IResponse, IPagedResponse<T>
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }

		public int PageNumber { get; }
		public int PageSize { get; }
		public int TotalItems { get; }
		public int TotalPages { get; }
		public T[] Items { get; }

		public PagedResponse(int pageNumber, int pageSize, int totalItems, int totalPages, T[] items)
		{
			PageNumber = pageNumber;
			PageSize = pageSize;
			TotalItems = totalItems;
			TotalPages = totalPages;
			Items = items ?? throw new ArgumentNullException(nameof(items));
		}

		public PagedResponse(IPagedCollection<T> pagedCollection)
		{
			if (pagedCollection == null)
			{
				throw new ArgumentNullException(nameof(pagedCollection));
			}

			if (pagedCollection.Items == null)
			{
				throw new ArgumentNullException(nameof(pagedCollection.Items));
			}

			PageNumber = pagedCollection.PageNumber;
			PageSize = pagedCollection.PageSize;
			TotalItems = pagedCollection.TotalItems;
			TotalPages = pagedCollection.TotalPages;
			Items = pagedCollection.Items.ToArray();
		}
	}
}