﻿using System;

namespace Beast.CQRS.Requests
{
	public interface IResponse
	{
		long RequestId { get; set; }
		long ResponseId { get; set; }
		DateTimeOffset Received { get; set; }
	}
}
