﻿using System;

namespace Beast.CQRS.Requests
{
	public class UnitResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
	}
}