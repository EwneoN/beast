﻿using Beast.Collections;

namespace Beast.CQRS.Requests
{
	public interface IPagedRequest<out TResponse> : IRequest<TResponse>
		where TResponse : IResponse
	{
		IPagedQueryParams QueryParams { get; }
	}
}
