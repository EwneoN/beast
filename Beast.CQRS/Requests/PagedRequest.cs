﻿using System;
using Beast.Collections;

namespace Beast.CQRS.Requests {
	public class PagedRequest<TResponse> : IPagedRequest<TResponse>
		where TResponse : IResponse
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IPagedQueryParams QueryParams { get; }

		public PagedRequest(IPagedQueryParams queryParams)
		{
			QueryParams = queryParams;
		}
	}
}