﻿using System;
using System.Net;
using System.Threading.Tasks;
using Beast.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Beast.Web.Middleware
{
	public class ExceptionMiddleware
	{
		private readonly RequestDelegate _Next;

		public ExceptionMiddleware(RequestDelegate next)
		{
			_Next = next;
		}

		public async Task InvokeAsync(HttpContext httpContext)
		{
			try
			{
				await _Next(httpContext);
			}
			catch (RecordNotFoundException ex)
			{
				await HandleRecordNotFoundExceptionAsync(httpContext, ex);
			}
			catch (AuthenticationException ex)
			{
				await HandleAuthenticationExceptionAsync(httpContext, ex);
			}
			catch (UnauthorisedException ex)
			{
				await HandleUnauthorisedExceptionAsync(httpContext, ex);
			}
			catch (Exception)
			{
				await HandleExceptionAsync(httpContext);
			}
		}

		private static Task HandleRecordNotFoundExceptionAsync(HttpContext context, RecordNotFoundException exception)
		{
			context.Response.ContentType = "application/json";
			context.Response.StatusCode = (int)HttpStatusCode.NotFound;

			return context.Response.WriteAsync(new ErrorDetails
			{
				StatusCode = context.Response.StatusCode,
				Message = exception.Message
			}.ToString());
		}

		private static Task HandleUnauthorisedExceptionAsync(HttpContext context, UnauthorisedException exception)
		{
			context.Response.ContentType = "application/json";
			context.Response.StatusCode = (int)HttpStatusCode.Forbidden;

			return context.Response.WriteAsync(new ErrorDetails
			{
				StatusCode = context.Response.StatusCode,
				Message = exception.Message
			}.ToString());
		}

		private static Task HandleAuthenticationExceptionAsync(HttpContext context, AuthenticationException exception)
		{
			context.Response.ContentType = "application/json";
			context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

			return context.Response.WriteAsync(new ErrorDetails
			{
				StatusCode = context.Response.StatusCode,
				Message = exception.Message
			}.ToString());
		}

		private static Task HandleExceptionAsync(HttpContext context)
		{
			context.Response.ContentType = "application/json";
			context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

			return context.Response.WriteAsync(new ErrorDetails
			{
				StatusCode = context.Response.StatusCode,
				Message = "Internal Server Error"
			}.ToString());
		}

		private class ErrorDetails
		{
			public int StatusCode { get; set; }
			public string Message { get; set; }

			public override string ToString()
			{
				return JsonConvert.SerializeObject(this);
			}
		}
	}
}