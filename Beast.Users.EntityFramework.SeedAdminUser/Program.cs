﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Beast.AutoFac;
using Beast.Azure.KeyVault;
using Beast.Dynamics;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.DTOs.Users;
using Beast.Users.EntityFramework.Repositories;
using Beast.Users.Services;
using IdGen;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Serilog.Events;

namespace Beast.Users.EntityFramework.SeedAdminUser
{
	public class Program
	{
		public static void Main()
		{
			Task.WaitAll(MainAsync());
		}

		public static async Task MainAsync()
		{
			Log.Logger = new LoggerConfiguration()
			             .MinimumLevel.Debug()
			             .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
			             .Enrich.FromLogContext()
			             .WriteTo.Console()
			             .WriteTo.RollingFile("log-{Date}.txt")
			             .CreateLogger();

			var settingsPath = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
			var configJsonString = File.ReadAllText(settingsPath);
			var configJson = JObject.Parse(configJsonString);
			var vaultBaseUrl = configJson.SelectToken("$.KeyVault.BaseUrl").Value<string>();

			var azureConfigProvider = new AzureKeyVaultConfigurationProvider(new AzureKeyVaultSecretManager(),
			                                                         Log.Logger,
			                                                         vaultBaseUrl);

			await azureConfigProvider.LoadAsync(); 

			ConfigurationRoot root = new ConfigurationRoot(new List<IConfigurationProvider>
			{
				azureConfigProvider,
				new JsonConfigurationProvider(new JsonConfigurationSource
				{
					FileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory()),
					Path = "appsettings.json"
				})
			});

			ContainerBuilder containerBuilder = new ContainerBuilder();
			containerBuilder.RegisterModule(new UsersConsoleModule(root));

			string authId;
			string email;
			string firstName;
			string lastName;
			string dateOfBirthString;
			DateTimeOffset dateOfBirth;

			do
			{
				Console.WriteLine("Enter authId:");
				authId = Console.ReadLine();
			} while (string.IsNullOrWhiteSpace(authId));

			do
			{
				Console.WriteLine("Enter email:");
				email = Console.ReadLine();
			} while (string.IsNullOrWhiteSpace(email));

			do
			{
				Console.WriteLine("Enter firstName:");
				firstName = Console.ReadLine();
			} while (string.IsNullOrWhiteSpace(firstName));

			do
			{
				Console.WriteLine("Enter lastName:");
				lastName = Console.ReadLine();
			} while (string.IsNullOrWhiteSpace(lastName));

			do
			{
				Console.WriteLine("Enter dateOfBirth:");
				dateOfBirthString = Console.ReadLine();
			} while (string.IsNullOrWhiteSpace(dateOfBirthString) || !DateTimeOffset.TryParse(dateOfBirthString, out dateOfBirth));

			Console.WriteLine("Enter middleName:");
			string middleName = Console.ReadLine();

			IContainer container = containerBuilder.Build();

			var usersService = container.Resolve<IUsersService>();
			try
			{

				var user = await usersService.SaveSignedUpUser(new SignedUpUser
				{
					AccountType = UserIdentityType.LocalAccount,
					AuthId = authId,
					EmailAddress = email,
					FirstName = firstName,
					MiddleName = middleName,
					LastName = lastName,
					DateOfBirth = dateOfBirth
				}, CancellationToken.None);
			}
			catch (Exception ex)
			{
				Log.Logger.Error(ex, "Oops!");
				throw;
			}

			Console.ReadLine();
		}
	}
}
