﻿using System;
using System.Linq;
using System.Security.Claims;
using Autofac;
using Beast.Requests;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.AutoFac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Beast.Users.EntityFramework.SeedAdminUser
{
	public class UsersConsoleModule : UsersModule<StandardIdService, StandardTimeService>
	{
		public UsersConsoleModule(IConfiguration configuration)
			: base(configuration) { }

		protected override void PreRegisterAction(ContainerBuilder builder) { }

		protected override void PostRegisterAction(ContainerBuilder builder) { }
	}
}