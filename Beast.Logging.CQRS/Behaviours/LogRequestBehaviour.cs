﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Behaviours;
using Beast.CQRS.Requests;
using MediatR;
using Serilog;

namespace Beast.Logging.CQRS.Behaviours
{
	public class LogRequestBehaviour<TRequest, TResponse> : IPipelineBehaviour<TRequest, TResponse>
		where TRequest : Beast.CQRS.Requests.IRequest<TResponse>
		where TResponse : IResponse
	{
		private readonly ILogger _Logger;

		public LogRequestBehaviour(ILogger logger)
		{
			_Logger = logger;
		}

		public async Task<TResponse>
			Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			_Logger.Information("Request received: {@Request}", new
			{
				RequestType = typeof(TRequest).FullName,
				request.RequestId
			});

			try
			{
				TResponse response = await next();
				stopwatch.Stop();

				_Logger.Information("Request response: {@Response}", new
				{
					RequestType = typeof(TRequest).FullName,
					request.RequestId,
					ResponseType = typeof(TResponse).FullName,
					response.ResponseId,
					stopwatch.ElapsedMilliseconds
				});
				return response;
			}
			catch (Exception ex)
			{
				stopwatch.Stop();
				_Logger.Error(ex, "Request error: {@Request}", new
				{
					RequestType = typeof(TRequest).FullName,
					request.RequestId,
					ex.Message,
					stopwatch.ElapsedMilliseconds
				});
				throw;
			}
		}
	}
}