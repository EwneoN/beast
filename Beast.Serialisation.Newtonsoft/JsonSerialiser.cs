﻿using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Beast.Serialisation.Newtonsoft
{
	public class JsonSerialiser : ISerialiser
	{
		public async Task<string> SerialiseToString<T>(T dataToSerialise)
		{
			return await Task.FromResult(JsonConvert.SerializeObject(dataToSerialise));
		}

		public async Task<byte[]> SerialiseToByteArray<T>(T dataToSerialise)
		{
			return await Task.FromResult(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(dataToSerialise)));
		}

		public async Task<T> DeserialiseFromString<T>(string dataToDeserialise)
		{
			return await Task.FromResult(JsonConvert.DeserializeObject<T>(dataToDeserialise));
		}

		public async Task<T> DeserialiseFromByteArray<T>(byte[] dataToDeserialise)
		{
			return await Task.FromResult(JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(dataToDeserialise)));
		}
	}
}
