﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace Beast.Dynamics.DotNetFramework {
	public class DynamicTypeGenerator : IDynamicTypeGenerator
	{
		private static readonly Dictionary<Type, Type> _DynamicTypes;
		private static readonly ReaderWriterLockSlim _DynamicTypesLock;

		static DynamicTypeGenerator()
		{
			_DynamicTypes = new Dictionary<Type, Type>();
			_DynamicTypesLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
		}

		public Type GetDynamicType<T>()
		{
			Type dynamicType;
			Type dynamicTypeKey = typeof(T);

			_DynamicTypesLock.EnterUpgradeableReadLock();

			try
			{
				if (_DynamicTypes.ContainsKey(dynamicTypeKey))
				{
					dynamicType = _DynamicTypes[dynamicTypeKey];

					_DynamicTypesLock.ExitReadLock();
				}
				else
				{
					_DynamicTypesLock.EnterWriteLock();

					try
					{
						dynamicType = GenerateDynamicType<T>();

						_DynamicTypes.Add(dynamicTypeKey, dynamicType);
					}
					finally
					{
						_DynamicTypesLock.ExitWriteLock();
					}
				}
			}
			finally
			{
				_DynamicTypesLock.ExitUpgradeableReadLock();
			}

			return dynamicType;
		}

		private static Type GenerateDynamicType<T>()
		{
			Type dynamicTypeBase = typeof(T);
			PropertyInfo[] writableProperties = dynamicTypeBase
			                                    .GetProperties(BindingFlags.Public | BindingFlags.FlattenHierarchy)
			                                    .Where(p => p.CanWrite)
			                                    .ToArray();

			string dynamicTypeName = GetDynamicTypeName(dynamicTypeBase);
			TypeBuilder typeBuilder = GetTypeBuilder(dynamicTypeName);

			if (dynamicTypeBase.IsInterface)
			{
				typeBuilder.AddInterfaceImplementation(dynamicTypeBase);
			}
			else
			{
				typeBuilder.SetParent(dynamicTypeBase);
			}

			foreach (PropertyInfo property in writableProperties)
			{
				CreateProperty(typeBuilder, property.Name, property.PropertyType);
			}

			return typeBuilder.CreateType();
		}

		private static string GetDynamicTypeName(Type dynamicTypeBase)
		{
			if (dynamicTypeBase.IsInterface)
			{
				return dynamicTypeBase.Name.StartsWith("I", StringComparison.OrdinalIgnoreCase)
					       ? $"Dynamic{dynamicTypeBase.Name.TrimStart('I')}"
					       : $"Dynamic{dynamicTypeBase.Name}";
			}

			if (dynamicTypeBase.IsAbstract)
			{
				return dynamicTypeBase.Name.EndsWith("Base", StringComparison.OrdinalIgnoreCase)
					       ? $"Dynamic{dynamicTypeBase.Name.Substring(0, dynamicTypeBase.Name.LastIndexOf("Base", StringComparison.OrdinalIgnoreCase))}"
					       : $"Dynamic{dynamicTypeBase.Name}";
			}

			return $"Dynamic{dynamicTypeBase.Name}";
		}

		private static TypeBuilder GetTypeBuilder(string typeName)
		{
			var typeSignature = typeName;
			var an = new AssemblyName(typeSignature);
			AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(an, AssemblyBuilderAccess.Run);
			ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("DynamicBeastModule");
			TypeBuilder tb = moduleBuilder.DefineType(typeSignature,
			                                          TypeAttributes.Public |
			                                          TypeAttributes.Class |
			                                          TypeAttributes.AutoClass |
			                                          TypeAttributes.AnsiClass |
			                                          TypeAttributes.BeforeFieldInit |
			                                          TypeAttributes.AutoLayout,
			                                          null);
			return tb;
		}

		private static void CreateProperty(TypeBuilder tb, string propertyName, Type propertyType)
		{
			FieldBuilder fieldBuilder = tb.DefineField("_" + propertyName, propertyType, FieldAttributes.Private);

			PropertyBuilder propertyBuilder = tb.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, null);
			MethodBuilder getPropMethodBuilder = tb.DefineMethod("get_" + propertyName, MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, propertyType, Type.EmptyTypes);
			ILGenerator getIl = getPropMethodBuilder.GetILGenerator();

			getIl.Emit(OpCodes.Ldarg_0);
			getIl.Emit(OpCodes.Ldfld, fieldBuilder);
			getIl.Emit(OpCodes.Ret);

			MethodBuilder setPropMethodBuilder =
				tb.DefineMethod("set_" + propertyName,
				                MethodAttributes.Public |
				                MethodAttributes.SpecialName |
				                MethodAttributes.HideBySig,
				                null, new[] { propertyType });

			ILGenerator setIl = setPropMethodBuilder.GetILGenerator();
			Label modifyProperty = setIl.DefineLabel();
			Label exitSet = setIl.DefineLabel();

			setIl.MarkLabel(modifyProperty);
			setIl.Emit(OpCodes.Ldarg_0);
			setIl.Emit(OpCodes.Ldarg_1);
			setIl.Emit(OpCodes.Stfld, fieldBuilder);

			setIl.Emit(OpCodes.Nop);
			setIl.MarkLabel(exitSet);
			setIl.Emit(OpCodes.Ret);

			propertyBuilder.SetGetMethod(getPropMethodBuilder);
			propertyBuilder.SetSetMethod(setPropMethodBuilder);
		}
	}
}