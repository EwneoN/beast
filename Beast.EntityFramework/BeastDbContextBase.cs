﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Beast.Models;
using Beast.Services.Ids;
using Beast.Services.Time;
using Z.EntityFramework.Extensions;

namespace Beast.EntityFramework
{
	public abstract class BeastDbContextBase : DbContext, IBeastDbContext
	{
		public IIdService IdService { get; }
		public ITimeService TimeService { get; }

		protected BeastDbContextBase(IIdService idService, ITimeService timeService, BeastDbContextConfig config)
			: base(config?.NameOrConnectionString ?? throw new ArgumentNullException(nameof(config)))
		{
			this.Configuration.ProxyCreationEnabled = false;

			IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
		}

		public virtual async Task BulkUpdateAsync<T>(ICollection<T> entities)
			where T : BeastModel
		{
			await BulkUpdateAsync(entities, CancellationToken.None);
		}

		public virtual async Task BulkUpdateAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkUpdateAsync(this, entities, cancellationToken);
		}

		public virtual async Task BulkUpdateAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
			where T : BeastModel
		{
			await BulkUpdateAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public virtual async Task BulkUpdateAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkUpdateAsync(this, entities, bulkOperationFactory, cancellationToken);
		}

		public virtual async Task BulkInsertAsync<T>(ICollection<T> entities)
			where T : BeastModel
		{
			await BulkInsertAsync(entities, CancellationToken.None);
		}

		public virtual async Task BulkInsertAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Id = await IdService.GetNewId();
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkInsertAsync(this, entities, cancellationToken);
		}

		public virtual async Task BulkInsertAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
			where T : BeastModel
		{
			await BulkInsertAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public virtual async Task BulkInsertAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Id = await IdService.GetNewId();
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkInsertAsync(this, entities, bulkOperationFactory, cancellationToken);
		}

		public virtual async Task BulkMergeAsync<T>(ICollection<T> entities)
			where T : BeastModel
		{
			await BulkMergeAsync(entities, CancellationToken.None);
		}

		public virtual async Task BulkMergeAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkMergeAsync(this, entities, cancellationToken);
		}

		public virtual async Task BulkMergeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
			where T : BeastModel
		{
			await BulkMergeAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public virtual async Task BulkMergeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkMergeAsync(this, entities, bulkOperationFactory, cancellationToken);
		}

		public virtual async Task BulkDeleteAsync<T>(ICollection<T> entities)
			where T : BeastModel
		{
			await BulkDeleteAsync(entities, CancellationToken.None);
		}

		public virtual async Task BulkDeleteAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
			where T : BeastModel
		{
			await DbContextExtensions.BulkDeleteAsync(this, entities, cancellationToken);
		}

		public virtual async Task BulkDeleteAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
			where T : BeastModel
		{
			await BulkDeleteAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public virtual async Task BulkDeleteAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
			where T : BeastModel
		{
			await DbContextExtensions.BulkDeleteAsync(this, entities, bulkOperationFactory, cancellationToken);
		}

		public virtual async Task BulkSynchronizeAsync<T>(ICollection<T> entities)
			where T : BeastModel
		{
			await BulkSynchronizeAsync(entities, CancellationToken.None);
		}

		public virtual async Task BulkSynchronizeAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkSynchronizeAsync(this, entities, cancellationToken);
		}

		public virtual async Task BulkSynchronizeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
			where T : BeastModel
		{
			await BulkSynchronizeAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public virtual async Task BulkSynchronizeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
			where T : BeastModel
		{
			foreach (T entity in entities)
			{
				entity.Timestamp = TimeService.UtcNow;
			}

			await DbContextExtensions.BulkSynchronizeAsync(this, entities, bulkOperationFactory, cancellationToken);
		}
	}
}