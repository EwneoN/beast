﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Beast.Models;
using Beast.Services.Ids;
using Beast.Services.Time;

namespace Beast.EntityFramework
{
	public abstract class BeastDbContext : BeastDbContextBase
	{
		protected BeastDbContext(IIdService idService, ITimeService timeService, BeastDbContextConfig config)
			: base(idService, timeService, config) { }

		public override int SaveChanges()
		{
			foreach (DbEntityEntry<BeastModel> entry in ChangeTracker.Entries<BeastModel>())
			{
				if (entry.State == EntityState.Added)
				{
					Task<long> task = IdService.GetNewId();
					Task.WaitAll(task);

					entry.Entity.Id = task.Result;
				}

				entry.Entity.Timestamp = TimeService.UtcNow;
			}

			return base.SaveChanges();
		}

		public override async Task<int> SaveChangesAsync()
		{
			return await SaveChangesAsync(CancellationToken.None);
		}

		public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
		{
			foreach (DbEntityEntry<BeastModel> entry in ChangeTracker.Entries<BeastModel>())
			{
				if (entry.State == EntityState.Added)
				{
					entry.Entity.Id = await IdService.GetNewId();
				}

				entry.Entity.Timestamp = TimeService.UtcNow;
			}

			return await base.SaveChangesAsync(cancellationToken);
		}

		protected void OnModelCreating(DbModelBuilder modelBuilder, params Assembly[] assemblies)
		{
			base.OnModelCreating(modelBuilder);

			if (assemblies == null || !assemblies.Any())
			{
				return;
			}

			foreach (Assembly assembly in assemblies)
			{
				modelBuilder.Configurations.AddFromAssembly(assembly);
			}
		}
	}
}