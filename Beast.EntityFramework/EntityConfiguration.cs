﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Beast.EntityFramework
{
	public class EntityConfiguration<TEntity> : EntityTypeConfiguration<TEntity>
		where TEntity : class
	{
		public const string DefaultSchema = "beast";

		public string Schema { get; }

		public EntityConfiguration(string schema)
		{
			if (string.IsNullOrWhiteSpace(schema))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(schema));
			}

			Schema = schema;
		}
	}
}