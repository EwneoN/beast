﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace Beast.EntityFramework
{
  public interface IBeastDbContext
  {
    Database Database { get; }
    DbChangeTracker ChangeTracker { get; }
    DbContextConfiguration Configuration { get; }
    int SaveChanges();
    Task<int> SaveChangesAsync();
    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
	}
}
