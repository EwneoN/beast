﻿using Microsoft.Extensions.Configuration;
using Serilog;

namespace Beast.Azure.KeyVault
{
	public static class AzureKeyVaultConfigurationExtensions
	{
		public static IConfigurationBuilder AddAzureKeyVault(
			this IConfigurationBuilder configurationBuilder, string vaultBaseUrl)
		{
			return configurationBuilder.Add(new AzureKeyVaultConfigurationSource(Log.Logger, vaultBaseUrl));
		}

		public static IConfigurationBuilder AddAzureKeyVault(
			this IConfigurationBuilder configurationBuilder, ILogger logger, string vaultBaseUrl)
		{
			return configurationBuilder.Add(new AzureKeyVaultConfigurationSource(logger, vaultBaseUrl));
		}
	}
}
