﻿using System;
using Microsoft.Extensions.Primitives;

namespace Beast.Azure.KeyVault
{
	public class AzureKeyVaultChangeToken : IChangeToken
	{
		public bool HasChanged => false;
		public bool ActiveChangeCallbacks => false;

		public IDisposable RegisterChangeCallback(Action<object> callback, object state) => EmptyDisposable.Instance;

		internal class EmptyDisposable : IDisposable
		{
			public static EmptyDisposable Instance { get; } = new EmptyDisposable();
			private EmptyDisposable() { }
			public void Dispose() { }
		}
	}
}