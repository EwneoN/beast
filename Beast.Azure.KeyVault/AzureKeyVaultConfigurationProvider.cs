﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Beast.Azure.KeyVault
{
	public class AzureKeyVaultConfigurationProvider : ConfigurationProvider
	{
		private readonly AzureKeyVaultSecretManager _Manager;
		private readonly ILogger _Logger;
		private readonly string _VaultBaseUrl;

		public AzureKeyVaultConfigurationProvider(AzureKeyVaultSecretManager manager, string vaultBaseUrl)
		{
			if (string.IsNullOrWhiteSpace(vaultBaseUrl))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(vaultBaseUrl));
			}

			_Manager = manager ?? throw new ArgumentNullException(nameof(manager));
			_Logger = Log.Logger ?? throw new ArgumentNullException(nameof(Log.Logger));
			_VaultBaseUrl = vaultBaseUrl;
		}

		public AzureKeyVaultConfigurationProvider(AzureKeyVaultSecretManager manager, ILogger logger, string vaultBaseUrl)
		{
			if (string.IsNullOrWhiteSpace(vaultBaseUrl))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(vaultBaseUrl));
			}

			_Manager = manager ?? throw new ArgumentNullException(nameof(manager));
			_Logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_VaultBaseUrl = vaultBaseUrl;
		}

		public override void Load() => LoadAsync().ConfigureAwait(false).GetAwaiter().GetResult();

		public async Task LoadAsync()
		{
			try
			{
				var client = GetClient();
				var secrets = await client.GetSecretsAsync(_VaultBaseUrl).ConfigureAwait(false);
				var data = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

				do
				{
					foreach (var secretItem in secrets)
					{
						if (!_Manager.Load(secretItem) || (secretItem.Attributes?.Enabled != true))
						{
							continue;
						}

						var value = await client.GetSecretAsync(secretItem.Id).ConfigureAwait(false);
						var key = _Manager.GetKey(value);
						data.Add(key, value.Value);
					}

					secrets = secrets.NextPageLink != null ?
						await client.GetSecretsNextAsync(secrets.NextPageLink).ConfigureAwait(false) :
						null;
				} while (secrets != null);

				Data = data;
			}
			catch (Exception ex)
			{
				_Logger.Error(ex, "An exception was thrown while loading secrets from the Azure Key Vault");
				throw;
			}
		}

		public static KeyVaultClient GetClient()
		{
			AzureServiceTokenProvider tokenProvider = new AzureServiceTokenProvider();
			var callback = new KeyVaultClient.AuthenticationCallback(tokenProvider.KeyVaultTokenCallback);
			var client = new KeyVaultClient(callback);
			return client;
		}
	}
}