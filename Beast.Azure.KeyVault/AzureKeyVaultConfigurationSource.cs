﻿using System;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;

namespace Beast.Azure.KeyVault
{
	public class AzureKeyVaultConfigurationSource : IConfigurationSource
	{
		private readonly ILogger _Logger;
		private readonly string _VaultBaseUrl;

		public AzureKeyVaultConfigurationSource(string vaultBaseUrl)
		{
			if (string.IsNullOrWhiteSpace(vaultBaseUrl))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(vaultBaseUrl));
			}

			_Logger = Log.Logger ?? throw new ArgumentNullException(nameof(Log.Logger));
			_VaultBaseUrl = vaultBaseUrl;
		}

		public AzureKeyVaultConfigurationSource(ILogger logger, string vaultBaseUrl)
		{
			if (string.IsNullOrWhiteSpace(vaultBaseUrl))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(vaultBaseUrl));
			}

			_Logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_VaultBaseUrl = vaultBaseUrl;
		}

		public IConfigurationProvider Build(IConfigurationBuilder config)
		{
			return new AzureKeyVaultConfigurationProvider(new AzureKeyVaultSecretManager(),
																										_Logger,
			                                              _VaultBaseUrl);
		}
	}
}
