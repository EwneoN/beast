﻿using Microsoft.Azure.KeyVault.Models;
using Microsoft.Extensions.Configuration;

namespace Beast.Azure.KeyVault
{
	public class AzureKeyVaultSecretManager
	{
		/// <inheritdoc />
		public virtual string GetKey(SecretBundle secret)
		{
			return secret.SecretIdentifier.Name.Replace("--", ConfigurationPath.KeyDelimiter);
		}

		/// <inheritdoc />
		public virtual bool Load(SecretItem secret)
		{
			return true;
		}
	}
}