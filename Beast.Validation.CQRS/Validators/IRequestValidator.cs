﻿using Beast.CQRS.Requests;

namespace Beast.Validation.CQRS.Validators
{
	public interface IRequestValidator<in TRequest> : FluentValidation.IValidator<TRequest>
		where TRequest : IRequest
	{

	}
}
