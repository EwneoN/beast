﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Behaviours;
using Beast.CQRS.Requests;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace Beast.Validation.CQRS.Behaviours
{
	public class ValidationBehaviour<TRequest, TResponse> : IPipelineBehaviour<TRequest, TResponse>
		where TRequest : Beast.CQRS.Requests.IRequest<TResponse>
		where TResponse : IResponse
	{
		private readonly ICollection<Validators.IRequestValidator<TRequest>> _Validators;

		public ValidationBehaviour(ICollection<Validators.IRequestValidator<TRequest>> validators)
		{
			_Validators = validators ?? throw new ArgumentNullException(nameof(validators));
		}

		public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
		{
			ValidationContext context = new ValidationContext(request);

			ValidationFailure[] failures = _Validators
			               .Select(v => v.Validate(context))
			               .SelectMany(v => v.Errors)
			               .Where(e => e != null)
			               .ToArray();

			if (failures.Any())
			{
				throw new ValidationException(failures);
			}

			return await next();
		}
	}
}