﻿using System;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Users.CQRS.Requests.Users.CreateNewUser;
using Beast.Users.CQRS.Requests.Users.DeleteUser;
using Beast.Users.CQRS.Requests.Users.GetUser;
using Beast.Users.CQRS.Requests.Users.GetUserAuthId;
using Beast.Users.CQRS.Requests.Users.GetUserByAuthId;
using Beast.Users.CQRS.Requests.Users.GetUserByUsername;
using Beast.Users.CQRS.Requests.Users.GetUserId;
using Beast.Users.CQRS.Requests.Users.GetUsername;
using Beast.Users.CQRS.Requests.Users.GetUsers;
using Beast.Users.CQRS.Requests.Users.UpdateUser;
using Beast.Users.DTOs.Users;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Beast.Users.API.Controllers.v0
{
	[Route("api/v0/users")]
	[ApiController]
	[Produces("application/json", "application/xml")]
	public class UsersController : ControllerBase
	{
		private readonly IMediator _Mediator;

		public UsersController(IMediator mediator)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
		}

		[HttpGet("get-users")]
		public async Task<IActionResult> GetUsers([FromQuery]PagedQueryParams queryParams)
		{
			if (queryParams == null)
			{
				return BadRequest();
			}

			var response = await _Mediator.Send(new GetUsersRequest(queryParams));

			return Ok(response);
		}

		[HttpGet("{userId}")]
		[Produces(typeof(IUserInfo))]
		public async Task<IActionResult> Get(long userId)
		{
			var response = await _Mediator.Send(new GetUserRequest(userId));

			return Ok(response.UserInfo);
		}

		[HttpGet("get-user-by-username")]
		[Produces(typeof(IUserInfo))]
		public async Task<IActionResult> GetUserByUsername([FromQuery]string username)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				return BadRequest();
			}

			var response = await _Mediator.Send(new GetUserByUsernameRequest(username));

			return Ok(response.UserInfo);
		}

		[HttpGet("get-user-by-auth-id")]
		[Produces(typeof(IUserInfo))]
		public async Task<IActionResult> GetUserByAuthId([FromQuery]string authId)
		{
			if (string.IsNullOrWhiteSpace(authId))
			{
				return BadRequest();
			}

			var response = await _Mediator.Send(new GetUserByAuthIdRequest(authId));

			return Ok(response.UserInfo);
		}

		[HttpGet("{username}/userid")]
		[Produces(typeof(long))]
		public async Task<IActionResult> GetUserId([FromQuery]string username)
		{
			var response = await _Mediator.Send(new GetUserIdRequest(username));

			return Ok(response.UserId);
		}

		[HttpGet("{userId}/authid")]
		[Produces(typeof(string))]
		public async Task<IActionResult> GetAuthId(long userId)
		{
			var response = await _Mediator.Send(new GetUserAuthIdRequest(userId));

			return Ok(response.AuthId);
		}

		[HttpGet("{userId}/username")]
		[Produces(typeof(string))]
		public async Task<IActionResult> GetUsername(long userId)
		{
			var response = await _Mediator.Send(new GetUsernameRequest(userId));

			return Ok(response.Username);
		}

		[HttpPost]
		[Produces(typeof(IUserInfo))]
		public async Task<IActionResult> Post([FromBody]NewUser newUser)
		{
			if (newUser == null)
			{
				return BadRequest();
			}

			var response = await _Mediator.Send(new CreateNewUserRequest(newUser));

			return Ok(response.UserInfo);
		}

		[HttpPut]
		[Produces(typeof(IUserInfo))]
		public async Task<IActionResult> Put([FromBody]UserUpdate update)
		{
			if (update == null)
			{
				return BadRequest();
			}

			var response = await _Mediator.Send(new UpdateUserRequest(update));

			return Ok(response.UserInfo);
		}

		[HttpDelete("{userId}")]
		public async Task<IActionResult> Delete(long userId)
		{
			await _Mediator.Send(new DeleteUserRequest(userId));

			return Ok();
		}
	}
}
