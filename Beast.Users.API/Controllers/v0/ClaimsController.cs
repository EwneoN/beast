﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Beast.Users.CQRS.Requests.Claims.GetAllClaims;
using Beast.Users.DTOs.Claims;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Beast.Users.API.Controllers.v0
{
	[Route("api/v0/claims")]
	[ApiController]
	[Produces("application/json", "application/xml")]
	public class ClaimsController : ControllerBase
	{
		private readonly IMediator _Mediator;

		public ClaimsController(IMediator mediator)
		{
			_Mediator = mediator;
		}

		[HttpGet]
		[Produces(typeof(ICollection<IClaimInfo>))]
		public async Task<IActionResult> Get()
		{
			var response = await _Mediator.Send(new GetAllClaimsRequest());

			return Ok(response.Claims);
		}
	}
}