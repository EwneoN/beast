﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Beast.Users.CQRS.Requests.UserClaims.GetUserClaims;
using Beast.Users.DTOs.UserClaims;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Beast.Users.API.Controllers.v0
{
	[Route("api/v0/user-claims")]
	[ApiController]
	[Produces("application/json", "application/xml")]
	public class UserClaimsController : ControllerBase
	{
		private readonly IMediator _Mediator;

		public UserClaimsController(IMediator mediator)
		{
			_Mediator = mediator;
		}

		[HttpGet]
		[Produces(typeof(ICollection<IUserClaimInfo>))]
		public async Task<IActionResult> Get([FromQuery]long userId)
		{
			var response = await _Mediator.Send(new GetUserClaimsRequest(userId));

			return Ok(response.UserClaims);
		}
	}
}