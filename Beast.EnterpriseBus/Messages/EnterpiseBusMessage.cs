﻿using System;

namespace Beast.EnterpriseBus.Messages
{
	public class EnterpiseBusMessage : IEnterpriseBusMessage
	{
		public long EnterpriseBusMessageId { get; set; }
		public long SenderId { get; set; }
		public long? ReceiverId { get; set; }
		public DateTimeOffset CreatedTimestamp { get; }
		public DateTimeOffset? SentTimestamp { get; set; }
		public DateTimeOffset? ReceivedTimestamp { get; set; }
		public string Exchange { get; set; }
		public string Topic { get; set; }

		public EnterpiseBusMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp)
		{
			EnterpriseBusMessageId = enterpriseBusMessageId;
			SenderId = senderId;
			CreatedTimestamp = createdTimestamp;
		}
	}
}