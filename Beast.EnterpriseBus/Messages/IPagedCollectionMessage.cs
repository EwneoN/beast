﻿using Beast.Collections;

namespace Beast.EnterpriseBus.Messages
{
	public interface IPagedCollectionMessage : IEnterpriseBusMessage
	{
		IPagedQueryParams QueryParams { get; set; }
	}
}