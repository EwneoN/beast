﻿using System;

namespace Beast.EnterpriseBus.Messages
{
	public interface IEnterpriseBusMessage
	{
		long EnterpriseBusMessageId { get; set; }
		long SenderId { get; set; }
		long? ReceiverId { get; set; }
		DateTimeOffset CreatedTimestamp { get; }
		DateTimeOffset? SentTimestamp { get; set; }
		DateTimeOffset? ReceivedTimestamp { get; set; }
		string Topic { get; set; }
	}
}