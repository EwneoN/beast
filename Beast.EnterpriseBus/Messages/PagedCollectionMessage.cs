﻿using System;
using Beast.Collections;

namespace Beast.EnterpriseBus.Messages
{
	public class PagedCollectionMessage : EnterpiseBusMessage, IPagedCollectionMessage
	{
		public IPagedQueryParams QueryParams { get; set; }

		public PagedCollectionMessage(long enterpriseBusMessageId, long senderId, DateTimeOffset createdTimestamp, IPagedQueryParams queryParams) 
			: base(enterpriseBusMessageId, senderId, createdTimestamp)
		{
			QueryParams = queryParams;
		}
	}
}