﻿using Beast.EnterpriseBus.Messages;

namespace Beast.EnterpriseBus.Events
{
	public delegate void MessageReceivedEventHandler(object sender, IEnterpriseBusMessage message);
}
