﻿using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Messages;

namespace Beast.EnterpriseBus.Clients
{
	public class NullEnterpriseBusPublisher : IEnterpriseBusPublisher
	{
		public long EnterpriseBusClientId { get; }
		public string Name { get; }
		public string Host { get; }

		public void Start() { }

		public void Stop() { }

		public async Task PublishMessage(IEnterpriseBusMessage message, CancellationToken cancellationToken)
		{
			await Task.FromResult(1);
		}
	}

	public class NullEnterpriseBusPublisherConfig
	{
	}
}