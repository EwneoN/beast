﻿using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Messages;

namespace Beast.EnterpriseBus.Clients
{
	public interface IEnterpriseBusPublisher : IEnterpriseBusClient
	{
		Task PublishMessage(IEnterpriseBusMessage message, CancellationToken cancellationToken);
	}
}