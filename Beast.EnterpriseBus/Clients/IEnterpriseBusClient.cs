﻿using System.Threading;
using System.Threading.Tasks;

namespace Beast.EnterpriseBus.Clients
{
	public interface IEnterpriseBusClient
	{
		long EnterpriseBusClientId { get; }
		string Name { get; }
		string Host { get; }
		void Start();
		void Stop();
	}
}