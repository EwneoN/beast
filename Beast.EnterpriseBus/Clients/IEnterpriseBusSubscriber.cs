﻿using System;
using Beast.Events;

namespace Beast.EnterpriseBus.Clients
{
	public interface IEnterpriseBusSubscriber<TMessage> : IEnterpriseBusClient
	{
		event BeastEventHandler<TMessage> MessageReceived;
	}
}