﻿using System;

namespace Beast.EnterpriseBus.Exceptions
{
	public class ClientNotActivatedException : Exception
	{
		public ClientNotActivatedException() : base("Client has not been activated yet") { }
	}
}
