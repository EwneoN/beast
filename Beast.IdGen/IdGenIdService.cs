﻿using System;
using System.Threading.Tasks;
using Beast.Services.Ids;
using IdGen;

namespace Beast.IdGen
{
	public class IdGenIdService : IIdService
	{
		private readonly IdGenerator _IdGenerator;

		public IdGenIdService(IdGenerator idGenerator)
		{
			_IdGenerator = idGenerator ?? throw new ArgumentNullException(nameof(idGenerator));
		}

		public async Task<long> GetNewId()
		{
			return await Task.FromResult(_IdGenerator.CreateId());
		}
	}
}
