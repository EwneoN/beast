﻿Place configuaration files for all injected types in this folder.
Each injected type should have 2 files:
1.	The Instance Type Config file indicating what type to instantiate and use for the injected type. 
		This file should be named as follows: {InjectedTypeNameWithoutAnyLeadingI}.json.
2.	The Instance Config file for the servicewhich has all the required configuration data.
		This file should be named as follows: {InjectedTypeNameWithoutAnyLeadingI}Config.json.

Example using IUsersContext:
		\Configuration\UsersContext.json				<- Instance Type Config (What type to use, and the type to use for config)
		{
			"InstanceType": "Beast.Users.EntityFramework.UsersContent",
			"ConfigType": "Beast.EntityFramework.BeastDbContextConfig"
		}

		\Configuration\UsersContextConfig.json	<- Instance Config (The actual config for the instatiated type to use)
		{
			"NameOrConnectionString": "Beast.Users" 
		}