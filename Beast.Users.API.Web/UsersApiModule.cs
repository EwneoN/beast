﻿using System;
using System.Linq;
using System.Security.Claims;
using Autofac;
using AzureADGraphClient;
using Beast.AutoFac;
using Beast.Encryption;
using Beast.EnterpriseBus.Clients;
using Beast.FileSystems;
using Beast.IdGen;
using Beast.Requests;
using Beast.Services.Time;
using Beast.Users.AutoFac;
using Beast.Users.EntityFramework;
using IdGen;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Beast.Users.API.Web
{
	public class UsersApiModule : UsersModule<IdGenIdService, StandardTimeService>
	{
		public UsersApiModule(IConfiguration configuration)
			: base(configuration) { }

		protected override void PreRegisterAction(ContainerBuilder builder)
		{
			var idGenParamName = typeof(IdGenerator).GetConstructors()
			                                        .First(c => c.GetParameters().Length == 1)
			                                        .GetParameters()
			                                        .First()
			                                        .Name;

			builder.RegisterType<IdGenerator>()
			       .As<IdGenerator>()
			       .WithParameter(idGenParamName, int.Parse(Configuration["IdGen:MachineId"]))
			       .InstancePerLifetimeScope();

			builder.Register(context =>
			       {
				       IHttpContextAccessor httpContextAccessor = context.Resolve<IHttpContextAccessor>();
				       HttpContext httpContext = httpContextAccessor?.HttpContext;
				       StringValues? authHeader = httpContext?.Request?.Headers["Authorization"];

				       if (!authHeader.HasValue)
				       {
					       return new AccessContext();
				       }

				       string authHeaderValue = authHeader.Value.FirstOrDefault();

				       if (string.IsNullOrWhiteSpace(authHeaderValue))
				       {
					       return new AccessContext();
				       }

				       string token = authHeaderValue.Substring(authHeaderValue.IndexOf(" ", StringComparison.Ordinal));

				       if (string.IsNullOrWhiteSpace(token))
				       {
					       return new AccessContext();
				       }

				       return new AccessContext
				       {
					       AuthId = httpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value,
					       IsAuthenticated = true
				       };
			       })
			       .As<IAccessContext>()
			       .InstancePerLifetimeScope();
		}

		protected override void PostRegisterAction(ContainerBuilder builder)
		{
			builder.RegisterConfigurableInstanceAs<B2CGraphClient>(nameof(B2CGraphClient), Configuration)
			       .SingleInstance();

			builder.RegisterConfigurableInstanceAs<IFileSystem>(nameof(IFileSystem).TrimStart('I'), Configuration)
			       .InstancePerLifetimeScope();

			builder.RegisterConfigurableInstanceAs<IEncryptionService>(nameof(IEncryptionService).TrimStart('I'),
			                                                           Configuration)
			       .SingleInstance();

			builder.RegisterConfigurableInstanceAs<IUsersContext>(nameof(IUsersContext).TrimStart('I'), Configuration)
			       .InstancePerLifetimeScope();

			builder.RegisterConfigurableInstanceAs<IEnterpriseBusPublisher>(nameof(IEnterpriseBusPublisher).TrimStart('I'),
			                                                                Configuration)
			       .InstancePerLifetimeScope();
		}
	}
}