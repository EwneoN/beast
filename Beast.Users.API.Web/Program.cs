﻿using System;
using Beast.Azure.KeyVault;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace Beast.Users.API.Web
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Log.Logger = new LoggerConfiguration()
#if DEBUG
			             .MinimumLevel.Debug()
#else
									 .MinimumLevel.Information()
#endif
									 .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
									 .Enrich.FromLogContext()
#if DEBUG
			             .WriteTo.Console()
#endif
									 .WriteTo.RollingFile(@"Logs\config-log-{Date}.txt")
			             .CreateLogger();

			CreateWebHostBuilder(args).Build().Run();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
						 .ConfigureAppConfiguration((context, config) =>
			       {
							 Log.Logger.Information("Loading configuration");

							 config.AddJsonFile("appsettings.json")
							       .AddEnvironmentVariables()
				             .AddCommandLine(args)
										 .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json");

				       var builtConfig = config.Build();
							 config.AddAzureKeyVault(Log.Logger, builtConfig["KeyVault:BaseUrl"]);
							 
				       Log.Logger.Information("Configuration loaded");
						 })
			       .ConfigureLogging((ctx, config) => {
				       config.ClearProviders();

							 Log.Logger = new LoggerConfiguration()
				                    .ReadFrom.Configuration(ctx.Configuration)
				                    .CreateLogger();
			       })
						 .UseSerilog()
						 .UseStartup<Startup>();
	}
}