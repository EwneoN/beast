﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using Beast.Users.API.Controllers.v0;
using Beast.Web.Middleware;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureADB2C.UI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

namespace Beast.Users.API.Web
{
	public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IServiceProvider ConfigureServices(IServiceCollection services)
		{
			Log.Logger.Information("Configuring services");

			ContainerBuilder containerBuilder = new ContainerBuilder();
			containerBuilder.RegisterLogger();
			
			var apiAssembly = typeof(UsersController).GetTypeInfo().Assembly;
			var assemblyPart = new AssemblyPart(apiAssembly);

			Log.Logger.Information("Configuring MVC");

			services.AddMvc()
							.ConfigureApplicationPartManager(apm => apm.ApplicationParts.Add(assemblyPart));

			Log.Logger.Information("Configuring authentication");

			services.AddAuthentication(AzureADB2CDefaults.BearerAuthenticationScheme)
			        .AddAzureADB2CBearer(options =>
			        {
				        var configJson = Configuration["Authentication:AzureADB2C"];
				        var config = JObject.Parse(configJson);

				        options.Instance = config["Instance"].Value<string>();
				        options.Domain = config["Domain"].Value<string>();
				        options.SignUpSignInPolicyId = config["SignUpSignInPolicyId"].Value<string>();
				        options.ResetPasswordPolicyId = config["ResetPasswordPolicyId"].Value<string>();
				        options.EditProfilePolicyId = config["EditProfilePolicyId"].Value<string>();
				        options.ClientId = config["ClientId"].Value<string>();
			        });

			Log.Logger.Information("Configuring swagger");

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc(Configuration["Swagger:Version"], new Info
				{
					Title = Configuration["Swagger:Title"],
					Description = Configuration["Swagger:Description"],
					Version = Configuration["Swagger:Version"],
					TermsOfService = Configuration["Swagger:TermsOfService"],
					License = new License
					{
						Name = Configuration["Swagger:License:Name"],
						Url = Configuration["Swagger:License:Url"]
					},
					Contact = new Contact
					{
						Name = Configuration["Swagger:Contact:Name"],
						Email = Configuration["Swagger:Contact:Email"],
						Url = Configuration["Swagger:Contact:Url"]
					}
				});
				
				var scopePrefixBuilder = new StringBuilder(Configuration["Swagger:Security:ScopePrefix"]);
				
				if (scopePrefixBuilder[scopePrefixBuilder.Length - 1] != '/')
				{
					scopePrefixBuilder.Append("/");
				}

				var scopePrefix = scopePrefixBuilder.ToString();

				c.AddSecurityDefinition("oauth2", new OAuth2Scheme
				{
					Type = "oauth2",
					Flow = "implicit",
					AuthorizationUrl = Configuration["Swagger:Security:AuthorizationUrl"],
					Scopes = new Dictionary<string, string>
					{
						["openid"] = "Open Id",
						[$"{scopePrefix}read_write"] = "Read/Write",
					}
				});
				c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
				{
					["oauth2"] = new[]
					{
						"openid",
						$"{scopePrefix}read_write"
					},
				});
			});

			Log.Logger.Information("Configuring Http Context");
			services.AddHttpContextAccessor();

			Log.Logger.Information("Configuring Mediatr");
			services.AddMediatR();

			Log.Logger.Information("Configuring User Components");
			containerBuilder.RegisterModule(new UsersApiModule(Configuration));
			containerBuilder.Populate(services);

			IContainer container = containerBuilder.Build();

			Log.Logger.Information("Services configured");

			return new AutofacServiceProvider(container);
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.ConfigureCustomExceptionMiddleware();

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.OAuthClientId(Configuration["Swagger:Security:ClientId"]);
				c.OAuthAppName(Configuration["Swagger:Security:OAuthAppName"]);
				c.OAuthScopeSeparator(" ");
				c.SwaggerEndpoint("/swagger/v0/swagger.json", "V0 Docs");
				//c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
			});

			app.UseHttpsRedirection();
			app.UseAuthentication();
			app.UseMvc();
		}
	}
}
