﻿using System;
using System.Threading;
using Beast.Serialisation;
using RabbitMQ.Client;
using Serilog;

namespace Beast.EnterpriseBus.RabbitMq
{
	public abstract class RabbitMqClient : IDisposable
	{
		private const int DefaultCloseTimeoutSeconds = 1;

		protected readonly int _CloseTimeoutSeconds;
		protected readonly ISerialiser _Serialiser;
		protected readonly ConnectionFactory _ConnectionFactory;
		protected readonly ReaderWriterLockSlim _IsActivatedLock;

		protected IConnection _Connection;
		protected IModel _Channel;
		protected bool _IsActivated;
		protected IBasicProperties _ChannelProperties;
		protected readonly ILogger _Logger;

		public long EnterpriseBusClientId { get; }
		public string Name { get; }
		public string Host { get; }
		public string Type { get; }
		public string Exchange { get; }
		public bool IsDurable { get; }
		public bool AutoDelete { get; }

		protected RabbitMqClient(ILogger logger, RabbitMqClientConfig config, ISerialiser serialiser)
		{
			if (config == null)
			{
				throw new ArgumentNullException(nameof(config));
			}
			
			EnterpriseBusClientId = config.Id;
			Name = config.Name ?? throw new ArgumentException("Name cannot be null", nameof(config));
			Host = config.Host ?? throw new ArgumentException("Host cannot be null", nameof(config));
			Exchange = config.Exchange ?? throw new ArgumentException("Exchange cannot be null", nameof(config));
			Type = config.Type ?? throw new ArgumentException("Type cannot be null", nameof(config));
			IsDurable = config.IsDurable;
			AutoDelete = config.AutoDelete;

			_IsActivatedLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

			_Logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_Serialiser = serialiser ?? throw new ArgumentNullException(nameof(serialiser));
			_CloseTimeoutSeconds = config.CloseTimeoutSeconds <= 0 
				                       ? DefaultCloseTimeoutSeconds 
				                       : config.CloseTimeoutSeconds;

			_ConnectionFactory = new ConnectionFactory
			{
				VirtualHost = config.VirtualHost,
				HostName = Host,
				Port = config.Port <= 0 ? AmqpTcpEndpoint.UseDefaultPort : config.Port,
				UserName = config.Username,
				Password = config.Password
			};
		}

		public abstract void Start();
		public abstract void Stop();

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				Stop();
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}