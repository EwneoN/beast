﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Beast.EnterpriseBus.Clients;
using Beast.EnterpriseBus.Exceptions;
using Beast.EnterpriseBus.Messages;
using Beast.Serialisation;
using RabbitMQ.Client;
using Serilog;

namespace Beast.EnterpriseBus.RabbitMq.Publishing
{
	public class RabbitMqPublisher : RabbitMqClient, IEnterpriseBusPublisher, IStartable
	{
		public RabbitMqPublisher(ILogger logger, RabbitMqClientConfig config, ISerialiser serialiser) 
			: base(logger, config, serialiser) { }
		
		public override void Start()
		{
			_IsActivatedLock.EnterWriteLock();

			try
			{
				if (_IsActivated)
				{
					return;
				}

				_Connection = _ConnectionFactory.CreateConnection();
				_Channel = _Connection.CreateModel();
				_ChannelProperties = _Channel.CreateBasicProperties();
				_Channel.ExchangeDeclare(Exchange, Type, IsDurable, AutoDelete);

				_IsActivated = true;
			}
			catch (Exception ex)
			{
				_Logger.Error(ex, "Exception thrown on Publisher Action: {@Action}", new { action = "Start" });
				throw;
			}
			finally
			{
				_IsActivatedLock.ExitWriteLock();
			}
		}

		public override void Stop()
		{
			_IsActivatedLock.EnterWriteLock();

			try
			{
				if (!_IsActivated)
				{
					return;
				}

				_Channel.Close();
				_Channel.Dispose();
				_Connection.Close(_CloseTimeoutSeconds);
				_Connection.Dispose();

				_IsActivated = false;
			}
			catch (Exception ex)
			{
				_Logger.Error(ex, "Exception thrown on Publisher Action: {@Action}", new { action = "Stop" });
				throw;
			}
			finally
			{
				_IsActivatedLock.ExitWriteLock();
			}
		}

		public async Task PublishMessage(IEnterpriseBusMessage message, CancellationToken cancellationToken)
		{
			if (message == null)
			{
				throw new ArgumentNullException(nameof(message));
			}

			_IsActivatedLock.EnterReadLock();

			try
			{
				if (!_IsActivated)
				{
					throw new ClientNotActivatedException();
				}

				byte[] messageBody = await _Serialiser.SerialiseToByteArray(message);

				_Channel.BasicPublish(Exchange, message.Topic, _ChannelProperties, messageBody);
			}
			catch (Exception ex)
			{
				_Logger.Error(ex, "Exception thrown on Publisher Action: {@Action}", new { action = "Publish" });
				throw;
			}
			finally
			{
				_IsActivatedLock.ExitReadLock();
			}
		}
	}
}
