﻿namespace Beast.EnterpriseBus.RabbitMq.Subscribing {
	public class RabbitMqSubscriberConfig : RabbitMqClientConfig
	{
		public string[] Topics { get; set; }
	}
}