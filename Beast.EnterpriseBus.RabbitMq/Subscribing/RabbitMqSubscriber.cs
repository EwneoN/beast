﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Beast.EnterpriseBus.Clients;
using Beast.EnterpriseBus.Exceptions;
using Beast.EnterpriseBus.Messages;
using Beast.Events;
using Beast.Serialisation;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;

namespace Beast.EnterpriseBus.RabbitMq.Subscribing
{
	public class RabbitMqSubscriber<TMessage> : RabbitMqClient, IEnterpriseBusSubscriber<TMessage>, IStartable
		where TMessage : IEnterpriseBusMessage
	{
		public event BeastEventHandler<TMessage> MessageReceived;

		public string[] Topics { get; }

		private AsyncEventingBasicConsumer _Consumer;

		public RabbitMqSubscriber(ILogger logger, RabbitMqSubscriberConfig config, ISerialiser serialiser) 
			: base(logger, config, serialiser)
		{
			Topics = config.Topics ?? new string[0];
		}

		public override void Start()
		{
			_IsActivatedLock.EnterWriteLock();

			try
			{
				if (_IsActivated)
				{
					return;
				}

				_Connection = _ConnectionFactory.CreateConnection();
				_Channel = _Connection.CreateModel();
				_ChannelProperties = _Channel.CreateBasicProperties();
				_Channel.ExchangeDeclare(Exchange, Type, IsDurable, AutoDelete);

				string queueName = _Channel.QueueDeclare(durable: IsDurable, autoDelete: AutoDelete).QueueName;

				if (Topics.Any())
				{
					foreach (string topic in Topics)
					{
						_Channel.QueueBind(queueName, Exchange, topic);
					}
				}
				else
				{
					_Channel.QueueBind(queueName, Exchange, "");
				}

				_Consumer = new AsyncEventingBasicConsumer(_Channel);
				_Consumer.Received += Consumer_Received;

				_IsActivated = true;
			}
			catch (Exception ex)
			{
				_Logger.Error(ex, "Exception thrown on Subscriber Action: {@Action}", new { action = "Start" });
				throw;
			}
			finally
			{
				_IsActivatedLock.ExitWriteLock();
			}
		}

		public override void Stop()
		{
			_IsActivatedLock.EnterWriteLock();

			try
			{
				if (!_IsActivated)
				{
					return;
				}

				_Consumer.Received -= Consumer_Received;
				_Channel.Close();
				_Channel.Dispose();
				_Connection.Close(_CloseTimeoutSeconds);
				_Connection.Dispose();

				_Consumer = null;
				_Channel = null;
				_Connection = null;

				_IsActivated = false;
			}
			catch (Exception ex)
			{
				_Logger.Error(ex, "Exception thrown on Subscriber Action: {@Action}", new { action = "Stop" });
				throw;
			}
			finally
			{
				_IsActivatedLock.ExitWriteLock();
			}
		}

		private async Task Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
		{
			_IsActivatedLock.EnterReadLock();

			try
			{
				if (!_IsActivated)
				{
					throw new ClientNotActivatedException();
				}

				if (MessageReceived == null)
				{
					return;
				}

				TMessage message = await _Serialiser.DeserialiseFromByteArray<TMessage>(eventArgs.Body);

				await MessageReceived(sender, new BeastEventArgs<TMessage>(message));

				_Channel.BasicAck(eventArgs.DeliveryTag, false);
			}
			catch (Exception ex)
			{
				_Logger.Error(ex, "Exception thrown on Subscriber Action: {@Action}", new { action = "MessageReceived" });
				throw;
			}
			finally
			{
				_IsActivatedLock.ExitReadLock();
			}
		}
	}
}