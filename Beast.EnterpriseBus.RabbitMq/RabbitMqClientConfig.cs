﻿namespace Beast.EnterpriseBus.RabbitMq {
	public class RabbitMqClientConfig
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public string VirtualHost { get; set; }
		public string Host { get; set; }
		public int Port { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public int CloseTimeoutSeconds { get; set; }
		public string Exchange { get; set; }
		public string Type { get; set; }
		public bool IsDurable { get; set; }
		public bool AutoDelete { get; set; }
	}
}