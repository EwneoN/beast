﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.Exceptions;
using Beast.Users.DTOs.Claims;
using Beast.Users.Exceptions;
using Beast.Users.Models;
using Beast.Users.Repositories;

namespace Beast.Users.EntityFramework.Repositories
{
	public class ClaimsRepository : UsersContextRepository, IClaimsRepository
	{
		public ClaimsRepository(IUsersContext dbContext) : base(dbContext) { }

		public async Task<bool> DoesClaimExist(long claimId, CancellationToken cancellationToken)
		{
			return await DbContext.Claims.AnyAsync(c => c.Id == claimId, cancellationToken);
		}

		public async Task<bool> DoesClaimExist(string name, CancellationToken cancellationToken)
		{
			return await DbContext.Claims.AnyAsync(c => c.Name == name, cancellationToken);
		}

		public async Task<IClaimInfo> CreateNewClaim(INewClaim newClaim, CancellationToken cancellationToken)
		{
			if (newClaim == null)
			{
				throw new ArgumentNullException(nameof(newClaim));
			}

			Claim claim = new Claim
			{
				Name = newClaim.Name,
				BaseClaimId = newClaim.BaseClaimId,
				IsBaseClaim = false
			};

			DbContext.Claims.Add(claim);

			await DbContext.SaveChangesAsync(cancellationToken);

			return new ClaimInfo
			{
				Name = claim.Name,
				ClaimId = claim.Id,
				IsBaseClaim = claim.IsBaseClaim,
				Timestamp = claim.Timestamp
			};
		}

		public async Task<ICollection<IClaimInfo>> GetAllClaims(CancellationToken cancellationToken)
		{
			return (await DbContext.Claims
			                      .Select(c => new ClaimInfo
			                      {
				                      ClaimId = c.Id,
				                      Name = c.Name,
				                      Timestamp = c.Timestamp
			                      })
			                      .ToListAsync(cancellationToken))
														.OfType<IClaimInfo>()
														.ToList();
		}

		public async Task<IClaimInfo> GetClaim(long claimId, CancellationToken cancellationToken)
		{
			return await DbContext.Claims
			                      .Where(c => c.Id == claimId)
			                      .Select(c => new ClaimInfo
			                      {
				                      ClaimId = c.Id,
				                      Name = c.Name,
				                      Timestamp = c.Timestamp
			                      })
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IClaimInfo> GetClaimByName(string name, CancellationToken cancellationToken)
		{
			return await DbContext.Claims
			                      .Where(c => c.Name == name)
			                      .Select(c => new ClaimInfo
			                      {
				                      ClaimId = c.Id,
				                      Name = c.Name,
				                      Timestamp = c.Timestamp
			                      })
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task DeleteClaim(long claimId, CancellationToken cancellationToken)
		{
			Claim claim = await DbContext.Claims.FirstOrDefaultAsync(c => c.Id == claimId, cancellationToken);

			if (claim == null)
			{
				throw new RecordNotFoundException("Claim", claimId);
			}

			if (claim.IsBaseClaim)
			{
				throw new BaseClaimDeleteAttemptException(claim.Name);
			}

			DbContext.Claims.Remove(claim);

			await DbContext.SaveChangesAsync(cancellationToken);
		}
	}
}
