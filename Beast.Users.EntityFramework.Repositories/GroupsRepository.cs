﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Exceptions;
using Beast.Services.Ids;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.DTOs.Groups;
using Beast.Users.Models;
using Beast.Users.Repositories;

namespace Beast.Users.EntityFramework.Repositories
{
	public class GroupsRepository : UsersContextRepository, IGroupsRepository
	{
		private readonly IIdService _IdService;

		public GroupsRepository(IUsersContext dbContext, IIdService idService) : base(dbContext)
		{
			_IdService = idService;
		}

		public async Task<bool> DoesGroupExist(long groupId, CancellationToken cancellationToken)
		{
			return await DbContext.Groups.AnyAsync(g => g.Id == groupId, cancellationToken);
		}

		public async Task<bool> DoesGroupExist(string groupName, CancellationToken cancellationToken)
		{
			return await DbContext.Groups.AnyAsync(g => g.Name == groupName, cancellationToken);
		}

		public async Task<IPagedCollection<IGroupInfo>> GetGroups(IPagedQueryParams queryParams,
		                                                          CancellationToken cancellationToken)
		{
			var totalCount = await DbContext.Groups.CountAsync(cancellationToken);
			var groups = await DbContext.Groups
			                            .Skip(queryParams.PageNumber - 1 * queryParams.PageSize)
			                            .Take(queryParams.PageSize)
			                            .Select(u => new
			                            {
				                            GroupId = u.Id,
				                            u.Name,
				                            Claims = u.Claims
				                                      .Select(c => new
				                                      {
					                                      GroupClaimId = c.Id,
					                                      c.GroupId,
					                                      c.ClaimId,
					                                      c.Claim.Name,
					                                      c.Timestamp
				                                      })
			                            })
			                            .ToArrayAsync(cancellationToken);

			return new PagedCollection<IGroupInfo>(queryParams, totalCount, groups
				                                       .Select(u => new GroupInfo
				                                       {
					                                       GroupId = u.GroupId,
					                                       Name = u.Name,
					                                       Claims = u.Claims
					                                                 .Select(c => new GroupClaimInfo
					                                                 {
						                                                 GroupClaimId = c.GroupClaimId,
						                                                 GroupId = c.GroupId,
						                                                 ClaimId = c.ClaimId,
						                                                 Name = c.Name,
						                                                 Timestamp = c.Timestamp
					                                                 } as IGroupClaimInfo)
					                                                 .ToArray()
				                                       } as IGroupInfo));
		}

		public async Task<IGroupInfo> GetGroup(long groupId, CancellationToken cancellationToken)
		{
			var group = await DbContext.Groups
			                      .Where(u => u.Id == groupId)
			                      .Select(u => new
			                      {
				                      GroupId = u.Id,
				                      ParentGroupId = u.ParentId,
				                      ParentGroupName = u.Parent.Name,
				                      u.Name,
				                      Claims = u.Claims
				                                .Select(c => new
				                                {
					                                GroupClaimId = c.Id,
					                                c.GroupId,
					                                c.ClaimId,
					                                c.Claim.Name,
					                                c.Timestamp
				                                })
			                      })
			                      .FirstOrDefaultAsync(cancellationToken);

			if (group == null)
			{
				return null;
			}

			return new GroupInfo
			{
				GroupId = group.GroupId,
				ParentGroupId = group.ParentGroupId,
				ParentGroupName = group.ParentGroupName,
				Name = group.Name,
				Claims = group.Claims
				          .Select(c => new GroupClaimInfo
				          {
					          GroupClaimId = c.GroupClaimId,
					          GroupId = c.GroupId,
					          ClaimId = c.ClaimId,
					          Name = c.Name,
					          Timestamp = c.Timestamp
				          } as IGroupClaimInfo)
				          .ToArray()
			};
		}

		public async Task<IGroupInfo> GetGroupByName(string groupName, CancellationToken cancellationToken)
		{
			var group = await DbContext.Groups
			                      .Where(u => u.Name == groupName)
														.Select(u => new
			                      {
				                      GroupId = u.Id,
				                      ParentGroupId = u.ParentId,
				                      ParentGroupName = u.Parent.Name,
				                      u.Name,
				                      Claims = u.Claims
				                                .Select(c => new
				                                {
					                                GroupClaimId = c.Id,
					                                c.GroupId,
					                                c.ClaimId,
					                                c.Claim.Name,
					                                c.Timestamp
				                                })
			                      })
			                      .FirstOrDefaultAsync(cancellationToken);

			if (group == null)
			{
				return null;
			}

			return new GroupInfo
			{
				GroupId = group.GroupId,
				ParentGroupId = group.ParentGroupId,
				ParentGroupName = group.ParentGroupName,
				Name = group.Name,
				Claims = group.Claims
				              .Select(c => new GroupClaimInfo
				              {
					              GroupClaimId = c.GroupClaimId,
					              GroupId = c.GroupId,
					              ClaimId = c.ClaimId,
					              Name = c.Name,
					              Timestamp = c.Timestamp
				              } as IGroupClaimInfo)
				              .ToArray()
			};
		}

		public async Task<IGroupInfo> CreateNewGroup(INewGroup newGroup, CancellationToken cancellationToken)
		{
			if (newGroup == null)
			{
				throw new ArgumentNullException(nameof(newGroup));
			}

			Group group = new Group
			{
				Id = await _IdService.GetNewId(),
				Name = newGroup.Name,
				ParentId = newGroup.ParentGroupId
			};

			DbContext.Groups.Add(group);

			await DbContext.SaveChangesAsync(cancellationToken);

			return await GetGroup(group.Id, cancellationToken);
		}

		public async Task<IGroupInfo> UpdateGroup(IGroupUpdate update, CancellationToken cancellationToken)
		{
			if (update == null)
			{
				throw new ArgumentNullException(nameof(update));
			}

			Group group = await DbContext.Groups.FirstOrDefaultAsync(u => u.Id == update.GroupId, cancellationToken);

			if (group == null)
			{
				throw new RecordNotFoundException("Group", update.GroupId);
			}

			group.Name = update.Name;
			group.ParentId = update.ParentGroupId;

			await DbContext.SaveChangesAsync(cancellationToken);

			return await GetGroup(update.GroupId, cancellationToken);
		}

		public async Task DeleteGroup(long groupId, CancellationToken cancellationToken)
		{
			Group group = await DbContext.Groups.FirstOrDefaultAsync(u => u.Id == groupId, cancellationToken);

			if (group == null)
			{
				throw new RecordNotFoundException("Group", groupId);
			}

			DbContext.Groups.Remove(group);

			await DbContext.SaveChangesAsync(cancellationToken);
		}

		private async Task<IGroupInfo> GetGroupInfo(Group group, CancellationToken cancellationToken)
		{
			var parent = await GetGroupParent(group, cancellationToken);

			return new GroupInfo
			{
				GroupId = group.Id,
				ParentGroupId = group.ParentId,
				ParentGroupName = parent?.Name,
				Name = group.Name,
				Claims = await GetGroupInfoClaims(group, cancellationToken)
			};
		}

		private async Task<Group> GetGroupParent(Group group, CancellationToken cancellationToken)
		{
			return group.Parent != null 
				       ? group 
				       : await DbContext.Groups.FirstOrDefaultAsync(g => g.ParentId == group.ParentId, cancellationToken);
		}

		private async Task<ICollection<IGroupClaimInfo>> GetGroupInfoClaims(Group group, CancellationToken cancellationToken)
		{
			return group.Claims.Any()
				       ? group.Claims
				             .Where(g => g.GroupId == group.Id)
				             .Select(c => new GroupClaimInfo
				             {
					             GroupClaimId = c.Id,
					             GroupId = c.GroupId,
					             ClaimId = c.ClaimId,
					             Name = c.Claim.Name,
					             Timestamp = c.Timestamp
				             } as IGroupClaimInfo)
				             .ToList()
				       : (await DbContext.GroupClaims
				                        .Where(g => g.GroupId == group.Id)
				                        .Select(c => new
				                        {
					                        GroupClaimId = c.Id,
					                        c.GroupId,
					                        c.ClaimId,
					                        c.Claim.Name,
					                        c.Timestamp
				                        })
				                        .ToListAsync(cancellationToken))
				         .Select(c => new GroupClaimInfo
				         {
					         GroupClaimId = c.ClaimId,
					         GroupId = c.GroupId,
					         ClaimId = c.ClaimId,
					         Name = c.Name,
					         Timestamp = c.Timestamp
				         } as IGroupClaimInfo)
				         .ToList();
		}
	}
}