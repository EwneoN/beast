﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.Exceptions;
using Beast.Services.Ids;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.Models;
using Beast.Users.Repositories;

namespace Beast.Users.EntityFramework.Repositories
{
	public class GroupClaimsRepository : UsersContextRepository, IGroupClaimsRepository
	{
		private readonly IIdService _IdService;

		public GroupClaimsRepository(IUsersContext dbContext, IIdService idService) : base(dbContext)
		{
			_IdService = idService;
		}

		public async Task<bool> DoesGroupClaimExist(long groupClaimId, CancellationToken cancellationToken)
		{
			return await DbContext.GroupClaims.AnyAsync(c => c.Id == groupClaimId, cancellationToken);
		}

		public async Task<bool> DoesGroupClaimExist(long groupId, long claimId, CancellationToken cancellationToken)
		{
			return await DbContext.GroupClaims.AnyAsync(c => c.GroupId == groupId && c.ClaimId == claimId, cancellationToken);
		}

		public async Task<bool> DoesGroupClaimExist(long groupId, string claimName, CancellationToken cancellationToken)
		{
			return await DbContext.GroupClaims.AnyAsync(c => c.GroupId == groupId && c.Claim.Name == claimName, cancellationToken);
		}

		public async Task<ICollection<IGroupClaimInfo>> GetGroupClaims(long groupId, CancellationToken cancellationToken)
		{
			return await DbContext.GroupClaims
			                      .Where(u => u.GroupId == groupId)
			                      .Select(u => new GroupClaimInfo
			                      {
				                      GroupClaimId = u.Id,
				                      GroupId = u.GroupId,
				                      ClaimId = u.ClaimId,
				                      Name = u.Claim.Name,
				                      Timestamp = u.Timestamp
			                      } as IGroupClaimInfo)
			                      .ToListAsync(cancellationToken);
		}

		public async Task<IGroupClaimInfo> GetGroupClaim(long groupClaimId, CancellationToken cancellationToken)
		{
			return await DbContext.GroupClaims
			                      .Where(u => u.Id == groupClaimId)
			                      .Select(u => new GroupClaimInfo
			                      {
				                      GroupClaimId = u.Id,
				                      GroupId = u.GroupId,
				                      ClaimId = u.ClaimId,
				                      Name = u.Claim.Name,
				                      Timestamp = u.Timestamp
			                      } as IGroupClaimInfo)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IGroupClaimInfo> GetGroupClaimByName(long groupId, string claimName, CancellationToken cancellationToken)
		{
			return await DbContext.GroupClaims
			                      .Where(u => u.GroupId == groupId && u.Claim.Name == claimName)
			                      .Select(u => new GroupClaimInfo
			                      {
				                      GroupClaimId = u.Id,
				                      GroupId = u.GroupId,
				                      ClaimId = u.ClaimId,
				                      Name = u.Claim.Name,
				                      Timestamp = u.Timestamp
			                      } as IGroupClaimInfo)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IGroupClaimInfo> CreateNewGroupClaim(INewGroupClaim newGroupClaim,
		                                                       CancellationToken cancellationToken)
		{
			if (newGroupClaim == null)
			{
				throw new ArgumentNullException(nameof(newGroupClaim));
			}

			GroupClaim groupClaim = new GroupClaim
			{
				Id = await _IdService.GetNewId(),
				GroupId = newGroupClaim.GroupId,
				ClaimId = newGroupClaim.ClaimId
			};

			DbContext.GroupClaims.Add(groupClaim);

			await DbContext.SaveChangesAsync(cancellationToken);

			return await GetGroupClaim(groupClaim.Id, cancellationToken);
		}

		public async Task DeleteGroupClaim(long groupClaimId, CancellationToken cancellationToken)
		{
			GroupClaim groupClaim = await DbContext.GroupClaims
			                                       .FirstOrDefaultAsync(u => u.Id == groupClaimId, cancellationToken);

			if (groupClaim == null)
			{
				throw new RecordNotFoundException("GroupClaim", groupClaimId);
			}

			DbContext.GroupClaims.Remove(groupClaim);

			await DbContext.SaveChangesAsync(cancellationToken);
		}
	}
}