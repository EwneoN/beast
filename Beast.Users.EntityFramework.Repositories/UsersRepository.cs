﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Exceptions;
using Beast.Services.Ids;
using Beast.Users.DTOs.UserClaims;
using Beast.Users.DTOs.Users;
using Beast.Users.Models;
using Beast.Users.Repositories;

namespace Beast.Users.EntityFramework.Repositories
{
	public class UsersRepository : UsersContextRepository, IUsersRepository
	{
		private readonly IIdService _IdService;

		public UsersRepository(IUsersContext dbContext, IIdService idService) : base(dbContext)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
		}

		public async Task<bool> DoesUserExist(long userId, CancellationToken cancellationToken)
		{
			return await DbContext.Users.AnyAsync(u => u.Id == userId, cancellationToken);
		}

		public async Task<bool> DoesUserExist(string username, CancellationToken cancellationToken)
		{
			return await DbContext.Users.AnyAsync(u => u.Username == username, cancellationToken);
		}

		public async Task<IPagedCollection<IUserInfo>> GetUsers(IPagedQueryParams queryParams,
		                                                        CancellationToken cancellationToken)
		{
			var totalCount = await DbContext.Users.CountAsync(cancellationToken);
			var users = await DbContext.Users
			                           .Skip(queryParams.PageNumber - 1 * queryParams.PageSize)
			                           .Take(queryParams.PageSize)
			                           .Select(u => new
			                           {
				                           u.Id,
				                           u.AuthId,
				                           Claims = u.Claims
				                                     .Select(c => new
				                                     {
					                                     c.Id,
					                                     c.UserId,
					                                     c.ClaimId,
					                                     c.Claim.Name,
					                                     c.Timestamp
				                                     })
			                           })
			                           .ToArrayAsync(cancellationToken);

			return new PagedCollection<IUserInfo>(queryParams, totalCount, users.Select(user => new UserInfo
			{
				UserId = user.Id,
				AuthId = user.AuthId,
				Claims = user.Claims
				             .Select(c => new UserClaimInfo
				             {
					             UserClaimId = c.Id,
					             UserId = c.UserId,
					             ClaimId = c.ClaimId,
					             Name = c.Name,
					             Timestamp = c.Timestamp
				             } as IUserClaimInfo)
				             .ToList()
			}));
		}

		public async Task<IUserInfo> GetUser(long userId, CancellationToken cancellationToken)
		{
			var user = await DbContext.Users
			                          .Where(u => u.Id == userId)
			                          .Select(u => new
			                          {
				                          u.Id,
				                          u.AuthId,
				                          Claims = u.Claims
				                                    .Select(c => new
				                                    {
					                                    c.Id,
					                                    c.UserId,
					                                    c.ClaimId,
					                                    c.Claim.Name,
					                                    c.Timestamp
				                                    })
			                          })
			                          .FirstOrDefaultAsync(cancellationToken);

			if (user == null)
			{
				return null;
			}

			return new UserInfo
			{
				UserId = user.Id,
				AuthId = user.AuthId,
				Claims = user.Claims
				             .Select(c => new UserClaimInfo
				             {
					             UserClaimId = c.Id,
					             UserId = c.UserId,
					             ClaimId = c.ClaimId,
					             Name = c.Name,
					             Timestamp = c.Timestamp
				             } as IUserClaimInfo)
				             .ToList()
			};
		}

		public async Task<IUserInfo> GetUserByAuthId(string authId, CancellationToken cancellationToken)
		{
			var user = await DbContext.Users
			                          .Where(u => u.AuthId == authId)
			                          .Select(u => new
			                          {
				                          u.Id,
				                          u.AuthId,
				                          Claims = u.Claims
				                                    .Select(c => new
				                                    {
					                                    c.Id,
					                                    c.UserId,
					                                    c.ClaimId,
					                                    c.Claim.Name,
					                                    c.Timestamp
				                                    })
			                          })
			                          .FirstOrDefaultAsync(cancellationToken);

			if (user == null)
			{
				return null;
			}

			return new UserInfo
			{
				UserId = user.Id,
				AuthId = user.AuthId,
				Claims = user.Claims
				             .Select(c => new UserClaimInfo
				             {
					             UserClaimId = c.Id,
					             UserId = c.UserId,
					             ClaimId = c.ClaimId,
					             Name = c.Name,
					             Timestamp = c.Timestamp
				             } as IUserClaimInfo)
				             .ToList()
			};
		}

		public async Task<IUserInfo> GetUserByUsername(string username, CancellationToken cancellationToken)
		{
			var user = await DbContext.Users
			                          .Where(u => u.Username == username)
			                          .Select(u => new
			                          {
				                          u.Id,
				                          u.AuthId,
				                          Claims = u.Claims
				                                    .Select(c => new
				                                    {
					                                    c.Id,
					                                    c.UserId,
					                                    c.ClaimId,
					                                    c.Claim.Name,
					                                    c.Timestamp
				                                    })
			                          })
			                          .FirstOrDefaultAsync(cancellationToken);

			if (user == null)
			{
				return null;
			}

			return new UserInfo
			{
				UserId = user.Id,
				AuthId = user.AuthId,
				Claims = user.Claims
				             .Select(c => new UserClaimInfo
				             {
					             UserClaimId = c.Id,
					             UserId = c.UserId,
					             ClaimId = c.ClaimId,
					             Name = c.Name,
					             Timestamp = c.Timestamp
				             } as IUserClaimInfo)
				             .ToList()
			};
		}

		public async Task<long> GetUserId(string username, CancellationToken cancellationToken)
		{
			return await DbContext.Users
			                      .Where(u => u.Username == username)
			                      .Select(u => u.Id)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<string> GetUserAuthId(long userId, CancellationToken cancellationToken)
		{
			return await DbContext.Users
			                      .Where(u => u.Id == userId)
			                      .Select(u => u.AuthId)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<string> GetUsername(long userId, CancellationToken cancellationToken)
		{
			return await DbContext.Users
			                      .Where(u => u.Id == userId)
			                      .Select(u => u.Username)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IUserInfo> CreateNewUser(INewUser newUser, CancellationToken cancellationToken)
		{
			if (newUser == null)
			{
				throw new ArgumentNullException(nameof(newUser));
			}

			User user = new User
			{
				Id = await _IdService.GetNewId(),
				Username = newUser.Username,
				FirstName = newUser.FirstName,
				MiddleName = newUser.MiddleName,
				LastName = newUser.LastName,
				DateOfBirth = newUser.DateOfBirth,
				DisplayName = newUser.DisplayName,
				EmailAddress = newUser.EmailAddress,
				MobilePhoneNumber = newUser.MobilePhoneNumber,
				Branch = newUser.Branch,
				BranchPostCode = newUser.BranchPostCode,
				EmployeeId = newUser.EmployeeId,
				JobTitle = newUser.JobTitle,
				IsLocalAccount = newUser.AccountType == UserIdentityType.LocalAccount
			};

			DbContext.Users.Add(user);

			await DbContext.SaveChangesAsync(cancellationToken);

			return await GetUserInfo(user, cancellationToken);
		}

		public async Task<IUserInfo> UpdateUser(IUserUpdate update, CancellationToken cancellationToken)
		{
			if (update == null)
			{
				throw new ArgumentNullException(nameof(update));
			}

			User user = await DbContext.Users.FirstOrDefaultAsync(u => u.Id == update.UserId, cancellationToken);

			if (user == null)
			{
				throw new RecordNotFoundException("User", update.UserId);
			}

			user.AuthId = update.AuthId;
			user.EmailAddress = update.EmailAddress;
			user.FirstName = update.FirstName;
			user.MiddleName = update.MiddleName;
			user.LastName = update.LastName;
			user.DateOfBirth = update.DateOfBirth;
			user.DisplayName = update.DisplayName;
			user.Branch = update.Branch;
			user.BranchPostCode = update.BranchPostCode;
			user.EmployeeId = update.EmployeeId;
			user.JobTitle = update.JobTitle;
			user.Username = update.Username;
			user.MobilePhoneNumber = update.MobilePhoneNumber;

			await DbContext.SaveChangesAsync(cancellationToken);

			return await GetUserInfo(user, cancellationToken);
		}

		public async Task DeleteUser(long userId, CancellationToken cancellationToken)
		{
			User user = await DbContext.Users.FirstOrDefaultAsync(u => u.Id == userId, cancellationToken);

			if (user == null)
			{
				throw new RecordNotFoundException("User", userId);
			}

			DbContext.Users.Remove(user);

			await DbContext.SaveChangesAsync(cancellationToken);
		}

		public async Task SetUserAuthId(long userId, string authId, CancellationToken cancellationToken)
		{
			User user = await DbContext.Users.FirstOrDefaultAsync(u => u.Id == userId, cancellationToken);

			if (user == null)
			{
				throw new RecordNotFoundException("User", userId);
			}

			user.AuthId = authId;

			await DbContext.SaveChangesAsync(cancellationToken);
		}

		private async Task<IUserInfo> GetUserInfo(User user, CancellationToken cancellationToken)
		{
			return new UserInfo
			{
				UserId = user.Id,
				AuthId = user.AuthId,
				Claims = await GetUserInfoClaims(user, cancellationToken)
			};
		}

		private async Task<ICollection<IUserClaimInfo>> GetUserInfoClaims(User user, CancellationToken cancellationToken)
		{
			return user.Claims.Any()
				       ? user.Claims
				             .Where(u => u.UserId == user.Id)
				             .Select(c => new UserClaimInfo
				             {
					             UserClaimId = c.Id,
					             UserId = c.UserId,
					             ClaimId = c.ClaimId,
					             Name = c.Claim.Name,
					             Timestamp = c.Timestamp
				             } as IUserClaimInfo)
				             .ToList()
				       : (await DbContext.UserClaims
				                         .Where(u => u.UserId == user.Id)
				                         .Select(c => new
				                         {
					                         UserClaimId = c.Id,
					                         c.UserId,
					                         c.ClaimId,
					                         c.Claim.Name,
					                         c.Timestamp
				                         })
				                         .ToListAsync(cancellationToken))
												         .Select(c => new UserClaimInfo
												         {
													         UserClaimId = c.ClaimId,
													         UserId = c.UserId,
													         ClaimId = c.ClaimId,
													         Name = c.Name,
													         Timestamp = c.Timestamp
												         } as IUserClaimInfo)
												         .ToList();
		}
	}
}