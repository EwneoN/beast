﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Beast.Repositories;

namespace Beast.Users.EntityFramework.Repositories
{
	public abstract class UsersContextRepository : IRepository
	{
		private DbContextTransaction _CurrentTransaction;

		public IUsersContext DbContext { get; }

		protected UsersContextRepository(IUsersContext dbContext)
		{
			DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		}

		public async Task BeginTransaction(CancellationToken cancellationToken)
		{
			if (_CurrentTransaction != null)
			{
				throw new InvalidOperationException("Cannot start transaction as one has already begun");
			}

			_CurrentTransaction = DbContext.Database.BeginTransaction();

			await Task.FromResult(0);
		}

		public async Task CommitTransaction(CancellationToken cancellationToken)
		{
			if (_CurrentTransaction == null)
			{
				throw new InvalidOperationException("Cannot commit transaction as none has begun");
			}

			_CurrentTransaction.Commit();
			_CurrentTransaction.Dispose();

			await Task.FromResult(0);
		}

		public async Task RollbackTransaction(CancellationToken cancellationToken)
		{
			if (_CurrentTransaction == null)
			{
				throw new InvalidOperationException("Cannot rollback transaction as none has begun");
			}

			_CurrentTransaction.Rollback();
			_CurrentTransaction.Dispose();

			await Task.FromResult(0);
		}
	}
}
