﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.Exceptions;
using Beast.Services.Ids;
using Beast.Users.DTOs.UserClaims;
using Beast.Users.Models;
using Beast.Users.Repositories;

namespace Beast.Users.EntityFramework.Repositories
{
	public class UserClaimsRepository : UsersContextRepository, IUserClaimsRepository
	{
		private readonly IIdService _IdService;

		public UserClaimsRepository(IUsersContext dbContext, IIdService idService) : base(dbContext)
		{
			_IdService = idService;
		}

		public async Task<bool> DoesUserClaimExist(long userId, string claimName, CancellationToken cancellationToken)
		{
			return await DbContext.UserClaims.AnyAsync(c => c.UserId == userId && c.Claim.Name == claimName, cancellationToken);
		}

		public async Task<bool> DoesUserClaimExist(long userId, long claimId, CancellationToken cancellationToken)
		{
			return await DbContext.UserClaims.AnyAsync(c => c.UserId == userId && c.ClaimId == claimId, cancellationToken);
		}

		public async Task<bool> DoesUserClaimExist(long userClaimId, CancellationToken cancellationToken)
		{
			return await DbContext.UserClaims.AnyAsync(c => c.Id == userClaimId, cancellationToken);
		}

		public async Task<ICollection<IUserClaimInfo>> GetUserClaims(long userId, CancellationToken cancellationToken)
		{
			return await DbContext.UserClaims
			                      .Where(u => u.UserId == userId)
			                      .Select(u => new UserClaimInfo
			                      {
															UserClaimId = u.Id,
															UserId = u.UserId,
															ClaimId = u.ClaimId,
															Name = u.Claim.Name,
															Timestamp = u.Timestamp
			                      } as IUserClaimInfo)
			                      .ToListAsync(cancellationToken);
		}

		public async Task<IUserClaimInfo> GetUserClaim(long userClaimId, CancellationToken cancellationToken)
		{
			return await DbContext.UserClaims
			                      .Where(u => u.Id == userClaimId)
			                      .Select(u => new UserClaimInfo
			                      {
				                      UserClaimId = u.Id,
				                      UserId = u.UserId,
				                      ClaimId = u.ClaimId,
				                      Name = u.Claim.Name,
				                      Timestamp = u.Timestamp
			                      } as IUserClaimInfo)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IUserClaimInfo> GetUserClaimByName(long userId, string claimName,
		                                                     CancellationToken cancellationToken)
		{
			return await DbContext.UserClaims
			                      .Where(u => u. UserId == userId && u.Claim.Name == claimName)
			                      .Select(u => new UserClaimInfo
			                      {
				                      UserClaimId = u.Id,
				                      UserId = u.UserId,
				                      ClaimId = u.ClaimId,
				                      Name = u.Claim.Name,
				                      Timestamp = u.Timestamp
			                      } as IUserClaimInfo)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IUserClaimInfo> CreateNewUserClaim(INewUserClaim newUserClaim, CancellationToken cancellationToken)
		{
			if (newUserClaim == null)
			{
				throw new ArgumentNullException(nameof(newUserClaim));
			}

			UserClaim userClaim = new UserClaim
			{
				Id = await _IdService.GetNewId(),
				UserId = newUserClaim.UserId,
				ClaimId = newUserClaim.ClaimId
			};

			DbContext.UserClaims.Add(userClaim);

			await DbContext.SaveChangesAsync(cancellationToken);
			
			return await GetUserClaim(userClaim.Id, cancellationToken);
		}

		public async Task DeleteUserClaim(long userClaimId, CancellationToken cancellationToken)
		{
			UserClaim userClaim = await DbContext.UserClaims
			                               .FirstOrDefaultAsync(u => u.Id == userClaimId, cancellationToken);

			if (userClaim == null)
			{
				throw new RecordNotFoundException("UserClaim", userClaimId);
			}

			DbContext.UserClaims.Remove(userClaim);

			await DbContext.SaveChangesAsync(cancellationToken);
		}
	}
}
