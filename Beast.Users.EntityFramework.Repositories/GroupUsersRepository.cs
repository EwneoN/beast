﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Exceptions;
using Beast.Services.Ids;
using Beast.Users.DTOs.Groups;
using Beast.Users.DTOs.GroupUsers;
using Beast.Users.Models;
using Beast.Users.Repositories;

namespace Beast.Users.EntityFramework.Repositories
{
	public class GroupUsersRepository : UsersContextRepository, IGroupUsersRepository
	{
		private readonly IIdService _IdService;

		public GroupUsersRepository(IUsersContext dbContext, IIdService idService) : base(dbContext)
		{
			_IdService = idService;
		}

		public async Task<bool> DoesGroupUserExist(long groupUserId, CancellationToken cancellationToken)
		{
			return await DbContext.GroupUsers.AnyAsync(c => c.Id == groupUserId, cancellationToken);
		}

		public async Task<bool> DoesGroupUserExist(long groupId, long userId, CancellationToken cancellationToken)
		{
			return await DbContext.GroupUsers.AnyAsync(c => c.GroupId == groupId && c.UserId == userId, cancellationToken);
		}

		public async Task<bool> DoesGroupUserExist(long groupId, string username, CancellationToken cancellationToken)
		{
			return await DbContext.GroupUsers.AnyAsync(c => c.GroupId == groupId && c.User.Username == username, cancellationToken);
		}

		public async Task<IPagedCollection<IGroupUserInfo>> GetGroupUsers(IGroupPagedQueryParams queryParams,
		                                                                  CancellationToken cancellationToken)
		{
			var totalCount = await DbContext.GroupUsers.CountAsync(cancellationToken);
			var groupUsers = await DbContext.GroupUsers
			                                .Skip(queryParams.PageNumber - 1 * queryParams.PageSize)
			                                .Take(queryParams.PageSize)
			                                .Select(u => new GroupUserInfo
			                                {
				                                GroupUserId = u.Id,
				                                GroupId = u.GroupId,
				                                UserId = u.UserId,
				                                Name = u.User.Username,
				                                Timestamp = u.Timestamp
			                                } as IGroupUserInfo)
			                                .ToArrayAsync(cancellationToken);

			return new PagedCollection<IGroupUserInfo>(queryParams, totalCount, groupUsers);
		}

		public async Task<IGroupUserInfo> GetGroupUser(long groupUserId, CancellationToken cancellationToken)
		{
			return await DbContext.GroupUsers
			                      .Where(u => u.Id == groupUserId)
			                      .Select(u => new GroupUserInfo
			                      {
				                      GroupUserId = u.Id,
				                      GroupId = u.GroupId,
				                      UserId = u.UserId,
				                      Name = u.User.Username,
				                      Timestamp = u.Timestamp
			                      } as IGroupUserInfo)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IGroupUserInfo> GetGroupUserByUsername(long groupId, string userName,
		                                                         CancellationToken cancellationToken)
		{
			return await DbContext.GroupUsers
			                      .Where(u => u.GroupId == groupId && u.User.Username == userName)
			                      .Select(u => new GroupUserInfo
			                      {
				                      GroupUserId = u.Id,
				                      GroupId = u.GroupId,
				                      UserId = u.UserId,
				                      Name = u.User.Username,
				                      Timestamp = u.Timestamp
			                      } as IGroupUserInfo)
			                      .FirstOrDefaultAsync(cancellationToken);
		}

		public async Task<IGroupUserInfo> CreateNewGroupUser(INewGroupUser newGroupUser,
		                                                     CancellationToken cancellationToken)
		{
			if (newGroupUser == null)
			{
				throw new ArgumentNullException(nameof(newGroupUser));
			}

			GroupUser groupUser = new GroupUser
			{
				Id = await _IdService.GetNewId(),
				GroupId = newGroupUser.GroupId,
				UserId = newGroupUser.UserId
			};

			DbContext.GroupUsers.Add(groupUser);

			await DbContext.SaveChangesAsync(cancellationToken);

			return await GetGroupUser(groupUser.Id, cancellationToken);
		}

		public async Task DeleteGroupUser(long groupUserId, CancellationToken cancellationToken)
		{
			GroupUser groupUser = await DbContext.GroupUsers
			                                     .FirstOrDefaultAsync(u => u.Id == groupUserId, cancellationToken);

			if (groupUser == null)
			{
				throw new RecordNotFoundException("GroupUser", groupUserId);
			}

			DbContext.GroupUsers.Remove(groupUser);

			await DbContext.SaveChangesAsync(cancellationToken);
		}
	}
}