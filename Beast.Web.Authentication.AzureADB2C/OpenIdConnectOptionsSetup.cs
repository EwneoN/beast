﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;

namespace Beast.Web.Authentication.AzureADB2C
{
	public static class AzureAdB2CAuthenticationBuilderExtensions
	{
		public static AuthenticationBuilder AddAzureAdB2C(this AuthenticationBuilder builder)
				=> builder.AddAzureAdB2C(_ =>
				{
				});

		public static AuthenticationBuilder AddAzureAdB2C(this AuthenticationBuilder builder, Action<AzureAdB2COptions> configureOptions)
		{
			builder.Services.Configure(configureOptions);
			builder.Services.AddSingleton<IConfigureOptions<OpenIdConnectOptions>, OpenIdConnectOptionsSetup>();
			builder.AddOpenIdConnect();
			return builder;
		}

		public class OpenIdConnectOptionsSetup : IConfigureNamedOptions<OpenIdConnectOptions>
		{
			private readonly OpenIdConnectEvents _Events;

			public AzureAdB2COptions AzureAdB2COptions { get; set; }

			public OpenIdConnectOptionsSetup(IOptions<AzureAdB2COptions> b2cOptions, OpenIdConnectEvents events)
			{
				_Events = events;
				AzureAdB2COptions = b2cOptions.Value;
			}

			public void Configure(string name, OpenIdConnectOptions options)
			{
				options.ClientId = AzureAdB2COptions.ClientId;
				options.Authority = AzureAdB2COptions.Authority;
				options.UseTokenLifetime = true;
				options.TokenValidationParameters = new TokenValidationParameters { NameClaimType = "name" };
				options.Events = _Events;
			}

			public void Configure(OpenIdConnectOptions options)
			{
				Configure(Options.DefaultName, options);
			}

			public Task DefaultOnRedirectToIdentityProvider(RedirectContext context)
			{
				var defaultPolicy = AzureAdB2COptions.DefaultPolicy;
				if (context.Properties.Items.TryGetValue(AzureAdB2COptions.PolicyAuthenticationProperty, out var policy) &&
						!policy.Equals(defaultPolicy))
				{
					context.ProtocolMessage.Scope = OpenIdConnectScope.OpenIdProfile;
					context.ProtocolMessage.ResponseType = OpenIdConnectResponseType.IdToken;
					context.ProtocolMessage.IssuerAddress = context.ProtocolMessage.IssuerAddress.ToLower().Replace(defaultPolicy.ToLower(), policy.ToLower());
					context.Properties.Items.Remove(AzureAdB2COptions.PolicyAuthenticationProperty);
				}
				else if (!string.IsNullOrEmpty(AzureAdB2COptions.ApiUrl))
				{
					context.ProtocolMessage.Scope += $" offline_access {AzureAdB2COptions.ApiScopes}";
					context.ProtocolMessage.ResponseType = OpenIdConnectResponseType.CodeIdToken;
				}
				return Task.FromResult(0);
			}
		}
	}
}
