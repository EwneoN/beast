﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Beast.Encryption;
using Beast.Models;
using Beast.Services.Ids;
using Beast.Services.Time;
using Z.EntityFramework.Extensions;

namespace Beast.EntityFramework.Encryption
{
	public abstract class EncryptedDbContext : BeastDbContextBase
	{
		public IEncryptionService EncryptionService { get; }

		protected EncryptedDbContext(IIdService idService, ITimeService timeService, BeastDbContextConfig config, IEncryptionService encryptionService)
			: base(idService, timeService, config)
		{
			EncryptionService = encryptionService ?? throw new ArgumentNullException(nameof(config));
		}

		public override int SaveChanges()
		{
			var entries = ChangeTracker.Entries<BeastModel>().ToArray();

			foreach (DbEntityEntry<BeastModel> entry in entries)
			{
				if (entry.State == EntityState.Added)
				{
					Task<long> task = IdService.GetNewId();
					Task.WaitAll(task);

					entry.Entity.Id = task.Result;
				}

				entry.Entity.Timestamp = TimeService.UtcNow;

				Task<BeastModel> encryptTask = EncryptionService.Encrypt(entry.Entity);
				Task.WaitAll(encryptTask);
			}

			var result = base.SaveChanges();

			foreach (DbEntityEntry<BeastModel> entry in entries)
			{
				Task<BeastModel> decryptTask = EncryptionService.Decrypt(entry.Entity);
				Task.WaitAll(decryptTask);
			}

			return result;
		}

		public override async Task<int> SaveChangesAsync()
		{
			return await SaveChangesAsync(CancellationToken.None);
		}

		public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
		{
			var entries = ChangeTracker.Entries<BeastModel>().ToArray();

			foreach (DbEntityEntry<BeastModel> entry in entries)
			{
				if (entry.State == EntityState.Added)
				{
					entry.Entity.Id = await IdService.GetNewId();
				}

				entry.Entity.Timestamp = TimeService.UtcNow;
				await EncryptionService.Encrypt(entry.Entity);
			}

			var result = await base.SaveChangesAsync(cancellationToken);

			foreach (DbEntityEntry<BeastModel> entry in entries)
			{
				await EncryptionService.Decrypt(entry.Entity);
			}

			return result;
		}

		public override async Task BulkUpdateAsync<T>(ICollection<T> entities)
		{
			await BulkUpdateAsync(entities, CancellationToken.None);
		}

		public override async Task BulkUpdateAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkUpdateAsync(entities, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		public override async Task BulkUpdateAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
		{
			await BulkUpdateAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public override async Task BulkUpdateAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkUpdateAsync(entities, bulkOperationFactory, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		public override async Task BulkInsertAsync<T>(ICollection<T> entities)
		{
			await BulkInsertAsync(entities, CancellationToken.None);
		}

		public override async Task BulkInsertAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkInsertAsync(entities, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		public override async Task BulkInsertAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
		{
			await BulkInsertAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public override async Task BulkInsertAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkInsertAsync(entities, bulkOperationFactory, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		public override async Task BulkMergeAsync<T>(ICollection<T> entities)
		{
			await BulkMergeAsync(entities, CancellationToken.None);
		}

		public override async Task BulkMergeAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkMergeAsync(entities, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		public override async Task BulkMergeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
		{
			await BulkMergeAsync(entities, bulkOperationFactory, CancellationToken.None);
		}

		public override async Task BulkMergeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkMergeAsync(entities, bulkOperationFactory, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		public override async Task BulkSynchronizeAsync<T>(ICollection<T> entities)
		{
			await BulkSynchronizeAsync(entities, CancellationToken.None);
		}

		public override async Task BulkSynchronizeAsync<T>(ICollection<T> entities, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkSynchronizeAsync(entities, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		public override async Task BulkSynchronizeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory)
		{
			await BulkSynchronizeAsync(entities, CancellationToken.None);
		}

		public override async Task BulkSynchronizeAsync<T>(ICollection<T> entities, Action<EntityBulkOperation<T>> bulkOperationFactory, CancellationToken cancellationToken)
		{
			foreach (T entity in entities)
			{
				await EncryptionService.Encrypt(entity);
			}

			await base.BulkSynchronizeAsync(entities, bulkOperationFactory, cancellationToken);

			foreach (T entity in entities)
			{
				await EncryptionService.Decrypt(entity);
			}
		}

		protected void OnModelCreating(DbModelBuilder modelBuilder, params Assembly[] assemblies)
		{
			base.OnModelCreating(modelBuilder);

			if (assemblies == null || !assemblies.Any())
			{
				return;
			}

			foreach (Assembly assembly in assemblies)
			{
				modelBuilder.Configurations.AddFromAssembly(assembly);
			}
		}
	}
}