﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Beast.FileSystems;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Beast.Azure.BlobStorage
{
	public class BlobFileSystem : IFileSystem
	{
		private readonly BlobFileSystemConfig _Config;

		public BlobFileSystem(BlobFileSystemConfig config)
		{
			_Config = config ?? throw new ArgumentNullException(nameof(config));
		}

		public async Task<bool> DoesDirectoryExist(string path, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			int lastSlashIndex = path.LastIndexOf('/');

			string prefix = lastSlashIndex >= 0 ? path.Remove(lastSlashIndex) : path;

			var client = GetClient();

			return (await client
						 .GetContainerReference(_Config.Container)
			       .ListBlobsSegmentedAsync(prefix, new BlobContinuationToken(), cancellationToken))
						 .Results
			       .Any();
		}

		public async Task<bool> DoesFileExist(string path, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			var client = GetClient();

			return await client
										.GetContainerReference(_Config.Container)
			              .GetBlockBlobReference(path)
			              .ExistsAsync(cancellationToken);
		}

		public async Task DeleteFile(string path, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			var client = GetClient();

			await client
						.GetContainerReference(_Config.Container)
			      .GetBlockBlobReference(path)
			      .DeleteAsync(cancellationToken);
		}

		public async Task<Stream> ReadFileToStream(string path, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			MemoryStream stream = new MemoryStream();

			var client = GetClient();

			await client
						.GetContainerReference(_Config.Container)
			      .GetBlockBlobReference(path)
			      .DownloadToStreamAsync(stream, cancellationToken);

			if (stream.Length == 0)
			{
				//TODO better exception
				throw new Exception("Failed to read file due to empty stream from server");
			}

			stream.Position = 0;

			return stream;
		}

		public async Task<string> ReadFileToString(string path, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			using (Stream stream = await ReadFileToStream(path, cancellationToken))
			using (StreamReader reader = new StreamReader(stream))
			{
				return await reader.ReadToEndAsync();
			}
		}

		public async Task<byte[]> ReadFileToByteArray(string path, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			using (MemoryStream stream = await ReadFileToStream(path, cancellationToken) as MemoryStream)
			{
				if (stream == null)
				{
					//TODO better exception
					throw new Exception("Failed to read file due to null stream");
				}

				return stream.ToArray();
			}
		}

		public async Task<ICollection<Stream>> ReadFilesToStreams(string directory, string wildCard, CancellationToken cancellationToken)
		{
			if (directory == null)
			{
				throw new ArgumentNullException(nameof(directory));
			}

			if (wildCard == null)
			{
				throw new ArgumentNullException(nameof(wildCard));
			}

			var client = GetClient();

			IEnumerable<IListBlobItem> blobs = client
									.GetContainerReference(_Config.Container)
			            .ListBlobs(directory, true)
			            .Where(b =>
			            {
				            if (!Path.HasExtension(b.Uri.AbsolutePath))
				            {
					            return string.Equals(wildCard, "*");
				            }

				            string pattern = $"^{wildCard.Replace(".", @"\.").Replace("*", ".?")}$";

				            return Regex.IsMatch(b.Uri.AbsolutePath, pattern, RegexOptions.IgnoreCase);
			            });

			List<Stream> streams = new List<Stream>();

			foreach (CloudBlockBlob blob in blobs.OfType<CloudBlockBlob>())
			{
				MemoryStream stream = new MemoryStream();

				await blob.DownloadToStreamAsync(stream, cancellationToken);

				if (stream.Length == 0)
				{
					//TODO better exception
					throw new Exception("Failed to read file due to empty stream from server");
				}

				stream.Position = 0;

				streams.Add(stream);
			}

			return streams;
		}

		public async Task<ICollection<Stream>> ReadFilesToStreams(string wildCard, CancellationToken cancellationToken)
		{
			if (wildCard == null)
			{
				throw new ArgumentNullException(nameof(wildCard));
			}

			var client = GetClient();

			IEnumerable<IListBlobItem> blobs = client
			      .GetContainerReference(_Config.Container)
			      .ListBlobs(useFlatBlobListing: true)
			      .Where(b =>
			      {
				      if (!Path.HasExtension(b.Uri.AbsolutePath))
				      {
					      return string.Equals(wildCard, "*");
				      }

				      string pattern = $"^{wildCard.Replace(".", @"\.").Replace("*", ".?")}$";

				      return Regex.IsMatch(b.Uri.AbsolutePath, pattern, RegexOptions.IgnoreCase);
			      });

			List<Stream> streams = new List<Stream>();

			foreach (CloudBlockBlob blob in blobs.OfType<CloudBlockBlob>())
			{
				MemoryStream stream = new MemoryStream();

				await blob.DownloadToStreamAsync(stream, cancellationToken);

				if (stream.Length == 0)
				{
					//TODO better exception
					throw new Exception("Failed to read file due to empty stream from server");
				}

				stream.Position = 0;

				streams.Add(stream);
			}
			
			return streams;
		}

		public async Task<ICollection<byte[]>> ReadFilesToByteArrays(string directory, string wildCard, CancellationToken cancellationToken)
		{
			if (directory == null)
			{
				throw new ArgumentNullException(nameof(directory));
			}

			if (wildCard == null)
			{
				throw new ArgumentNullException(nameof(wildCard));
			}

			var client = GetClient();

			IEnumerable<IListBlobItem> blobs = client
									.GetContainerReference(_Config.Container)
									.ListBlobs(directory, true)
									.Where(b =>
									{
										if (!Path.HasExtension(b.Uri.AbsolutePath))
										{
											return string.Equals(wildCard, "*");
										}

										string pattern = $"^{wildCard.Replace(".", @"\.").Replace("*", ".?")}$";

										return Regex.IsMatch(b.Uri.AbsolutePath, pattern, RegexOptions.IgnoreCase);
									});

			List<byte[]> arrays = new List<byte[]>();

			foreach (CloudBlockBlob blob in blobs.OfType<CloudBlockBlob>())
			{
				using (MemoryStream stream = new MemoryStream())
				{
					await blob.DownloadToStreamAsync(stream, cancellationToken);

					if (stream.Length == 0)
					{
						//TODO better exception
						throw new Exception("Failed to read file due to empty stream from server");
					}

					arrays.Add(stream.ToArray());
				}
			}

			return arrays;
		}

		public async Task<ICollection<byte[]>> ReadFilesToByteArrays(string wildCard, CancellationToken cancellationToken)
		{
			if (wildCard == null)
			{
				throw new ArgumentNullException(nameof(wildCard));
			}

			var client = GetClient();

			IEnumerable<IListBlobItem> blobs = client
						.GetContainerReference(_Config.Container)
						.ListBlobs(useFlatBlobListing: true)
						.Where(b =>
						{
							if (!Path.HasExtension(b.Uri.AbsolutePath))
							{
								return string.Equals(wildCard, "*");
							}

							string pattern = $"^{wildCard.Replace(".", @"\.").Replace("*", ".?")}$";

							return Regex.IsMatch(b.Uri.AbsolutePath, pattern, RegexOptions.IgnoreCase);
						});

			List<byte[]> arrays = new List<byte[]>();

			foreach (CloudBlockBlob blob in blobs.OfType<CloudBlockBlob>())
			{
				using (MemoryStream stream = new MemoryStream())
				{
					await blob.DownloadToStreamAsync(stream, cancellationToken);

					if (stream.Length == 0)
					{
						//TODO better exception
						throw new Exception("Failed to read file due to empty stream from server");
					}

					arrays.Add(stream.ToArray());
				}
			}

			return arrays;
		}

		public async Task WriteFile(string path, Stream data, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (data == null)
			{
				throw new ArgumentNullException(nameof(data));
			}

			var client = GetClient();

			await client
			      .GetContainerReference(_Config.Container)
			      .GetBlockBlobReference(path)
			      .UploadFromStreamAsync(data, cancellationToken);
		}

		public async Task WriteFile(string path, string data, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (data == null)
			{
				throw new ArgumentNullException(nameof(data));
			}

			var client = GetClient();

			await client
			      .GetContainerReference(_Config.Container)
			      .GetBlockBlobReference(path)
			      .UploadTextAsync(data, cancellationToken);
		}

		public async Task WriteFile(string path, byte[] data, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (data == null)
			{
				throw new ArgumentNullException(nameof(data));
			}

			var client = GetClient();

			await client
						.GetContainerReference(_Config.Container)
			      .GetBlockBlobReference(path)
			      .UploadFromByteArrayAsync(data, 0, data.Length, cancellationToken);
		}

		public async Task AppendFile(string path, Stream data, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (data == null)
			{
				throw new ArgumentNullException(nameof(data));
			}

			byte[] existingData = await ReadFileToByteArray(path, cancellationToken);

			using (MemoryStream tempStream = new MemoryStream())
			{
				int length = checked((int)data.Length);

				await data.CopyToAsync(tempStream, length, cancellationToken);
				
				byte[] newData = tempStream.ToArray();
				byte[] combined = existingData.Concat(newData).ToArray();

				var client = GetClient();

				await client
								.GetContainerReference(_Config.Container)
				      .GetBlockBlobReference(path)
				      .UploadFromByteArrayAsync(combined, 0, combined.Length, cancellationToken);
			}
		}

		public async Task AppendFile(string path, string data, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (data == null)
			{
				throw new ArgumentNullException(nameof(data));
			}

			string existingData = await ReadFileToString(path, cancellationToken);
			StringBuilder stringBuilder = new StringBuilder(existingData);
			stringBuilder.Append(data);
			string combined = stringBuilder.ToString();

			var client = GetClient();

			await client
						.GetContainerReference(_Config.Container)
			      .GetBlockBlobReference(path)
			      .UploadTextAsync(combined, cancellationToken);
		}

		public async Task AppendFile(string path, byte[] data, CancellationToken cancellationToken)
		{
			if (path == null)
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (data == null)
			{
				throw new ArgumentNullException(nameof(data));
			}

			byte[] existingData = await ReadFileToByteArray(path, cancellationToken);
			byte[] combined = existingData.Concat(data).ToArray();

			var client = GetClient();

			await client
						.GetContainerReference(_Config.Container)
			      .GetBlockBlobReference(path)
			      .UploadFromByteArrayAsync(combined, 0, combined.Length, cancellationToken);
		}

		private CloudBlobClient GetClient()
		{
			CloudStorageAccount.TryParse(_Config.ConnectionString, out CloudStorageAccount account);

			return account.CreateCloudBlobClient();
		}
	}
}
