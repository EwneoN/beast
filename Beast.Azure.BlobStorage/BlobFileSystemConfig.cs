﻿using System.Security.Cryptography.X509Certificates;

namespace Beast.Azure.BlobStorage
{
	public class BlobFileSystemConfig
	{
		public string Container { get; set; }
		public string ConnectionString { get; set; }
	}
}