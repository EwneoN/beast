﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using AzureADGraphClient;
using Beast.AutoFac;
using Beast.CQRS.Behaviours;
using Beast.Dynamics;
using Beast.Encryption;
using Beast.EnterpriseBus.Clients;
using Beast.FileSystems;
using Beast.Logging.CQRS.Behaviours;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.AzureAD.Services;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Behaviours;
using Beast.Users.CQRS.Requests.Users.GetUser;
using Beast.Users.EntityFramework;
using Beast.Users.EntityFramework.Repositories;
using Beast.Users.Repositories;
using Beast.Users.Services;
using Beast.Users.Services.Default;
using Beast.Validation.CQRS.Behaviours;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using IAuthenticationService = Beast.Users.Services.IAuthenticationService;

namespace Beast.Users.AutoFac
{
	public abstract class UsersModule<TIdService, TTimeService> : BeastModule<TIdService, TTimeService>
		where TIdService : IIdService
		where TTimeService : ITimeService
	{
		public UsersModule(IConfiguration configuration)
			: base(configuration) { }

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			PreRegisterAction(builder);

			builder.RegisterGeneric(typeof(LogRequestBehaviour<,>))
						 .As(typeof(IPipelineBehavior<,>))
						 .InstancePerLifetimeScope();
			builder.RegisterGeneric(typeof(UserAuthorisationBehaviour<,>))
						 .As(typeof(IPipelineBehavior<,>))
						 .InstancePerLifetimeScope();
			builder.RegisterGeneric(typeof(ValidationBehaviour<,>))
						 .As(typeof(IPipelineBehavior<,>))
						 .InstancePerLifetimeScope();
			builder.RegisterGeneric(typeof(DefaultResponseBehaviour<,>))
						 .As(typeof(IPipelineBehavior<,>))
						 .InstancePerLifetimeScope();

			builder.Register(context => new CustomAssemblyLocationTypeResolverConfig
							{
								AssembliesLocation = Configuration["Assemblies:Location"]
							})
						 .As<CustomAssemblyLocationTypeResolverConfig>()
						 .SingleInstance();

			builder.RegisterType<CustomAssemblyLocationTypeResolver>()
						 .As<ITypeResolver>()
						 .SingleInstance();

			//repositories
			builder.RegisterType<ClaimsRepository>()
						 .As<IClaimsRepository>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<UsersRepository>()
						 .As<IUsersRepository>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<UserClaimsRepository>()
						 .As<IUserClaimsRepository>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<GroupsRepository>()
						 .As<IGroupsRepository>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<GroupClaimsRepository>()
						 .As<IGroupClaimsRepository>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<GroupUsersRepository>()
						 .As<IGroupUsersRepository>()
						 .InstancePerLifetimeScope();

			//per lifetime scope
			builder.RegisterType<AzureB2CService>()
						 .As<IAuthenticationService>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<ClaimsService>()
						 .As<IClaimsService>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<UsersService>()
						 .As<IUsersService>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<UserClaimsService>()
						 .As<IUserClaimsService>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<GroupsService>()
						 .As<IGroupsService>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<GroupClaimsService>()
						 .As<IGroupClaimsService>()
						 .InstancePerLifetimeScope();
			builder.RegisterType<GroupUsersService>()
						 .As<IGroupUsersService>()
						 .InstancePerLifetimeScope();

			Assembly usersAssembly = typeof(GetUserRequest).GetTypeInfo().Assembly;

			Type[] userAssemblyTypes = usersAssembly.ExportedTypes.ToArray();

			builder
				.RegisterTypes(userAssemblyTypes
											 .Where(t => !t.IsAbstract && !t.IsInterface &&
																	 t.GetInterfaces()
																		.Any(i => !t.IsAbstract && i.IsGenericType && i.GetGenericTypeDefinition() ==
																							typeof(IUserRequestAuthoriser<,>))).ToArray())
				.AsClosedTypesOf(typeof(IUserRequestAuthoriser<,>))
				.InstancePerLifetimeScope();

			builder
				.RegisterTypes(userAssemblyTypes
											 .Where(t => !t.IsAbstract &&
																	 !t.IsInterface &&
																	 t.GetInterfaces()
																		.Any(i => i.IsGenericType &&
																							i.GetGenericTypeDefinition() ==
																							typeof(IValidator<>))).ToArray())
				.AsClosedTypesOf(typeof(IValidator<>))
				.InstancePerLifetimeScope();

			foreach (Type mediatrOpenType in MediatrHandlerInterfaces)
			{
				Type[] types = userAssemblyTypes
					.Where(t => !t.IsAbstract && !t.IsInterface &&
											t.GetInterfaces()
											 .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == mediatrOpenType))
											 .ToArray();

				builder
					.RegisterTypes(types)
					.AsClosedTypesOf(mediatrOpenType)
					.InstancePerLifetimeScope();
			}

			PostRegisterAction(builder);
		}

		protected abstract void PreRegisterAction(ContainerBuilder builder);

		protected abstract void PostRegisterAction(ContainerBuilder builder);
	}
}