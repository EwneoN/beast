﻿namespace Beast.Requests
{
	public class AccessContext : IAccessContext
	{
		public string AuthId { get; set; }
		public bool IsAuthenticated { get; set; }
	}
}
