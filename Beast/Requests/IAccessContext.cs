﻿namespace Beast.Requests
{
	public interface IAccessContext
	{
		string AuthId { get; set; }
		bool IsAuthenticated { get; set; }
	}
}