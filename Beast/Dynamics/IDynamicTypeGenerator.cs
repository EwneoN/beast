﻿using System;

namespace Beast.Dynamics
{
	public interface IDynamicTypeGenerator
	{
		Type GetDynamicType<T>();
	}
}
