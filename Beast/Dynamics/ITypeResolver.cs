﻿using System;

namespace Beast.Dynamics
{
	public interface ITypeResolver
	{
		Type GetType(string fullName);
	}
}