﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Beast.Dynamics
{
	public class StandardTypeResolver : ITypeResolver
	{
		private readonly Dictionary<string, Type> _Types;
		
		public StandardTypeResolver()
		{
			_Types = new Dictionary<string, Type>();

			Type[] types = AppDomain.CurrentDomain
			                     .GetAssemblies()
			                     .SelectMany(a => a.ExportedTypes)
			                     .ToArray();

			foreach (Type type in types)
			{
				_Types.Add(type.FullName ?? type.Name, type);
			}
		}

		public Type GetType(string fullName)
		{
			return _Types[fullName];
		}
	}
}
