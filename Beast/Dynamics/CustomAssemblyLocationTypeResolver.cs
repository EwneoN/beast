﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Beast.Dynamics
{
	public class CustomAssemblyLocationTypeResolver : ITypeResolver
	{
		public static readonly string DefaultAssemblyLocation = "Assemblies";

		private static readonly ConcurrentDictionary<string, Assembly> _DynamicallyLoadedAssemblies;
		private static readonly ConcurrentDictionary<string, Type> _AppDomainTypes;
		private readonly ConcurrentDictionary<string, Type> _Types;

		static CustomAssemblyLocationTypeResolver()
		{
			_DynamicallyLoadedAssemblies = new ConcurrentDictionary<string, Assembly>();

			var appDomainAssemblies = AppDomain.CurrentDomain.GetAssemblies()
			                                   .Where(a => !a.IsDynamic && a.GetExportedTypes().Any())
			                                   .ToArray();
			var types = appDomainAssemblies
			                      .GroupBy(a => a.GetName().Name)
			                      .SelectMany(v => v.OrderByDescending(a => a.GetName().Version.Major)
			                                        .ThenByDescending(a => a.GetName().Version.Minor)
			                                        .ThenByDescending(a => a.GetName().Version.Revision)
			                                        .ThenByDescending(a => a.GetName().Version.Build)
			                                        .FirstOrDefault()?.ExportedTypes)
			                      .OrderBy(t => t.FullName)
			                      .ToArray();

			_AppDomainTypes = new ConcurrentDictionary<string, Type>();

			foreach (Type type in types)
			{
				if (_AppDomainTypes.ContainsKey(type.FullName))
				{
					continue;
				}

				_AppDomainTypes.AddOrUpdate(type.FullName, type, (s, t) => t);
			}

			
			AppDomain.CurrentDomain.AssemblyResolve += ResolveAssemblyHandler;
		}

		private static Assembly ResolveAssemblyHandler(object sender, ResolveEventArgs args)
		{
			if (_DynamicallyLoadedAssemblies.TryGetValue(args.Name, out Assembly assembly))
			{
				return assembly;
			}

			if (!args.Name.Contains("Retargetable=Yes"))
			{
				return AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName.Equals(args.Name));
			}

			Regex regex = new Regex(@"^[a-zA-Z0-9\.]+(?=,)");
			Match match = regex.Match(args.Name);
			string name = match.Value;

			return AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.GetName().Name.Equals(name));
		}

		public CustomAssemblyLocationTypeResolver() 
			: this(new CustomAssemblyLocationTypeResolverConfig
				{
					AssembliesLocation = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DefaultAssemblyLocation)
				})
		{
		}

		public CustomAssemblyLocationTypeResolver(CustomAssemblyLocationTypeResolverConfig config)
		{
			if (config == null)
			{
				throw new ArgumentNullException(nameof(config));
			}

			if (string.IsNullOrWhiteSpace(config.AssembliesLocation))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(config.AssembliesLocation));
			}

			if (!Directory.Exists(config.AssembliesLocation))
			{
				throw new ArgumentException("Directory does not exist.", nameof(config.AssembliesLocation));
			}

			var assemblies = Directory.GetFiles(config.AssembliesLocation, "*.dll")
			                          .Select(Assembly.LoadFrom)
			                          .ToArray();
			//we only work with the latest version of each file when resolving
			//we could look at improving this to work with multiple versions if ever required
			var types = assemblies.Where(a => a.ExportedTypes.Any())
			                     .GroupBy(a => a.GetName().Name)
			                     .SelectMany(v => v.OrderByDescending(a => a.GetName().Version.Major)
			                                       .ThenByDescending(a => a.GetName().Version.Minor)
			                                       .ThenByDescending(a => a.GetName().Version.Revision)
			                                       .ThenByDescending(a => a.GetName().Version.Build)
			                                       .FirstOrDefault()?.ExportedTypes)
			                                       .OrderBy(t => t.FullName)
			                                       .ToArray();

			foreach (Assembly assembly in assemblies)
			{
				_DynamicallyLoadedAssemblies.AddOrUpdate(assembly.FullName, assembly, (s, a) => a);
			}

			_Types = new ConcurrentDictionary<string, Type>(types.ToDictionary(t => t.FullName ?? t.Name, t => t));
		}

		public Type GetType(string fullName)
		{
			return _AppDomainTypes.TryGetValue(fullName, out Type appDomainType) ? appDomainType : _Types[fullName];
		}
	}
}