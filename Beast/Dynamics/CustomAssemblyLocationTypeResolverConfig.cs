﻿namespace Beast.Dynamics
{
	public class CustomAssemblyLocationTypeResolverConfig
	{
		public string AssembliesLocation { get; set; }
	}
}