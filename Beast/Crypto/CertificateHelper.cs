﻿using System.Security.Cryptography.X509Certificates;

namespace Beast.Crypto
{
	public class CertificateHelper
	{
		public static X509Certificate2 FindCertificateByThumbprint(StoreName storeName, StoreLocation storeLocation, string findValue, bool onlyValidCertificates = false)
		{
			X509Store store = new X509Store(storeName, storeLocation);

			try
			{
				store.Open(OpenFlags.ReadOnly);

				X509Certificate2Collection collection = store.Certificates
					.Find(X509FindType.FindByThumbprint, findValue, onlyValidCertificates);

				if (collection == null || collection.Count == 0)
				{
					return null;
				}

				return collection[0];
			}
			finally
			{
				store.Close();
			}
		}
	}
}
