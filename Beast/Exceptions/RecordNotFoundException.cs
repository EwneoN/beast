﻿using System;
using System.Linq;

namespace Beast.Exceptions
{
  public class RecordNotFoundException : Exception
  {
    public RecordNotFoundException(string recordDisplayName, long recordId)
      : base($"Failed to find {recordDisplayName} with an id of {recordId}") { }

	  public RecordNotFoundException(string recordDisplayName, long recordId1, long recordId2)
		  : base($"Failed to find {recordDisplayName} with ids of {recordId1} and {recordId2}") { }

		public RecordNotFoundException(string recordDisplayName, string searchKey, string searchTerm)
		  : base($"Failed to find {recordDisplayName} with {GetAOrAn(searchKey)} {searchKey} of {searchTerm}") { }

	  private static string GetAOrAn(string searchKey)
	  {
		  char[] vowels = { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' };

		  return vowels.Contains(searchKey[0]) ? "an" : "a";
	  }
  }
}
