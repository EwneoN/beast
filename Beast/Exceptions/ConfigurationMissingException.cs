﻿using System;

namespace Beast.Exceptions
{
	public class ConfigurationMissingException : Exception
	{
		public ConfigurationMissingException(Type typeMissingConfig)
			: base($"Configuration missing for {typeMissingConfig}") { }
		public ConfigurationMissingException(string typeMissingConfig)
			: base($"Configuration missing for {typeMissingConfig}") { }
	}
}
