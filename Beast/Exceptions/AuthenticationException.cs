﻿using System;

namespace Beast.Exceptions
{
	public class AuthenticationException : RequestException
	{
		private const string DefaultMessage = "Request has not been authenticated";

		public AuthenticationException(long requestId) 
			: base(requestId, DefaultMessage) { }
		public AuthenticationException(long requestId, Exception innerException) 
			: base(requestId, DefaultMessage, innerException) { }
	}
}
