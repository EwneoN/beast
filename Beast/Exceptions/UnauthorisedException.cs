﻿using System;

namespace Beast.Exceptions
{
	public class UnauthorisedException : RequestException
	{
		public string[] MissingClaims { get; }

		public UnauthorisedException(long requestId, string[] missingClaims)
			: base(requestId, $"Authorisation failed for request: {requestId}")
		{
			MissingClaims = missingClaims ?? throw new ArgumentNullException(nameof(missingClaims));
		}

		public UnauthorisedException(long requestId, string[] missingClaims, Exception innerException)
			: base(requestId, $"Authorisation failed for request: {requestId}", innerException)
		{
			MissingClaims = missingClaims ?? throw new ArgumentNullException(nameof(missingClaims));
		}
	}
}