﻿using System;

namespace Beast.Exceptions
{
	public abstract class RequestException : Exception
	{
		public long RequestId { get; }

		protected RequestException(long requestId)
		{
			RequestId = requestId;
		}

		protected RequestException(long requestId, string message) : base(message)
		{
			RequestId = requestId;
		}

		protected RequestException(long requestId, string message, Exception innerException) : base(message, innerException)
		{
			RequestId = requestId;
		}
	}
}
