﻿using System;
using System.Reflection;

namespace Beast
{
	public static class ReflectionExtensions
	{
		public static TAttribute GetAttribute<TSource, TAttribute>(this TSource enumVal)
			where TAttribute : Attribute
		{
			Type type = enumVal.GetType();
			MemberInfo[] memInfo = type.GetMember(enumVal.ToString());
			object[] attributes = memInfo[0].GetCustomAttributes(typeof(TAttribute), false);

			return (attributes.Length > 0) ? (TAttribute) attributes[0] : null;
		}
	}
}