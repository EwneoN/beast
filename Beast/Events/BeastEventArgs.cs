﻿using System;

namespace Beast.Events
{
	public class BeastEventArgs : EventArgs
	{
		public BeastEventArgs() { }
	}

	public class BeastEventArgs<TRequest> : EventArgs
	{
		public TRequest Request { get; }

		public BeastEventArgs(TRequest request)
		{
			Request = request;
		}
	}

	public class BeastEventArgs<TRequest, TResponse> : EventArgs
	{
		public TRequest Request { get; }
		public TResponse Response { get; }

		public BeastEventArgs(TRequest request, TResponse response)
		{
			Request = request;
			Response = response;
		}
	}
}
