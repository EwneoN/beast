﻿using System.Threading.Tasks;

namespace Beast.Events
{
	public delegate Task BeastEventHandler(object sender, BeastEventArgs eventArgs);
	public delegate Task BeastEventHandler<TRequest>(object sender, BeastEventArgs<TRequest> eventArgs);
	public delegate Task BeastEventHandler<TRequest, TResponse>(object sender, BeastEventArgs<TRequest, TResponse> eventArgs);
}
