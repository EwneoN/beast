﻿namespace Beast.Models
{
	public class EnumModel : BeastModel
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string Code { get; set; }
	}
}