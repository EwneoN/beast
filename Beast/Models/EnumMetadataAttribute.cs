﻿using System;

namespace Beast.Models
{
  [AttributeUsage(AttributeTargets.Field)]
  public class EnumMetadataAttribute : Attribute
  {
    public Type Model { get; }
    public string Name { get; set; }
    public string Module { get; set; }
    public string Resource { get; set; }
    public string Code { get; set; }
    public string Description { get; set; }

    public EnumMetadataAttribute(Type model)
    {
      Model = model;
    }
  }
}
