﻿using System;

namespace Beast.Models
{
  public interface ITimestampedModel
  {
    DateTimeOffset Timestamp { get; set; }
  }
}
