﻿using System;

namespace Beast.Models
{
	public abstract class BeastModel : ITimestampedModel
	{
		public long Id { get; set; }
		// public Dictionary<string, object> ExtendedProperties { get; set; }
		public DateTimeOffset Timestamp { get; set; }
	}
}
