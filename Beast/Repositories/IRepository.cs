﻿using System.Threading;
using System.Threading.Tasks;

namespace Beast.Repositories
{
	public interface IRepository
	{
		Task BeginTransaction(CancellationToken cancellationToken);
		Task CommitTransaction(CancellationToken cancellationToken);
		Task RollbackTransaction(CancellationToken cancellationToken);
	}
}
