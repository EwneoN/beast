﻿using System;

namespace Beast
{
	public static class EnumExtensions
	{
		public static T ToEnum<T>(this string value)
			where T : Enum
		{
			return value.ToEnum<T>(false);
		}

		public static T ToEnum<T>(this string value, bool ignoreCase)
			where T : Enum
		{
			return (T) Enum.Parse(typeof(T), value, ignoreCase);
		}
	}
}