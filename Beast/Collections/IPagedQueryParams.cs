﻿using System.Linq.Expressions;

namespace Beast.Collections
{
	public interface IPagedQueryParams
	{
		int PageNumber { get; set; }
		int PageSize { get; set; }
	}

	public interface IPagedQueryParams<T>
	{
		int PageNumber { get; set; }
		int PageSize { get; set; }
		Expression<T> Query { get; set; }
	}
}