﻿using System.Linq.Expressions;

namespace Beast.Collections
{
	public class PagedQueryParams : IPagedQueryParams
	{
		public int PageNumber { get; set; }
		public int PageSize { get; set; }
	}

	public class PagedQueryParams<T> : IPagedQueryParams<T>
	{
		public int PageNumber { get; set; }
		public int PageSize { get; set; }
		public Expression<T> Query { get; set; }
	}
}
