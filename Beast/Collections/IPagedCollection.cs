﻿namespace Beast.Collections
{
	public interface IPagedCollection<T>
	{
		int PageNumber { get; }
		int PageSize { get; }
		int TotalItems { get; }
		int TotalPages { get; }
		T[] Items { get; }
	}
}