﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Beast.Collections
{
	public class PagedCollection<T> : IPagedCollection<T>
	{
		public int PageNumber { get; }
		public int PageSize { get; }
		public int TotalItems { get; }
		public int TotalPages { get; }

		public T[] Items { get; }

		public PagedCollection(IPagedQueryParams queryParams, int totalItems, IEnumerable<T> items)
		{
			if (queryParams == null)
			{
				throw new ArgumentNullException(nameof(queryParams));
			}

			if (queryParams.PageNumber <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(queryParams.PageNumber));
			}

			if (queryParams.PageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(queryParams.PageSize));
			}

			if (items == null)
			{
				throw new ArgumentNullException(nameof(items));
			}

			PageNumber = queryParams.PageNumber;
			PageSize = queryParams.PageSize;
			TotalItems = totalItems;
			TotalPages = (int)Math.Ceiling((decimal)totalItems / queryParams.PageSize);
			Items = items.ToArray();
		}
	}
}
