﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Beast.FileSystems
{
	public interface IFileSystem
	{
		Task<bool> DoesDirectoryExist(string path, CancellationToken cancellationToken);
		Task<bool> DoesFileExist(string path, CancellationToken cancellationToken);
		Task DeleteFile(string path, CancellationToken cancellationToken);

		Task<Stream> ReadFileToStream(string path, CancellationToken cancellationToken);
		Task<string> ReadFileToString(string path, CancellationToken cancellationToken);
		Task<byte[]> ReadFileToByteArray(string path, CancellationToken cancellationToken);

		Task<ICollection<Stream>> ReadFilesToStreams(string wildCard, CancellationToken cancellationToken);
		Task<ICollection<Stream>> ReadFilesToStreams(string directory, string wildCard, CancellationToken cancellationToken);
		Task<ICollection<byte[]>> ReadFilesToByteArrays(string directory, string wildCard, CancellationToken cancellationToken);
		Task<ICollection<byte[]>> ReadFilesToByteArrays(string wildCard, CancellationToken cancellationToken);

		Task WriteFile(string path, Stream data, CancellationToken cancellationToken);
		Task WriteFile(string path, string data, CancellationToken cancellationToken);
		Task WriteFile(string path, byte[] data, CancellationToken cancellationToken);

		Task AppendFile(string path, Stream data, CancellationToken cancellationToken);
		Task AppendFile(string path, string data, CancellationToken cancellationToken);
		Task AppendFile(string path, byte[] data, CancellationToken cancellationToken);
	}
}