﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Beast.FileSystems
{
	public class StandardFileSystem : IFileSystem
	{
		public string Root { get; }

		public StandardFileSystem()
		{
			Root = AppDomain.CurrentDomain.BaseDirectory;
		}

		public StandardFileSystem(string root)
		{
			if (string.IsNullOrWhiteSpace(root))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(root));
			}

			Root = root;
		}

		public async Task<bool> DoesDirectoryExist(string path, CancellationToken cancellationToken)
		{
			return await Task.FromResult(Directory.Exists(path));
		}

		public async Task<bool> DoesFileExist(string path, CancellationToken cancellationToken)
		{
			return await Task.FromResult(File.Exists(path));
		}

		public async Task DeleteFile(string path, CancellationToken cancellationToken)
		{
			await Task.Run(() => File.Delete(path), cancellationToken);
		}

		public async Task<Stream> ReadFileToStream(string path, CancellationToken cancellationToken)
		{
			using (FileStream fileStream = File.OpenRead(GetFullPath(path)))
			{
				MemoryStream stream = new MemoryStream();

				checked
				{
					int length = (int)stream.Length;
					await fileStream.CopyToAsync(stream, length, cancellationToken);
				}

				stream.Position = 0;

				return stream;
			}
		}

		public async Task<string> ReadFileToString(string path, CancellationToken cancellationToken)
		{
			return await Task.FromResult(File.ReadAllText(GetFullPath(path)));
		}

		public async Task<byte[]> ReadFileToByteArray(string path, CancellationToken cancellationToken)
		{
			return await Task.FromResult(File.ReadAllBytes(GetFullPath(path)));
		}

		public async Task<ICollection<Stream>> ReadFilesToStreams(string wildCard, CancellationToken cancellationToken)
		{
			string[] files = Directory.GetFiles(Root, wildCard, SearchOption.AllDirectories);
			List<Stream> streams = new List<Stream>();

			foreach (string file in files)
			{
				using (FileStream fileStream = File.OpenRead(file))
				{
					MemoryStream stream = new MemoryStream();

					checked
					{
						int length = (int)stream.Length;
						await fileStream.CopyToAsync(stream, length, cancellationToken);
					}

					stream.Position = 0;

					streams.Add(stream);
				}
			}

			return streams;
		}

		public async Task<ICollection<Stream>> ReadFilesToStreams(string directory, string wildCard, CancellationToken cancellationToken)
		{
			string[] files = Directory.GetFiles(GetFullPath(directory), wildCard, SearchOption.AllDirectories);
			List<Stream> streams = new List<Stream>();

			foreach (string file in files)
			{
				using (FileStream fileStream = File.OpenRead(file))
				{
					MemoryStream stream = new MemoryStream();

					checked
					{
						int length = (int)stream.Length;
						await fileStream.CopyToAsync(stream, length, cancellationToken);
					}

					stream.Position = 0;

					streams.Add(stream);
				}
			}

			return streams;
		}

		public async Task<ICollection<byte[]>> ReadFilesToByteArrays(string directory, string wildCard, CancellationToken cancellationToken)
		{
			string[] files = Directory.GetFiles(GetFullPath(directory), wildCard, SearchOption.AllDirectories);
			List<byte[]> arrays = new List<byte[]>();

			foreach (string file in files)
			{
				using (FileStream fileStream = File.OpenRead(file))
				{
					MemoryStream stream = new MemoryStream();

					checked
					{
						int length = (int)stream.Length;
						await fileStream.CopyToAsync(stream, length, cancellationToken);
					}

					stream.Position = 0;

					arrays.Add(stream.ToArray());
				}
			}

			return arrays;
		}

		public async Task<ICollection<byte[]>> ReadFilesToByteArrays(string wildCard, CancellationToken cancellationToken)
		{
			string[] files = Directory.GetFiles(Root, wildCard, SearchOption.AllDirectories);
			List<byte[]> arrays = new List<byte[]>();

			foreach (string file in files)
			{
				using (FileStream fileStream = File.OpenRead(file))
				{
					MemoryStream stream = new MemoryStream();

					checked
					{
						int length = (int)stream.Length;
						await fileStream.CopyToAsync(stream, length, cancellationToken);
					}

					stream.Position = 0;

					arrays.Add(stream.ToArray());
				}
			}

			return arrays;
		}

		public async Task WriteFile(string path, Stream data, CancellationToken cancellationToken)
		{
			using (FileStream stream = File.OpenWrite(GetFullPath(path)))
			{
				checked
				{
					int length = (int)stream.Length;
					await data.CopyToAsync(stream, length, cancellationToken);
				}
			}
		}

		public async Task WriteFile(string path, string data, CancellationToken cancellationToken)
		{
			await Task.Run(() => File.WriteAllText(GetFullPath(path), data), cancellationToken);
		}

		public async Task WriteFile(string path, byte[] data, CancellationToken cancellationToken)
		{
			await Task.Run(() => File.WriteAllBytes(GetFullPath(path), data), cancellationToken);
		}

		public async Task AppendFile(string path, Stream data, CancellationToken cancellationToken)
		{
			using (FileStream stream = File.Open(GetFullPath(path), FileMode.Append))
			using (StreamReader streamReader = new StreamReader(data))
			using (StreamWriter streamWriter = new StreamWriter(stream))
			{
				string line;

				while ((line = streamReader.ReadLine()) != null)
				{
					await streamWriter.WriteLineAsync(line);
				}

				await stream.FlushAsync(cancellationToken);
			}
		}

		public async Task AppendFile(string path, string data, CancellationToken cancellationToken)
		{
			await Task.Run(() => File.AppendAllText(GetFullPath(path), data), cancellationToken);
		}

		public async Task AppendFile(string path, byte[] data, CancellationToken cancellationToken)
		{
			using (Stream stream = File.Open(GetFullPath(path), FileMode.Append))
			using (StreamReader streamReader = new StreamReader(new MemoryStream(data)))
			using (StreamWriter streamWriter = new StreamWriter(stream))
			{
				string line;

				while ((line = streamReader.ReadLine()) != null)
				{
					await streamWriter.WriteLineAsync(line);
				}

				await stream.FlushAsync(cancellationToken);
			}
		}

		private string GetFullPath(string path)
		{
			return Path.Combine(Root, path);
		}
	}
}