﻿using System;

namespace Beast.Services.Time
{
  public class StandardTimeService : ITimeService
  {
    public DateTimeOffset Now => DateTimeOffset.Now;
    public DateTimeOffset UtcNow => DateTimeOffset.UtcNow;
	}
}
