﻿using System;

namespace Beast.Services.Time
{
	public class FixedTimeService : ITimeService
	{
		private readonly DateTimeOffset _FixedTime;

		public DateTimeOffset Now => _FixedTime.ToLocalTime();
		public DateTimeOffset UtcNow => _FixedTime.ToUniversalTime();

		public FixedTimeService(DateTimeOffset fixedTime)
		{
			_FixedTime = fixedTime;
		}
	}
}