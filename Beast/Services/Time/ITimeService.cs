﻿using System;

namespace Beast.Services.Time
{
  public interface ITimeService
  {
    DateTimeOffset Now { get; }

    DateTimeOffset UtcNow { get; }
  }
}
