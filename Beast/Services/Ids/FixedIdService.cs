﻿using System.Threading.Tasks;

namespace Beast.Services.Ids
{
	public class FixedIdService : IIdService
	{
		public long Id { get; }

		public FixedIdService(long id)
		{
			Id = id;
		}

		public async Task<long> GetNewId()
		{
			return await Task.FromResult(Id);
		}
	}
}