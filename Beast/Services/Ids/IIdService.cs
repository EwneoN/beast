﻿using System.Threading.Tasks;

namespace Beast.Services.Ids
{
  public interface IIdService
  {
    Task<long> GetNewId();
  }
}
