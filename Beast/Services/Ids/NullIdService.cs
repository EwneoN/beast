﻿using System.Threading.Tasks;

namespace Beast.Services.Ids
{
	public class NullIdService : IIdService
	{
		public async Task<long> GetNewId()
		{
			return await Task.FromResult(0);
		}
	}
}