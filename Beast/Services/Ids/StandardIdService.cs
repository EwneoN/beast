﻿using System;
using System.Threading.Tasks;

namespace Beast.Services.Ids
{
	public class StandardIdService : IIdService
	{
		private readonly Random _IdGenerator;

		public StandardIdService()
		{
			_IdGenerator = new Random();
		}

		public async Task<long> GetNewId()
		{
			return await Task.FromResult(checked((long)_IdGenerator.NextDouble()));
		}
	}
}
