﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.UserClaims.UserClaimsRead
{
	public class UserClaimsReadNotification : INotification
	{
		public long UserId { get; }

		public UserClaimsReadNotification(long userId)
		{
			UserId = userId;
		}
	}
}