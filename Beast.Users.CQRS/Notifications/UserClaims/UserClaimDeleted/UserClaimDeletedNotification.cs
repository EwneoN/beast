﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.UserClaims.UserClaimDeleted
{
	public class UserClaimDeletedNotification : INotification
	{
		public long UserClaimId { get; }

		public UserClaimDeletedNotification(long userClaimId)
		{
			UserClaimId = userClaimId;
		}
	}
}
