﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.UserClaims.UserClaimDeleted;
using MediatR;

namespace Beast.Users.CQRS.Notifications.UserClaims.UserClaimDeleted
{
	public class UserClaimDeletedNotificationHandler : INotificationHandler<UserClaimDeletedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public UserClaimDeletedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(UserClaimDeletedNotification notification, CancellationToken cancellationToken)
		{
			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new UserClaimDeletedMessage(messageId, clientId, _TimeService.UtcNow, notification.UserClaimId);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}