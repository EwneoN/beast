﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.UserClaims.UserClaimRead
{
	public class UserClaimReadNotification : INotification
	{
		public long UserClaimId { get; }

		public UserClaimReadNotification(long userClaimId)
		{
			UserClaimId = userClaimId;
		}
	}
}