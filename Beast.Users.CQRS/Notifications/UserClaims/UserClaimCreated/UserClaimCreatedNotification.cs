﻿using System;
using Beast.Users.DTOs.UserClaims;
using MediatR;

namespace Beast.Users.CQRS.Notifications.UserClaims.UserClaimCreated
{
	public class UserClaimCreatedNotification : INotification
	{
		public IUserClaimInfo UserClaimInfo { get; }

		public UserClaimCreatedNotification(IUserClaimInfo userClaimInfo)
		{
			UserClaimInfo = userClaimInfo ?? throw new ArgumentNullException(nameof(userClaimInfo));
		}
	}
}