﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimRead
{
	public class GroupClaimReadNotification : INotification
	{
		public long GroupClaimId { get; }

		public GroupClaimReadNotification(long groupClaimId)
		{
			GroupClaimId = groupClaimId;
		}
	}
}