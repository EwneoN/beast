﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimsRead
{
	public class GroupClaimsReadNotification : INotification
	{
		public long GroupId { get; }

		public GroupClaimsReadNotification(long groupId)
		{
			GroupId = groupId;
		}
	}
}