﻿using System;
using Beast.Users.DTOs.GroupClaims;
using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimCreated
{
	public class GroupClaimCreatedNotification : INotification
	{
		public IGroupClaimInfo GroupClaimInfo { get; }

		public GroupClaimCreatedNotification(IGroupClaimInfo userClaimInfo)
		{
			GroupClaimInfo = userClaimInfo ?? throw new ArgumentNullException(nameof(userClaimInfo));
		}
	}
}