﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.GroupClaims.GroupClaimCreated;
using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimCreated
{
	public class GroupClaimCreatedNotificationHandler : INotificationHandler<GroupClaimCreatedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public GroupClaimCreatedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(GroupClaimCreatedNotification notification, CancellationToken cancellationToken)
		{
			if (notification.GroupClaimInfo == null)
			{
				return;
			}

			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new GroupClaimCreatedMessage(messageId, clientId, _TimeService.UtcNow, notification.GroupClaimInfo);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}