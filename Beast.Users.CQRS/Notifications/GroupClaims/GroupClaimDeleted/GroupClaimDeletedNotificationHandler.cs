﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.GroupClaims.GroupClaimDeleted;
using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimDeleted
{
	public class GroupClaimDeletedNotificationHandler : INotificationHandler<GroupClaimDeletedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public GroupClaimDeletedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(GroupClaimDeletedNotification notification, CancellationToken cancellationToken)
		{
			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new GroupClaimDeletedMessage(messageId, clientId, _TimeService.UtcNow, notification.GroupClaimId);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}