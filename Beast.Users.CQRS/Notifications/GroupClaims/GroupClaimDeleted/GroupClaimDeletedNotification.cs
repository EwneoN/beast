﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimDeleted
{
	public class GroupClaimDeletedNotification : INotification
	{
		public long GroupClaimId { get; }

		public GroupClaimDeletedNotification(long groupClaimId)
		{
			GroupClaimId = groupClaimId;
		}
	}
}
