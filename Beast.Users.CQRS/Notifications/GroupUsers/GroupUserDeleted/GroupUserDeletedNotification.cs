﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupUsers.GroupUserDeleted
{
	public class GroupUserDeletedNotification : INotification
	{
		public long GroupUserId { get; }

		public GroupUserDeletedNotification(long groupUserId)
		{
			GroupUserId = groupUserId;
		}
	}
}
