﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.GroupUsers.GroupUsersRead;
using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupUsers.GroupUsersRead
{
	public class GroupUsersReadNotificationHandler : INotificationHandler<GroupUsersReadNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public GroupUsersReadNotificationHandler(IIdService idService, ITimeService timeService,
		                                          IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(GroupUsersReadNotification notification, CancellationToken cancellationToken)
		{
			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new GroupUsersReadMessage(messageId, clientId, _TimeService.UtcNow, notification.QueryParams);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}