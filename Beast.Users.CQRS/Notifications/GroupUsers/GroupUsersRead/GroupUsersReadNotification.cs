﻿using System;
using Beast.Users.DTOs.Groups;
using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupUsers.GroupUsersRead
{
	public class GroupUsersReadNotification : INotification
	{
		public IGroupPagedQueryParams QueryParams { get; }

		public GroupUsersReadNotification(IGroupPagedQueryParams queryParams)
		{
			QueryParams = queryParams ?? throw new ArgumentNullException(nameof(queryParams));
		}
	}
}