﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupUsers.GroupUserRead
{
	public class GroupUserReadNotification : INotification
	{
		public long GroupUserId { get; }

		public GroupUserReadNotification(long groupUserId)
		{
			GroupUserId = groupUserId;
		}
	}
}