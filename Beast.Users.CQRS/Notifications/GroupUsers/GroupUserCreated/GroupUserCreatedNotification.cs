﻿using System;
using Beast.Users.DTOs.GroupUsers;
using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupUsers.GroupUserCreated
{
	public class GroupUserCreatedNotification : INotification
	{
		public IGroupUserInfo GroupUserInfo { get; }

		public GroupUserCreatedNotification(IGroupUserInfo userUserInfo)
		{
			GroupUserInfo = userUserInfo ?? throw new ArgumentNullException(nameof(userUserInfo));
		}
	}
}