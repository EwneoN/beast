﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.GroupUsers.GroupUserCreated;
using MediatR;

namespace Beast.Users.CQRS.Notifications.GroupUsers.GroupUserCreated
{
	public class GroupUserCreatedNotificationHandler : INotificationHandler<GroupUserCreatedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public GroupUserCreatedNotificationHandler(IIdService idService, ITimeService timeService,
		                                           IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(GroupUserCreatedNotification notification, CancellationToken cancellationToken)
		{
			if (notification.GroupUserInfo == null)
			{
				return;
			}

			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new GroupUserCreatedMessage(messageId, clientId, _TimeService.UtcNow, notification.GroupUserInfo);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}