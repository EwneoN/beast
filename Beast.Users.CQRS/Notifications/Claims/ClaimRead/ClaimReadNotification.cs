﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.Claims.ClaimRead
{
	public class ClaimReadNotification : INotification
	{
		public long ClaimId { get; }

		public ClaimReadNotification(long claimId)
		{
			ClaimId = claimId;
		}
	}
}