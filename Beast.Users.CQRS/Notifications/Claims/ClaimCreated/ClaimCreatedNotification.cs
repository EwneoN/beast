﻿using Beast.Users.DTOs.Claims;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Claims.ClaimCreated
{
	public class ClaimCreatedNotification : INotification
	{
		public IClaimInfo ClaimInfo { get; }

		public ClaimCreatedNotification(IClaimInfo claimInfo)
		{
			ClaimInfo = claimInfo;
		}
	}
}