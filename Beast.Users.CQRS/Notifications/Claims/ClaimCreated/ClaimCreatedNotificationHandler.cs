﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.Claims.ClaimCreated;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Claims.ClaimCreated
{
	public class ClaimCreatedNotificationHandler : INotificationHandler<ClaimCreatedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public ClaimCreatedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(ClaimCreatedNotification notification, CancellationToken cancellationToken)
		{
			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new ClaimCreatedMessage(messageId, clientId, _TimeService.UtcNow, notification.ClaimInfo);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}