﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.Claims.ClaimDeleted
{
	public class ClaimDeletedNotification : INotification
	{
		public long ClaimId { get; }

		public ClaimDeletedNotification(long claimId)
		{
			ClaimId = claimId;
		}
	}
}