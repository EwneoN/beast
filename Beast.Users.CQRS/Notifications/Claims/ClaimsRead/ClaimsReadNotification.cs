﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.Claims.ClaimsRead
{
	public class ClaimsReadNotification : INotification { }
}