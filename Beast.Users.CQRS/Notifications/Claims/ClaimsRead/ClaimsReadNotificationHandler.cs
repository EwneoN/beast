﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.Claims.ClaimsRead;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Claims.ClaimsRead
{
	public class ClaimsReadNotificationHandler : INotificationHandler<ClaimsReadNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public ClaimsReadNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(ClaimsReadNotification notification, CancellationToken cancellationToken)
		{
			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new ClaimsReadMessage(messageId, clientId, _TimeService.UtcNow);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}