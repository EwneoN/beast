﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.Users.UserRead
{
	public class UserReadNotification : INotification
	{
		public long UserId { get; }

		public UserReadNotification(long userId)
		{
			UserId = userId;
		}
	}
}