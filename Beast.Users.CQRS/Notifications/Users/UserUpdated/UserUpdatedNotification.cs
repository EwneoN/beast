﻿using System;
using Beast.Users.DTOs.Users;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Users.UserUpdated
{
	public class UserUpdatedNotification : INotification
	{
		public IUserUpdate UserUpdate { get; }
		public IUserInfo UserInfo { get; }

		public UserUpdatedNotification(IUserUpdate userUpdate, IUserInfo userInfo)
		{
			UserUpdate = userUpdate ?? throw new ArgumentNullException(nameof(userUpdate));
			UserInfo = userInfo ?? throw new ArgumentNullException(nameof(userInfo));
		}
	}
}