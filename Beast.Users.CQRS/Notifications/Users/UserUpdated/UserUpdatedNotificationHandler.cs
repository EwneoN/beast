﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.Users.UserUpdated;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Users.UserUpdated
{
	public class UserUpdatedNotificationHandler : INotificationHandler<UserUpdatedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public UserUpdatedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(UserUpdatedNotification notification, CancellationToken cancellationToken)
		{
			if (notification.UserInfo == null)
			{
				return;
			}

			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new UserUpdatedMessage(messageId, clientId, _TimeService.UtcNow, notification.UserInfo);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}