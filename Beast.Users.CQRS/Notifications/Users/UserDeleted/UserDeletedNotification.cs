﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.Users.UserDeleted
{
	public class UserDeletedNotification : INotification
	{
		public long UserId { get; }

		public UserDeletedNotification(long userId)
		{
			UserId = userId;
		}
	}
}
