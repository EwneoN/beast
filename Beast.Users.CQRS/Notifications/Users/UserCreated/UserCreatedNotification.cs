﻿using System;
using Beast.Users.DTOs.Users;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Users.UserCreated
{
	public class UserCreatedNotification : INotification
	{
		public IUserInfo UserInfo { get; }

		public UserCreatedNotification(IUserInfo userInfo)
		{
			UserInfo = userInfo ?? throw new ArgumentNullException(nameof(userInfo));
		}
	}
}