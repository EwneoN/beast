﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.Users.UserCreated;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Users.UserCreated
{
	public class UserCreatedNotificationHandler : INotificationHandler<UserCreatedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public UserCreatedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(UserCreatedNotification notification, CancellationToken cancellationToken)
		{
			if (notification.UserInfo == null)
			{
				return;
			}

			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new UserCreatedMessage(messageId, clientId, _TimeService.UtcNow, notification.UserInfo);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}