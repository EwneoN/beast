﻿using System;
using Beast.Collections;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Users.UsersRead
{
	public class UsersReadNotification : INotification
	{
		public IPagedQueryParams QueryParams { get; }

		public UsersReadNotification(IPagedQueryParams queryParams)
		{
			QueryParams = queryParams ?? throw new ArgumentNullException(nameof(queryParams));
		}
	}
}