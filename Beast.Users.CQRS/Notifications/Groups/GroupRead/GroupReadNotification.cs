﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.Groups.GroupRead
{
	public class GroupReadNotification : INotification
	{
		public long GroupId { get; }

		public GroupReadNotification(long userId)
		{
			GroupId = userId;
		}
	}
}