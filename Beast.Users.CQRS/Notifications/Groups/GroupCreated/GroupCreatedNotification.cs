﻿using System;
using Beast.Users.DTOs.Groups;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Groups.GroupCreated
{
	public class GroupCreatedNotification : INotification
	{
		public IGroupInfo GroupInfo { get; }

		public GroupCreatedNotification(IGroupInfo groupInfo)
		{
			GroupInfo = groupInfo ?? throw new ArgumentNullException(nameof(groupInfo));
		}
	}
}