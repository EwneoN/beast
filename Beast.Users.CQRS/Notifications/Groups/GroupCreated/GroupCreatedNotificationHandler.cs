﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.Groups.GroupCreated;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Groups.GroupCreated
{
	public class GroupCreatedNotificationHandler : INotificationHandler<GroupCreatedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public GroupCreatedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(GroupCreatedNotification notification, CancellationToken cancellationToken)
		{
			if (notification.GroupInfo == null)
			{
				return;
			}

			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new GroupCreatedMessage(messageId, clientId, _TimeService.UtcNow, notification.GroupInfo);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}