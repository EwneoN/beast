﻿using MediatR;

namespace Beast.Users.CQRS.Notifications.Groups.GroupDeleted
{
	public class GroupDeletedNotification : INotification
	{
		public long GroupId { get; }

		public GroupDeletedNotification(long userId)
		{
			GroupId = userId;
		}
	}
}
