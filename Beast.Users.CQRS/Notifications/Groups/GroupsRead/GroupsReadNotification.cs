﻿using System;
using Beast.Collections;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Groups.GroupsRead
{
	public class GroupsReadNotification : INotification
	{
		public IPagedQueryParams QueryParams { get; }

		public GroupsReadNotification(IPagedQueryParams queryParams)
		{
			QueryParams = queryParams ?? throw new ArgumentNullException(nameof(queryParams));
		}
	}
}