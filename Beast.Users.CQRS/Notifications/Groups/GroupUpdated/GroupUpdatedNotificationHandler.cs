﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.EnterpriseBus.Clients;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.EnterpriseBus.Groups.GroupUpdated;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Groups.GroupUpdated
{
	public class GroupUpdatedNotificationHandler : INotificationHandler<GroupUpdatedNotification>
	{
		private readonly IIdService _IdService;
		private readonly ITimeService _TimeService;
		private readonly IEnterpriseBusPublisher _BusClient;

		public GroupUpdatedNotificationHandler(IIdService idService, ITimeService timeService,
		                                      IEnterpriseBusPublisher busClient)
		{
			_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
			_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
			_BusClient = busClient ?? throw new ArgumentNullException(nameof(busClient));
		}

		public async Task Handle(GroupUpdatedNotification notification, CancellationToken cancellationToken)
		{
			if (notification.GroupInfo == null)
			{
				return;
			}

			var messageId = await _IdService.GetNewId();
			var clientId = _BusClient.EnterpriseBusClientId;
			var message = new GroupUpdatedMessage(messageId, clientId, _TimeService.UtcNow, notification.GroupInfo);

			await _BusClient.PublishMessage(message, cancellationToken);
		}
	}
}