﻿using System;
using Beast.Users.DTOs.Groups;
using MediatR;

namespace Beast.Users.CQRS.Notifications.Groups.GroupUpdated
{
	public class GroupUpdatedNotification : INotification
	{
		public IGroupUpdate GroupUpdate { get; }
		public IGroupInfo GroupInfo { get; }

		public GroupUpdatedNotification(IGroupUpdate userUpdate, IGroupInfo userInfo)
		{
			GroupUpdate = userUpdate ?? throw new ArgumentNullException(nameof(userUpdate));
			GroupInfo = userInfo ?? throw new ArgumentNullException(nameof(userInfo));
		}
	}
}