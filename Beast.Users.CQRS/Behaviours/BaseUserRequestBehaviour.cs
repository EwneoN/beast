﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Behaviours;
using Beast.CQRS.Requests;
using Beast.Exceptions;
using Beast.Requests;
using Beast.Users.CQRS.Requests.Users;
using Beast.Users.Models;
using Beast.Users.DTOs.Users;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Behaviours
{
	public abstract class BaseUserRequestBehaviour<TRequest, TResponse> : IPipelineBehaviour<TRequest, TResponse>
		where TRequest : IUserRequest<TResponse>
		where TResponse : IResponse
	{
		private readonly IUsersService _UsersService;

		public IAccessContext Context { get; }
		public IUserInfo User { get; private set; }
		public string[] UserClaims { get; private set; }

		protected BaseUserRequestBehaviour(IAccessContext context, IUsersService usersService)
		{
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));
			Context = context ?? throw new ArgumentNullException(nameof(context));
		}

		public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken,
		                                       RequestHandlerDelegate<TResponse> next);

		protected async Task GetUser(long requestId, CancellationToken cancellationToken)
		{
			if (string.IsNullOrWhiteSpace(Context.AuthId))
			{
				return;
			}

			try
			{
				User = await _UsersService.GetUserByAuthId(Context.AuthId, cancellationToken);
			}
			catch (RecordNotFoundException)
			{
				throw new AuthenticationException(requestId);
			}

			var claims = User?.Claims?
			                 .Select(c => new { c.Module, c.Resource, c.Name })
			                 .GroupBy(c => c.Module)
			                 .ToDictionary(c => c.Key,
																					c => c.GroupBy(ic => ic.Resource)
																								.ToDictionary(ic => ic.Key, ic => ic.Select(iic => iic.Name)
																								.ToArray()));

			UserClaims = claims?.SelectMany(c => c.Value.SelectMany(v => v.Value.Select(s => $"{c}{Claim.Separator}{v}{Claim.Separator}{s}"))).ToArray();
		}
	}
}