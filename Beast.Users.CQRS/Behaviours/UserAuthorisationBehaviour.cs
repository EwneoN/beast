﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Exceptions;
using Beast.Requests;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Requests.Users;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Behaviours
{
	public class UserAuthorisationBehaviour<TRequest, TResponse> : BaseUserRequestBehaviour<TRequest, TResponse>
		where TRequest : IUserRequest<TResponse>
		where TResponse : IResponse
	{
		private readonly IUserRequestAuthoriser<TRequest, TResponse> _Authoriser;

		public UserAuthorisationBehaviour(IAccessContext context, IUsersService usersService, IUserRequestAuthoriser<TRequest, TResponse> authoriser)
			: base(context, usersService)
		{
			_Authoriser = authoriser;
		}

		public override async Task<TResponse>
			Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
		{
			if (!Context.IsAuthenticated)
			{
				throw new AuthenticationException(request.RequestId);
			}

			await GetUser(request.RequestId, cancellationToken);

			if (User != null)
			{
				request.CurrentUserId = User.UserId;
			}

			if (!_Authoriser.IsAuthorised(UserClaims))
			{
				string[] missingClaims = _Authoriser.GetRequiredClaims().Where(c => !UserClaims.Contains(c)).ToArray();
				throw new UnauthorisedException(request.RequestId, missingClaims);
			}

			return await next();
		}
	}
}