﻿using Beast.CQRS.Requests;
using Beast.Requests;
using Beast.Users.Auth;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Requests.Claims;
using Beast.Users.CQRS.Requests.Claims.CreateClaim;
using Beast.Users.CQRS.Requests.Claims.DeleteClaim;
using Beast.Users.CQRS.Requests.Claims.DoesClaimExistByName;
using Beast.Users.CQRS.Requests.Claims.GetAllClaims;
using Beast.Users.CQRS.Requests.Claims.GetClaim;
using Beast.Users.CQRS.Requests.Claims.GetClaimByName;
using Beast.Users.Services;

namespace Beast.Users.CQRS.Behaviours.Claims
{
	public class CreateClaimRequestAuthoriser: UserRequestAuthoriser<CreateClaimRequest, ClaimInfoResponse>
	{
		public CreateClaimRequestAuthoriser()
			: base(Resources.Claims, ClaimsResourceClaimsEnum.Create.ToString()) { }
	}

	public class GetClaimRequestAuthoriser: UserRequestAuthoriser<GetClaimRequest, ClaimInfoResponse>
	{
		public GetClaimRequestAuthoriser()
			: base(Resources.Claims, ClaimsResourceClaimsEnum.Read.ToString()) { }
	}

	public class GetAllClaimsRequestAuthoriser: UserRequestAuthoriser<GetAllClaimsRequest, GetAllClaimsResponse>
	{
		public GetAllClaimsRequestAuthoriser()
			: base(Resources.Claims, ClaimsResourceClaimsEnum.Read.ToString()) { }
	}

	public class GetClaimByNameRequestAuthoriser: UserRequestAuthoriser<GetClaimByNameRequest, ClaimInfoResponse>
	{
		public GetClaimByNameRequestAuthoriser()
			: base(Resources.Claims, ClaimsResourceClaimsEnum.Read.ToString()) { }
	}

	public class DoesClaimExistRequestAuthoriser: UserRequestAuthoriser<DoesClaimExistByNameRequest, ClaimExistsResponse>
	{
		public DoesClaimExistRequestAuthoriser()
			: base(Resources.Claims, ClaimsResourceClaimsEnum.Read.ToString()) { }
	}

	public class DoesClaimExistByNameRequestAuthoriser: UserRequestAuthoriser<DoesClaimExistByNameRequest, ClaimExistsResponse>
	{
		public DoesClaimExistByNameRequestAuthoriser()
			: base(Resources.Claims, ClaimsResourceClaimsEnum.Read.ToString()) { }
	}

	public class DeleteClaimRequestAuthoriser: UserRequestAuthoriser<DeleteClaimRequest, UnitResponse>
	{
		public DeleteClaimRequestAuthoriser()
			: base(Resources.Claims, ClaimsResourceClaimsEnum.Delete.ToString()) { }
	}
}