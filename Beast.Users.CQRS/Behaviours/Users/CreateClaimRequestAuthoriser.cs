﻿using Beast.CQRS.Requests;
using Beast.Requests;
using Beast.Users.Auth;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Requests.Users;
using Beast.Users.CQRS.Requests.Users.CreateNewUser;
using Beast.Users.CQRS.Requests.Users.DeleteUser;
using Beast.Users.CQRS.Requests.Users.DoesUserExist;
using Beast.Users.CQRS.Requests.Users.DoesUserExistByUsername;
using Beast.Users.CQRS.Requests.Users.GetUsers;
using Beast.Users.CQRS.Requests.Users.GetUser;
using Beast.Users.CQRS.Requests.Users.GetUserByUsername;
using Beast.Users.Services;

namespace Beast.Users.CQRS.Behaviours.Users
{
	public class CreateUserRequestAuthoriser: UserRequestAuthoriser<CreateNewUserRequest, UserInfoResponse>
	{
		public CreateUserRequestAuthoriser()
			: base(Resources.Users, UsersResourceClaimsEnum.Create.ToString()) { }
	}

	public class GetUserRequestAuthoriser: UserRequestAuthoriser<GetUserRequest, UserInfoResponse>
	{
		public GetUserRequestAuthoriser()
			: base(Resources.Users, UsersResourceClaimsEnum.Read.ToString()) { }
	}

	public class GetUsersRequestAuthoriser: UserRequestAuthoriser<GetUsersRequest, GetUsersResponse>
	{
		public GetUsersRequestAuthoriser()
			: base(Resources.Users, UsersResourceClaimsEnum.Read.ToString()) { }
	}

	public class GetUserByUsernameRequestAuthoriser: UserRequestAuthoriser<GetUserByUsernameRequest, UserInfoResponse>
	{
		public GetUserByUsernameRequestAuthoriser()
			: base(Resources.Users, UsersResourceClaimsEnum.Read.ToString()) { }
	}

	public class DoesUserExistRequestAuthoriser: UserRequestAuthoriser<DoesUserExistRequest, UserExistsResponse>
	{
		public DoesUserExistRequestAuthoriser()
			: base(Resources.Users, UsersResourceClaimsEnum.Read.ToString()) { }
	}

	public class DoesUserExistByUsernameRequestAuthoriser: UserRequestAuthoriser<DoesUserExistByUsernameRequest, UserExistsResponse>
	{
		public DoesUserExistByUsernameRequestAuthoriser()
			: base(Resources.Users, UsersResourceClaimsEnum.Read.ToString()) { }
	}

	public class DeleteUserRequestAuthoriser: UserRequestAuthoriser<DeleteUserRequest, UnitResponse>
	{
		public DeleteUserRequestAuthoriser()
			: base(Resources.Users, UsersResourceClaimsEnum.Delete.ToString()) { }
	}
}