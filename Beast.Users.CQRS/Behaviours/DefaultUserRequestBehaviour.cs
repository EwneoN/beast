﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Requests;
using Beast.Services.Ids;
using Beast.Services.Time;
using Beast.Users.CQRS.Requests.Users;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Behaviours
{
	//public class DefaultUserRequestBehaviour<TRequest, TResponse> : BaseUserRequestBehaviour<TRequest, TResponse>
	//	where TRequest : IUserRequest<TResponse> 
	//	where TResponse : IResponse
	//{
	//	private readonly IIdService _IdService;
	//	private readonly ITimeService _TimeService;

	//	public DefaultUserRequestBehaviour(IAccessContext accessContext, IUsersService usersService, IIdService idService, ITimeService timeService)
	//		: base(accessContext, usersService)
	//	{
	//		_IdService = idService ?? throw new ArgumentNullException(nameof(idService));
	//		_TimeService = timeService ?? throw new ArgumentNullException(nameof(timeService));
	//	}

	//	public override async Task<TResponse> 
	//		Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
	//	{
	//		await GetUser(request.RequestId, cancellationToken);

	//		request.RequestId = await _IdService.GetNewId();
	//		request.Received = _TimeService.UtcNow;

	//		return await next();
	//	}
	//}
}
