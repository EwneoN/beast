﻿using System.Linq;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Authorisers
{
	public abstract class UserRequestAuthoriser<TRequest, TResponse> 
		: IUserRequestAuthoriser<TRequest, TResponse>
		where TRequest : IUserRequest<TResponse>
		where TResponse : IResponse
	{
		private string[] _RequiredClaims { get; }

		protected UserRequestAuthoriser(params string[] requiredClaims)
		{
			_RequiredClaims = requiredClaims ?? new string[0];
		}

		public bool IsAuthorised(string[] userClaims)
		{
			return _RequiredClaims.Any() && userClaims == null || !_RequiredClaims.All(c => userClaims.Contains(c));
		}

		public string[] GetRequiredClaims()
		{ 
			string[] copiedClaims = new string[_RequiredClaims.Length];
			_RequiredClaims.CopyTo(copiedClaims, 0);
			return copiedClaims;
		}
	}
}