﻿using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Authorisers
{
	public interface IUserRequestAuthoriser<TRequest, TResponse>
		where TRequest : IUserRequest<TResponse>
		where TResponse : IResponse
	{
		bool IsAuthorised(string[] userClaims);

		string[] GetRequiredClaims();
	}
}
