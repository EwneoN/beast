﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.UserClaims.DoesUserClaimExist
{
	public class DoesUserClaimExistRequest : IUserRequest<UserClaimExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserClaimId { get; }

		public DoesUserClaimExistRequest(long userClaimId)
		{
			UserClaimId = userClaimId;
		}
	}
}
