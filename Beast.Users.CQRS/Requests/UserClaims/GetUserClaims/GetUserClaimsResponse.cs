﻿using System;
using System.Collections.Generic;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.CQRS.Requests.UserClaims.GetUserClaims
{
	public class GetUserClaimsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public ICollection<IUserClaimInfo> UserClaims { get; }

		public GetUserClaimsResponse(ICollection<IUserClaimInfo> claims)
		{
			UserClaims = claims ?? throw new ArgumentNullException(nameof(claims));
		}
	}
}