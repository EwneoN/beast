﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.UserClaims.UserClaimsRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.UserClaims.GetUserClaims
{
	public class GetUserClaimsRequestHandler : IRequestHandler<GetUserClaimsRequest, GetUserClaimsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUserClaimsService _UserClaimsService;

		public GetUserClaimsRequestHandler(IMediator mediator, IUserClaimsService userClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UserClaimsService = userClaimsService ?? throw new ArgumentNullException(nameof(userClaimsService));

			_UserClaimsService.UserClaimsRead += UserClaimsService_UserClaimsRead;
		}

		public async Task<GetUserClaimsResponse> Handle(GetUserClaimsRequest request, CancellationToken cancellationToken)
		{
			var result = await _UserClaimsService.GetUserClaims(request.UserId, cancellationToken);

			return new GetUserClaimsResponse(result);
		}

		private async Task UserClaimsService_UserClaimsRead(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserClaimsReadNotification(eventArgs.Request));
		}
	}
}