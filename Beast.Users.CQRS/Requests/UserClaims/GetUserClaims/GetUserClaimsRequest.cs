﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.UserClaims.GetUserClaims
{
	public class GetUserClaimsRequest : IUserRequest<GetUserClaimsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }

		public GetUserClaimsRequest(long userId)
		{
			UserId = userId;
		}
	}
}
