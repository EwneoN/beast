﻿using Beast.Users.CQRS.Requests.UserClaims.CreateNewUserClaim;
using Beast.Validation;
using Beast.Validation.CQRS.Validators;
using FluentValidation;

namespace Beast.Users.CQRS.Requests.UserClaims.CreateNewUserClaim
{
	public class CreateNewUserClaimRequestValidator : AbstractValidator<CreateNewUserClaimRequest>, IRequestValidator<CreateNewUserClaimRequest>
	{
		public CreateNewUserClaimRequestValidator()
		{
			RuleFor(r => r.NewUserClaim)
				.RequiredObject();
			RuleFor(r => r.NewUserClaim.UserId)
				.Required();
			RuleFor(r => r.NewUserClaim.ClaimId)
				.Required();
		}
	}
}
