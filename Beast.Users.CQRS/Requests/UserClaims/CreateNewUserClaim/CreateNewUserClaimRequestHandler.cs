﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.UserClaims.UserClaimCreated;
using Beast.Users.DTOs.UserClaims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.UserClaims.CreateNewUserClaim
{
	public class CreateNewUserClaimRequestHandler : IRequestHandler<CreateNewUserClaimRequest, UserClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUserClaimsService _UserClaimsService;

		public CreateNewUserClaimRequestHandler(IMediator mediator, IUserClaimsService userClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UserClaimsService = userClaimsService ?? throw new ArgumentNullException(nameof(userClaimsService));

			_UserClaimsService.UserClaimCreated += UserClaimsService_UserClaimCreated;
		}

		public async Task<UserClaimInfoResponse> Handle(CreateNewUserClaimRequest request, CancellationToken cancellationToken)
		{
			var result = await _UserClaimsService.CreateNewUserClaim(request.NewUserClaim, cancellationToken);

			return new UserClaimInfoResponse(result);
		}

		private async Task UserClaimsService_UserClaimCreated(object sender, BeastEventArgs<INewUserClaim, IUserClaimInfo> eventArgs)
		{
			await _Mediator.Publish(new UserClaimCreatedNotification(eventArgs.Response));
		}
	}
}