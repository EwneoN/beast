﻿using System;
using Beast.Users.CQRS.Requests.Users;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.CQRS.Requests.UserClaims.CreateNewUserClaim
{
	public class CreateNewUserClaimRequest : IUserRequest<UserClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public INewUserClaim NewUserClaim { get; }
		public long CurrentUserId { get; set; }

		public CreateNewUserClaimRequest(INewUserClaim newUserClaim)
		{
			NewUserClaim = newUserClaim ?? throw new ArgumentNullException(nameof(newUserClaim));
		}
	}
}