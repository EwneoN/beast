﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Notifications.UserClaims.UserClaimDeleted;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.UserClaims.DeleteUserClaim
{
	public class DeleteUserClaimRequestHandler : IRequestHandler<DeleteUserClaimRequest, UnitResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUserClaimsService _UserClaimsService;

		public DeleteUserClaimRequestHandler(IMediator mediator, IUserClaimsService userClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UserClaimsService = userClaimsService ?? throw new ArgumentNullException(nameof(userClaimsService));

			_UserClaimsService.UserClaimDeleted += UsersService_UserDeleted;
		}

		public async Task<UnitResponse> Handle(DeleteUserClaimRequest request, CancellationToken cancellationToken)
		{
			await _UserClaimsService.DeleteUserClaim(request.UserClaimId, cancellationToken);

			return new UnitResponse();
		}

		private async Task UsersService_UserDeleted(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserClaimDeletedNotification(eventArgs.Request));
		}
	}
}
