﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.UserClaims.DeleteUserClaim
{
	public class DeleteUserClaimRequest : IUserRequest<UnitResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserClaimId { get; }

		public DeleteUserClaimRequest(long userClaimId)
		{
			UserClaimId = userClaimId;
		}
	}
}
