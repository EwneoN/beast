﻿using Beast.Users.CQRS.Requests.UserClaims.DeleteUserClaim;
using Beast.Validation;
using Beast.Validation.CQRS.Validators;
using FluentValidation;

namespace Beast.Users.CQRS.Requests.UserClaims.DeleteUserClaim
{
	public class DeleteUserClaimRequestValidator : AbstractValidator<DeleteUserClaimRequest>,
	                                               IRequestValidator<DeleteUserClaimRequest>
	{
		public DeleteUserClaimRequestValidator()
		{
			RuleFor(r => r.UserClaimId)
				.Required();
		}
	}
}