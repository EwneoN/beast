﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.UserClaims;

namespace Beast.Users.CQRS.Requests.UserClaims
{
	public class UserClaimInfoResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IUserClaimInfo UserClaimInfo { get; }

		public UserClaimInfoResponse(IUserClaimInfo userClaimInfo)
		{
			UserClaimInfo = userClaimInfo ?? throw new ArgumentNullException(nameof(userClaimInfo));
		}
	}
}