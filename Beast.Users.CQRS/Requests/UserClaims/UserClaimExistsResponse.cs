﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.UserClaims
{
	public class UserClaimExistsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public bool DoesUserClaimExist { get; }

		public UserClaimExistsResponse(bool doesUserClaimExist)
		{
			DoesUserClaimExist = doesUserClaimExist;
		}
	}
}