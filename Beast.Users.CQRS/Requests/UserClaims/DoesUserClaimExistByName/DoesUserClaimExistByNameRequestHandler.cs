﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.UserClaims.UserClaimRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.UserClaims.DoesUserClaimExistByName
{
	public class DoesUserClaimExistByNameRequestHandler 
		: IRequestHandler<DoesUserClaimExistByNameRequest, UserClaimExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUserClaimsService _UserClaimsService;

		public DoesUserClaimExistByNameRequestHandler(IMediator mediator, IUserClaimsService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UserClaimsService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UserClaimsService.UserClaimRead += UserClaimsService_UserClaimRead;
		}

		public async Task<UserClaimExistsResponse> Handle(DoesUserClaimExistByNameRequest request,
		                                                  CancellationToken cancellationToken)
		{
			var result = await _UserClaimsService.DoesUserClaimExist(request.UserId, request.ClaimName, cancellationToken);

			return new UserClaimExistsResponse(result);
		}

		private async Task UserClaimsService_UserClaimRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserClaimReadNotification(eventArgs.Request));
		}
	}
}