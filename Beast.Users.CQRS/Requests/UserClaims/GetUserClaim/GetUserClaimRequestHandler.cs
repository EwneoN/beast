﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.UserClaims.UserClaimRead;
using Beast.Users.DTOs.UserClaims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.UserClaims.GetUserClaim
{
	public class GetUserClaimRequestHandler : IRequestHandler<GetUserClaimRequest, UserClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUserClaimsService _UserClaimsService;

		public GetUserClaimRequestHandler(IMediator mediator, IUserClaimsService userClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UserClaimsService = userClaimsService ?? throw new ArgumentNullException(nameof(userClaimsService));

			_UserClaimsService.UserClaimRead += UsersService_UserRead;
		}

		public async Task<UserClaimInfoResponse> Handle(GetUserClaimRequest request, CancellationToken cancellationToken)
		{
			IUserClaimInfo userInfo = await _UserClaimsService.GetUserClaim(request.UserClaimId, cancellationToken);

			return new UserClaimInfoResponse(userInfo);
		}

		private async Task UsersService_UserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserClaimReadNotification(eventArgs.Request));
		}
	}
}