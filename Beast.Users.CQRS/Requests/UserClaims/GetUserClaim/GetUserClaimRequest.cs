﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.UserClaims.GetUserClaim
{
	public class GetUserClaimRequest : IUserRequest<UserClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserClaimId { get; }

		public GetUserClaimRequest(long userClaimId)
		{
			UserClaimId = userClaimId;
		}
	}
}
