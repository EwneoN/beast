﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.UserClaims.UserClaimRead;
using Beast.Users.DTOs.UserClaims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.UserClaims.GetUserClaimByName
{
	public class GetUserClaimByNameRequestHandler : IRequestHandler<GetUserClaimByNameRequest, UserClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUserClaimsService _UserClaimsService;

		public GetUserClaimByNameRequestHandler(IMediator mediator, IUserClaimsService userClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UserClaimsService = userClaimsService ?? throw new ArgumentNullException(nameof(userClaimsService));

			_UserClaimsService.UserClaimRead += UsersService_UserRead;
		}

		public async Task<UserClaimInfoResponse> Handle(GetUserClaimByNameRequest request, CancellationToken cancellationToken)
		{
			IUserClaimInfo userInfo = await _UserClaimsService.GetUserClaimByName(request.UserId, request.ClaimName, cancellationToken);

			return new UserClaimInfoResponse(userInfo);
		}

		private async Task UsersService_UserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserClaimReadNotification(eventArgs.Request));
		}
	}
}
