﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.UserClaims.GetUserClaimByName
{
	public class GetUserClaimByNameRequest : IUserRequest<UserClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }
		public string ClaimName { get; }

		public GetUserClaimByNameRequest(long userId, string claimName)
		{
			if (string.IsNullOrWhiteSpace(claimName))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(claimName));
			}

			UserId = userId;
			ClaimName = claimName;
		}
	}
}
