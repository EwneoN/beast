﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.UserClaims.DoesUserClaimExistByIds
{
	public class DoesUserClaimExistByIdsRequest : IUserRequest<UserClaimExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }
		public long ClaimId { get; }

		public DoesUserClaimExistByIdsRequest(long userId, long claimId)
		{
			UserId = userId;
			ClaimId = claimId;
		}
	}
}
