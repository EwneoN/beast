﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupClaims.GetGroupClaim
{
	public class GetGroupClaimRequest : IUserRequest<GroupClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupClaimId { get; }

		public GetGroupClaimRequest(long userClaimId)
		{
			GroupClaimId = userClaimId;
		}
	}
}