﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimRead;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupClaims.GetGroupClaim
{
	public class GetGroupClaimRequestHandler : IRequestHandler<GetGroupClaimRequest, GroupClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupClaimsService _GroupClaimsService;

		public GetGroupClaimRequestHandler(IMediator mediator, IGroupClaimsService groupClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupClaimsService = groupClaimsService ?? throw new ArgumentNullException(nameof(groupClaimsService));

			_GroupClaimsService.GroupClaimRead += GroupsService_GroupRead;
		}

		public async Task<GroupClaimInfoResponse> Handle(GetGroupClaimRequest request, CancellationToken cancellationToken)
		{
			IGroupClaimInfo userInfo = await _GroupClaimsService.GetGroupClaim(request.GroupClaimId, cancellationToken);

			return new GroupClaimInfoResponse(userInfo);
		}

		private async Task GroupsService_GroupRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupClaimReadNotification(eventArgs.Request));
		}
	}
}