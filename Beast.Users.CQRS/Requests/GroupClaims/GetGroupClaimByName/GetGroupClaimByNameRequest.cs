﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupClaims.GetGroupClaimByName
{
	public class GetGroupClaimByNameRequest : IUserRequest<GroupClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupId { get; }
		public string ClaimName { get; }

		public GetGroupClaimByNameRequest(long groupId, string claimName)
		{
			if (string.IsNullOrWhiteSpace(claimName))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(claimName));
			}

			GroupId = groupId;
			ClaimName = claimName;
		}
	}
}
