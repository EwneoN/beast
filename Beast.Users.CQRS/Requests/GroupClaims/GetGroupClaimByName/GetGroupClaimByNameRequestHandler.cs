﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimRead;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupClaims.GetGroupClaimByName
{
	public class GetGroupClaimByNameRequestHandler : IRequestHandler<GetGroupClaimByNameRequest, GroupClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupClaimsService _GroupClaimsService;

		public GetGroupClaimByNameRequestHandler(IMediator mediator, IGroupClaimsService groupClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupClaimsService = groupClaimsService ?? throw new ArgumentNullException(nameof(groupClaimsService));

			_GroupClaimsService.GroupClaimRead += GroupsService_GroupRead;
		}

		public async Task<GroupClaimInfoResponse> Handle(GetGroupClaimByNameRequest request,
		                                                 CancellationToken cancellationToken)
		{
			IGroupClaimInfo userInfo =
				await _GroupClaimsService.GetGroupClaimByName(request.GroupId, request.ClaimName, cancellationToken);

			return new GroupClaimInfoResponse(userInfo);
		}

		private async Task GroupsService_GroupRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupClaimReadNotification(eventArgs.Request));
		}
	}
}