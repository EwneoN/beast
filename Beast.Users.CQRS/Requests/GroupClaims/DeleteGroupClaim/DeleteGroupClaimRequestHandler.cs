﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimDeleted;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupClaims.DeleteGroupClaim
{
	public class DeleteGroupClaimRequestHandler : IRequestHandler<DeleteGroupClaimRequest, UnitResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupClaimsService _GroupClaimsService;

		public DeleteGroupClaimRequestHandler(IMediator mediator, IGroupClaimsService groupClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupClaimsService = groupClaimsService ?? throw new ArgumentNullException(nameof(groupClaimsService));

			_GroupClaimsService.GroupClaimDeleted += GroupsService_GroupDeleted;
		}

		public async Task<UnitResponse> Handle(DeleteGroupClaimRequest request, CancellationToken cancellationToken)
		{
			await _GroupClaimsService.DeleteGroupClaim(request.GroupClaimId, cancellationToken);

			return new UnitResponse();
		}

		private async Task GroupsService_GroupDeleted(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupClaimDeletedNotification(eventArgs.Request));
		}
	}
}
