﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupClaims.DeleteGroupClaim
{
	public class DeleteGroupClaimRequest : IUserRequest<UnitResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupClaimId { get; }

		public DeleteGroupClaimRequest(long groupClaimId)
		{
			GroupClaimId = groupClaimId;
		}
	}
}