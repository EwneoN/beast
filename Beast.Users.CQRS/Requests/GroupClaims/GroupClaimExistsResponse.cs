﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.GroupClaims
{
	public class GroupClaimExistsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public bool DoesGroupClaimExist { get; }

		public GroupClaimExistsResponse(bool doesGroupClaimExist)
		{
			DoesGroupClaimExist = doesGroupClaimExist;
		}
	}
}