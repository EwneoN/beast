﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.CQRS.Requests.GroupClaims.CreateNewGroupClaim
{
	public class CreateNewGroupClaimRequest : IRequest<GroupClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public INewGroupClaim NewGroupClaim { get; }

		public CreateNewGroupClaimRequest(INewGroupClaim newGroupClaim)
		{
			NewGroupClaim = newGroupClaim ?? throw new ArgumentNullException(nameof(newGroupClaim));
		}
	}
}