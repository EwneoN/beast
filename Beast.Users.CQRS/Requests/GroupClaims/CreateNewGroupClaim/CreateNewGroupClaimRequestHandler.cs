﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimCreated;
using Beast.Users.DTOs.GroupClaims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupClaims.CreateNewGroupClaim
{
	public class CreateNewGroupClaimRequestHandler : IRequestHandler<CreateNewGroupClaimRequest, GroupClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupClaimsService _GroupClaimsService;

		public CreateNewGroupClaimRequestHandler(IMediator mediator, IGroupClaimsService groupClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupClaimsService = groupClaimsService ?? throw new ArgumentNullException(nameof(groupClaimsService));

			_GroupClaimsService.GroupClaimCreated += GroupClaimsService_GroupClaimCreated;
		}

		public async Task<GroupClaimInfoResponse> Handle(CreateNewGroupClaimRequest request,
		                                                 CancellationToken cancellationToken)
		{
			var result = await _GroupClaimsService.CreateNewGroupClaim(request.NewGroupClaim, cancellationToken);

			return new GroupClaimInfoResponse(result);
		}

		private async Task GroupClaimsService_GroupClaimCreated(object sender,
		                                                        BeastEventArgs<INewGroupClaim, IGroupClaimInfo> eventArgs)
		{
			await _Mediator.Publish(new GroupClaimCreatedNotification(eventArgs.Response));
		}
	}
}