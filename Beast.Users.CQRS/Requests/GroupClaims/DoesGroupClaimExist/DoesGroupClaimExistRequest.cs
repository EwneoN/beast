﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupClaims.DoesGroupClaimExist
{
	public class DoesGroupClaimExistRequest : IUserRequest<GroupClaimExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupClaimId { get; }

		public DoesGroupClaimExistRequest(long userClaimId)
		{
			GroupClaimId = userClaimId;
		}
	}
}
