﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupClaims.DoesGroupClaimExist
{
	public class DoesGroupClaimExistRequestHandler : IRequestHandler<DoesGroupClaimExistRequest, GroupClaimExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupClaimsService _GroupClaimsService;

		public DoesGroupClaimExistRequestHandler(IMediator mediator, IGroupClaimsService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupClaimsService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_GroupClaimsService.GroupClaimRead += GroupClaimsService_GroupClaimRead;
		}

		public async Task<GroupClaimExistsResponse> Handle(DoesGroupClaimExistRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupClaimsService.DoesGroupClaimExist(request.GroupClaimId, cancellationToken);

			return new GroupClaimExistsResponse(result);
		}

		private async Task GroupClaimsService_GroupClaimRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupClaimReadNotification(eventArgs.Request));
		}
	}
}