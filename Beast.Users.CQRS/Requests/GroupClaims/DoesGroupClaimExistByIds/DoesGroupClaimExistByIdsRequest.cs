﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupClaims.DoesGroupClaimExistByIds
{
	public class DoesGroupClaimExistByIdsRequest : IUserRequest<GroupClaimExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupId { get; }
		public long ClaimId { get; }

		public DoesGroupClaimExistByIdsRequest(long groupId, long claimId)
		{
			GroupId = groupId;
			ClaimId = claimId;
		}
	}
}
