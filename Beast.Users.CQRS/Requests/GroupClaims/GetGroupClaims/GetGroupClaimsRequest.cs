﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupClaims.GetGroupClaims
{
	public class GetGroupClaimsRequest : IUserRequest<GetGroupClaimsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupId { get; }

		public GetGroupClaimsRequest(long groupId)
		{
			GroupId = groupId;
		}
	}
}