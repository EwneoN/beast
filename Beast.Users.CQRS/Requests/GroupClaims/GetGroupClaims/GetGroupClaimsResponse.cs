﻿using System;
using System.Collections.Generic;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.CQRS.Requests.GroupClaims.GetGroupClaims
{
	public class GetGroupClaimsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public ICollection<IGroupClaimInfo> GroupClaims { get; }

		public GetGroupClaimsResponse(ICollection<IGroupClaimInfo> claims)
		{
			GroupClaims = claims ?? throw new ArgumentNullException(nameof(claims));
		}
	}
}