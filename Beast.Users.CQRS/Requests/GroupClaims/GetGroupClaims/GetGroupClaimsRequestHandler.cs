﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimsRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupClaims.GetGroupClaims
{
	public class GetGroupClaimsRequestHandler : IRequestHandler<GetGroupClaimsRequest, GetGroupClaimsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupClaimsService _GroupClaimsService;

		public GetGroupClaimsRequestHandler(IMediator mediator, IGroupClaimsService groupClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupClaimsService = groupClaimsService ?? throw new ArgumentNullException(nameof(groupClaimsService));

			_GroupClaimsService.GroupClaimsRead += GroupClaimsService_GroupClaimsRead;
		}

		public async Task<GetGroupClaimsResponse> Handle(GetGroupClaimsRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupClaimsService.GetGroupClaims(request.GroupId, cancellationToken);

			return new GetGroupClaimsResponse(result);
		}

		private async Task GroupClaimsService_GroupClaimsRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupClaimsReadNotification(eventArgs.Request));
		}
	}
}