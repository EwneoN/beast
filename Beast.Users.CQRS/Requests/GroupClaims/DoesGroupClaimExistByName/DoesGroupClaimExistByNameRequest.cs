﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupClaims.DoesGroupClaimExistByName
{
	public class DoesGroupClaimExistByNameRequest : IUserRequest<GroupClaimExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupId { get; }
		public string ClaimName { get; }

		public DoesGroupClaimExistByNameRequest(long groupId, string claimName)
		{
			if (string.IsNullOrWhiteSpace(claimName))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(claimName));
			}

			GroupId = groupId;
			ClaimName = claimName;
		}
	}
}