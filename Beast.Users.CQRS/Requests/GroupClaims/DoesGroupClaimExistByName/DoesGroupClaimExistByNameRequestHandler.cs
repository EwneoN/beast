﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupClaims.GroupClaimRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupClaims.DoesGroupClaimExistByName
{
	public class
		DoesGroupClaimExistByNameRequestHandler : IRequestHandler<DoesGroupClaimExistByNameRequest, GroupClaimExistsResponse
		>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupClaimsService _GroupClaimsService;

		public DoesGroupClaimExistByNameRequestHandler(IMediator mediator, IGroupClaimsService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupClaimsService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_GroupClaimsService.GroupClaimRead += GroupsService_GroupRead;
		}

		public async Task<GroupClaimExistsResponse> Handle(DoesGroupClaimExistByNameRequest request,
		                                                   CancellationToken cancellationToken)
		{
			var result = await _GroupClaimsService.DoesGroupClaimExist(request.GroupId, request.ClaimName, cancellationToken);

			return new GroupClaimExistsResponse(result);
		}

		private async Task GroupsService_GroupRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupClaimReadNotification(eventArgs.Request));
		}
	}
}