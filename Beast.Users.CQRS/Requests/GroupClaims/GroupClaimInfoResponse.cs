﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.GroupClaims;

namespace Beast.Users.CQRS.Requests.GroupClaims
{
	public class GroupClaimInfoResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IGroupClaimInfo GroupClaimInfo { get; }

		public GroupClaimInfoResponse(IGroupClaimInfo groupClaimInfo)
		{
			GroupClaimInfo = groupClaimInfo ?? throw new ArgumentNullException(nameof(groupClaimInfo));
		}
	}
}