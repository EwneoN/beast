﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupUsers.GetGroupUserByUsername
{
	public class GetGroupUserByUsernameRequest : IUserRequest<GroupUserInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupId { get; }
		public string Username { get; }

		public GetGroupUserByUsernameRequest(long groupId, string username)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
			}

			GroupId = groupId;
			Username = username;
		}
	}
}
