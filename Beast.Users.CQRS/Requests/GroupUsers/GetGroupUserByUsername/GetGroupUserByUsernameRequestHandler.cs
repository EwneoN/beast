﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupUsers.GroupUserRead;
using Beast.Users.DTOs.GroupUsers;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupUsers.GetGroupUserByUsername
{
	public class GetGroupUserByUsernameRequestHandler : IRequestHandler<GetGroupUserByUsernameRequest, GroupUserInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupUsersService _GroupUsersService;

		public GetGroupUserByUsernameRequestHandler(IMediator mediator, IGroupUsersService groupUsersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupUsersService = groupUsersService ?? throw new ArgumentNullException(nameof(groupUsersService));

			_GroupUsersService.GroupUserRead += GroupsService_GroupRead;
		}

		public async Task<GroupUserInfoResponse> Handle(GetGroupUserByUsernameRequest request, CancellationToken cancellationToken)
		{
			IGroupUserInfo userInfo = await _GroupUsersService.GetGroupUserByUsername(request.GroupId, request.Username, cancellationToken);

			return new GroupUserInfoResponse(userInfo);
		}

		private async Task GroupsService_GroupRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupUserReadNotification(eventArgs.Request));
		}
	}
}