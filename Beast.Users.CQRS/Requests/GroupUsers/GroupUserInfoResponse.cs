﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.GroupUsers;

namespace Beast.Users.CQRS.Requests.GroupUsers
{
	public class GroupUserInfoResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IGroupUserInfo GroupUserInfo { get; }

		public GroupUserInfoResponse(IGroupUserInfo groupUserInfo)
		{
			GroupUserInfo = groupUserInfo ?? throw new ArgumentNullException(nameof(groupUserInfo));
		}
	}
}