﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.GroupUsers
{
	public class GroupUserExistsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public bool DoesGroupClaimExist { get; }

		public GroupUserExistsResponse(bool doesGroupClaimExist)
		{
			DoesGroupClaimExist = doesGroupClaimExist;
		}
	}
}