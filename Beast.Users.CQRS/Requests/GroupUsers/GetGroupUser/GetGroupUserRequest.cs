﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupUsers.GetGroupUser
{
	public class GetGroupUserRequest : IUserRequest<GroupUserInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupUserId { get; }

		public GetGroupUserRequest(long userUserId)
		{
			GroupUserId = userUserId;
		}
	}
}
