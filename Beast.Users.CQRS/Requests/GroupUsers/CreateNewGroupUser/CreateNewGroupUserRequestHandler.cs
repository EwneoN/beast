﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupUsers.GroupUserCreated;
using Beast.Users.DTOs.GroupUsers;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupUsers.CreateNewGroupUser
{
	public class CreateNewGroupUserRequestHandler : IRequestHandler<CreateNewGroupUserRequest, GroupUserInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupUsersService _GroupUsersService;

		public CreateNewGroupUserRequestHandler(IMediator mediator, IGroupUsersService groupUsersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupUsersService = groupUsersService ?? throw new ArgumentNullException(nameof(groupUsersService));

			_GroupUsersService.GroupUserCreated += GroupUsersService_GroupUserCreated;
		}

		public async Task<GroupUserInfoResponse> Handle(CreateNewGroupUserRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupUsersService.CreateNewGroupUser(request.NewGroupUser, cancellationToken);

			return new GroupUserInfoResponse(result);
		}

		private async Task GroupUsersService_GroupUserCreated(object sender, BeastEventArgs<INewGroupUser, IGroupUserInfo> eventArgs)
		{
			await _Mediator.Publish(new GroupUserCreatedNotification(eventArgs.Response));
		}
	}
}