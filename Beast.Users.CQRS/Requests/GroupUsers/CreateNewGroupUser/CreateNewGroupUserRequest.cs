﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.GroupUsers;

namespace Beast.Users.CQRS.Requests.GroupUsers.CreateNewGroupUser
{
	public class CreateNewGroupUserRequest : IRequest<GroupUserInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public INewGroupUser NewGroupUser { get; }

		public CreateNewGroupUserRequest(INewGroupUser newGroupUser)
		{
			NewGroupUser = newGroupUser ?? throw new ArgumentNullException(nameof(newGroupUser));
		}
	}
}