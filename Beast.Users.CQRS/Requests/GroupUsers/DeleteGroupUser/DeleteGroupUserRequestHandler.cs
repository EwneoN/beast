﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Notifications.GroupUsers.GroupUserDeleted;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupUsers.DeleteGroupUser
{
	public class DeleteGroupUserRequestHandler : IRequestHandler<DeleteGroupUserRequest, UnitResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupUsersService _GroupUsersService;

		public DeleteGroupUserRequestHandler(IMediator mediator, IGroupUsersService groupUsersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupUsersService = groupUsersService ?? throw new ArgumentNullException(nameof(groupUsersService));

			_GroupUsersService.GroupUserDeleted += GroupsService_GroupDeleted;
		}

		public async Task<UnitResponse> Handle(DeleteGroupUserRequest request, CancellationToken cancellationToken)
		{
			await _GroupUsersService.DeleteGroupUser(request.GroupUserId, cancellationToken);

			return new UnitResponse();
		}

		private async Task GroupsService_GroupDeleted(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupUserDeletedNotification(eventArgs.Request));
		}
	}
}
