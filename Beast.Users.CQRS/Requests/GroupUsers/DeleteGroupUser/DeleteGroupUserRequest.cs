﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupUsers.DeleteGroupUser
{
	public class DeleteGroupUserRequest : IUserRequest<UnitResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupUserId { get; }

		public DeleteGroupUserRequest(long groupUserId)
		{
			GroupUserId = groupUserId;
		}
	}
}
