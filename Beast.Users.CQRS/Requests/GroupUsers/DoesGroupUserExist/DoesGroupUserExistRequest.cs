﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupUsers.DoesGroupUserExist
{
	public class DoesGroupUserExistRequest : IUserRequest<GroupUserExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupUserId { get; }

		public DoesGroupUserExistRequest(long userUserId)
		{
			GroupUserId = userUserId;
		}
	}
}
