﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupUsers.GroupUserRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupUsers.DoesGroupUserExist
{
	public class DoesGroupUserExistRequestHandler : IRequestHandler<DoesGroupUserExistRequest, GroupUserExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupUsersService _GroupUsersService;

		public DoesGroupUserExistRequestHandler(IMediator mediator, IGroupUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupUsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_GroupUsersService.GroupUserRead += GroupUsersService_GroupUserRead;
		}

		public async Task<GroupUserExistsResponse> Handle(DoesGroupUserExistRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupUsersService.DoesGroupUserExist(request.GroupUserId, cancellationToken);

			return new GroupUserExistsResponse(result);
		}

		private async Task GroupUsersService_GroupUserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupUserReadNotification(eventArgs.Request));
		}
	}
}