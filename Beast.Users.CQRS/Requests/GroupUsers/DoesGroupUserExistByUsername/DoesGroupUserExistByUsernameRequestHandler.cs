﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.GroupUsers.GroupUserRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupUsers.DoesGroupUserExistByUsername
{
	public class DoesGroupUserExistByUsernameRequestHandler 
		: IRequestHandler<DoesGroupUserExistByUsernameRequest, GroupUserExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupUsersService _GroupUsersService;

		public DoesGroupUserExistByUsernameRequestHandler(IMediator mediator, IGroupUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupUsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_GroupUsersService.GroupUserRead += GroupsService_GroupRead;
		}

		public async Task<GroupUserExistsResponse> Handle(DoesGroupUserExistByUsernameRequest request,
		                                                   CancellationToken cancellationToken)
		{
			var result = await _GroupUsersService.DoesGroupUserExist(request.GroupId, request.Username, cancellationToken);

			return new GroupUserExistsResponse(result);
		}

		private async Task GroupsService_GroupRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupUserReadNotification(eventArgs.Request));
		}
	}
}