﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.GroupUsers.DoesGroupUserExistByIds
{
	public class DoesGroupUserExistByIdsRequest : IUserRequest<GroupUserExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupId { get; }
		public long UserId { get; }

		public DoesGroupUserExistByIdsRequest(long groupId, long claimId)
		{
			GroupId = groupId;
			UserId = claimId;
		}
	}
}
