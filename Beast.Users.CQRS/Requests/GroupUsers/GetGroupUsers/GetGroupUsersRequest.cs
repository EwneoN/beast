﻿using System;
using Beast.Users.CQRS.Requests.Users;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.CQRS.Requests.GroupUsers.GetGroupUsers
{
	public class GetGroupUsersRequest : IUserRequest<GetGroupUsersResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public IGroupPagedQueryParams QueryParams { get; }

		public GetGroupUsersRequest(IGroupPagedQueryParams queryParams)
		{
			QueryParams = queryParams ?? throw new ArgumentNullException(nameof(queryParams));
		}
	}
}
