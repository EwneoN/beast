﻿using System;
using Beast.Collections;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.GroupUsers;

namespace Beast.Users.CQRS.Requests.GroupUsers.GetGroupUsers
{
	public class GetGroupUsersResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IPagedCollection<IGroupUserInfo> GroupUsers { get; }

		public GetGroupUsersResponse(IPagedCollection<IGroupUserInfo> claims)
		{
			GroupUsers = claims ?? throw new ArgumentNullException(nameof(claims));
		}
	}
}