﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.GroupUsers.GroupUsersRead;
using Beast.Users.DTOs.Groups;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.GroupUsers.GetGroupUsers
{
	public class GetGroupUsersRequestHandler : IRequestHandler<GetGroupUsersRequest, GetGroupUsersResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupUsersService _GroupUsersService;

		public GetGroupUsersRequestHandler(IMediator mediator, IGroupUsersService groupUsersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupUsersService = groupUsersService ?? throw new ArgumentNullException(nameof(groupUsersService));

			_GroupUsersService.GroupUsersRead += GroupUsersService_GroupUsersRead;
		}

		public async Task<GetGroupUsersResponse> Handle(GetGroupUsersRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupUsersService.GetGroupUsers(request.QueryParams, cancellationToken);

			return new GetGroupUsersResponse(result);
		}

		private async Task GroupUsersService_GroupUsersRead(object sender, Events.BeastEventArgs<IGroupPagedQueryParams> eventArgs)
		{
			await _Mediator.Publish(new GroupUsersReadNotification(eventArgs.Request));
		}
	}
}