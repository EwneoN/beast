﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Groups.DoesGroupExistByName
{
	public class DoesGroupExistByNameRequest : IUserRequest<GroupExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public string GroupName { get; }

		public DoesGroupExistByNameRequest(string groupName)
		{
			if (string.IsNullOrWhiteSpace(groupName))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(groupName));
			}

			GroupName = groupName;
		}
	}
}