﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Groups.GroupRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Groups.DoesGroupExistByName
{
	public class DoesGroupExistByNameRequestHandler : IRequestHandler<DoesGroupExistByNameRequest, GroupExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupsService _GroupsService;

		public DoesGroupExistByNameRequestHandler(IMediator mediator, IGroupsService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupsService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_GroupsService.GroupRead += GroupsService_GroupRead;
		}

		public async Task<GroupExistsResponse> Handle(DoesGroupExistByNameRequest request,
		                                              CancellationToken cancellationToken)
		{
			var result = await _GroupsService.DoesGroupExist(request.GroupName, cancellationToken);

			return new GroupExistsResponse(result);
		}

		private async Task GroupsService_GroupRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupReadNotification(eventArgs.Request));
		}
	}
}