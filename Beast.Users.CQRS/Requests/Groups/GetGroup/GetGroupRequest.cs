﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Groups.GetGroup
{
	public class GetGroupRequest : IRequest<GroupInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long GroupId { get; }

		public GetGroupRequest(long groupId)
		{
			GroupId = groupId;
		}
	}
}