﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Groups.GroupRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Groups.GetGroup
{
	public class GetGroupRequestHandler : IRequestHandler<GetGroupRequest, GroupInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupsService _GroupsService;

		public GetGroupRequestHandler(IMediator mediator, IGroupsService groupsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupsService = groupsService ?? throw new ArgumentNullException(nameof(groupsService));

			_GroupsService.GroupRead += GroupsService_GroupRead;
		}

		public async Task<GroupInfoResponse> Handle(GetGroupRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupsService.GetGroup(request.GroupId, cancellationToken);

			return new GroupInfoResponse(result);
		}

		private async Task GroupsService_GroupRead(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupReadNotification(eventArgs.Request));
		}
	}
}