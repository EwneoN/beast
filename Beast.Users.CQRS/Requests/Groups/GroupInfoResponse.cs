﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.CQRS.Requests.Groups
{
	public class GroupInfoResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IGroupInfo Group { get; }

		public GroupInfoResponse(IGroupInfo group)
		{
			Group = group ?? throw new ArgumentNullException(nameof(group));
		}
	}
}