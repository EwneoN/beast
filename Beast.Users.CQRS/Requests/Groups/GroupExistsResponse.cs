﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Groups
{
	public class GroupExistsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public bool DoesGroupExist { get; }

		public GroupExistsResponse(bool doesGroupExist)
		{
			DoesGroupExist = doesGroupExist;
		}
	}
}