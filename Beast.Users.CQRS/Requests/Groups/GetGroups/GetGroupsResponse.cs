﻿using Beast.Collections;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.CQRS.Requests.Groups.GetGroups
{
	public class GetGroupsResponse : PagedResponse<IGroupInfo>
	{
		public GetGroupsResponse(int pageNumber, int pageSize, int totalItems, int totalPages, IGroupInfo[] items) 
			: base(pageNumber, pageSize, totalItems, totalPages, items) { }

		public GetGroupsResponse(IPagedCollection<IGroupInfo> pagedCollection) : base(pagedCollection) { }
	}
}