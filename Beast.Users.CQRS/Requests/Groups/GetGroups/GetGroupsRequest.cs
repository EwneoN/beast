﻿using System;
using Beast.Collections;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Groups.GetGroups
{
	public class GetGroupsRequest : IPagedRequest<GetGroupsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IPagedQueryParams QueryParams { get; }

		public GetGroupsRequest(IPagedQueryParams queryParams)
		{
			QueryParams = queryParams ?? throw new ArgumentNullException(nameof(queryParams));
		}
	}
}