﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Groups.GroupsRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Groups.GetGroups
{
	public class GetGroupsRequestHandler : IRequestHandler<GetGroupsRequest, GetGroupsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupsService _GroupsService;

		public GetGroupsRequestHandler(IMediator mediator, IGroupsService groupsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupsService = groupsService ?? throw new ArgumentNullException(nameof(groupsService));

			_GroupsService.GroupsRead += GroupsService_GroupsRead;
		}

		public async Task<GetGroupsResponse> Handle(GetGroupsRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupsService.GetGroups(request.QueryParams, cancellationToken);

			return new GetGroupsResponse(result);
		}

		private async Task GroupsService_GroupsRead(object sender, Events.BeastEventArgs<Collections.IPagedQueryParams> eventArgs)
		{
			await _Mediator.Publish(new GroupsReadNotification(eventArgs.Request));
		}
	}
}