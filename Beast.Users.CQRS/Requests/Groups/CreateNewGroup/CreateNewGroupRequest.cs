﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.CQRS.Requests.Groups.CreateNewGroup
{
	public class CreateNewGroupRequest : IRequest<GroupInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public INewGroup NewGroup { get; }

		public CreateNewGroupRequest(INewGroup newGroup)
		{
			NewGroup = newGroup ?? throw new ArgumentNullException(nameof(newGroup));
		}
	}
}