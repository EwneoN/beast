﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Groups.GroupCreated;
using Beast.Users.DTOs.Groups;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Groups.CreateNewGroup
{
	public class CreateNewGroupRequestHandler : IRequestHandler<CreateNewGroupRequest, GroupInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupsService _GroupsService;

		public CreateNewGroupRequestHandler(IMediator mediator, IGroupsService groupsRepository)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupsService = groupsRepository ?? throw new ArgumentNullException(nameof(groupsRepository));

			_GroupsService.GroupCreated += GroupsService_GroupCreated;
		}

		public async Task<GroupInfoResponse> Handle(CreateNewGroupRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupsService.CreateNewGroup(request.NewGroup, cancellationToken);

			return new GroupInfoResponse(result);
		}

		private async Task GroupsService_GroupCreated(object sender, Events.BeastEventArgs<INewGroup, IGroupInfo> eventArgs)
		{
			await _Mediator.Publish(new GroupCreatedNotification(eventArgs.Response));
		}
	}
}