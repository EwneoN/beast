﻿using Beast.Users.CQRS.Requests.Groups.CreateNewGroup;
using Beast.Validation;
using Beast.Validation.CQRS.Validators;
using FluentValidation;

namespace Beast.Users.CQRS.Requests.Groups.CreateNewGroup
{
	public class CreateNewGroupRequestValidator : AbstractValidator<CreateNewGroupRequest>, IRequestValidator<CreateNewGroupRequest>
	{
		public CreateNewGroupRequestValidator()
		{
			RuleFor(r => r.NewGroup)
				.RequiredObject();
			RuleFor(r => r.NewGroup.Name)
				.Required();
		}

		//todo add parentId check
	}
}
