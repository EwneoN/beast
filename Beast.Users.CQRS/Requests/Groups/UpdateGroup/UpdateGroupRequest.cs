﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Groups;

namespace Beast.Users.CQRS.Requests.Groups.UpdateGroup
{
	public class UpdateGroupRequest : IRequest<GroupInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IGroupUpdate GroupUpdate { get; }

		public UpdateGroupRequest(IGroupUpdate userUpdate)
		{
			GroupUpdate = userUpdate ?? throw new ArgumentNullException(nameof(userUpdate));
		}
	}
}
