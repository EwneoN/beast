﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Groups.GroupUpdated;
using Beast.Users.DTOs.Groups;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Groups.UpdateGroup
{
	public class UpdateGroupRequestHandler : IRequestHandler<UpdateGroupRequest, GroupInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupsService _GroupsService;

		public UpdateGroupRequestHandler(IMediator mediator, IGroupsService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupsService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_GroupsService.GroupUpdated += GroupsService_GroupUpdated;
		}

		public async Task<GroupInfoResponse> Handle(UpdateGroupRequest request, CancellationToken cancellationToken)
		{
			IGroupInfo userInfo = await _GroupsService.UpdateGroup(request.GroupUpdate, cancellationToken);

			return new GroupInfoResponse(userInfo);
		}

		private async Task GroupsService_GroupUpdated(object sender, BeastEventArgs<IGroupUpdate, IGroupInfo> eventArgs)
		{
			await _Mediator.Publish(new GroupUpdatedNotification(eventArgs.Request, eventArgs.Response));
		}
	}
}