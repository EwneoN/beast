﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Groups.GetGroupByName
{
	public class GetGroupByNameRequest : IRequest<GroupInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public string Name { get; }

		public GetGroupByNameRequest(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));
			}

			Name = name;
		}
	}
}