﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Groups.GroupRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Groups.GetGroupByName
{
	public class GetGroupByNameRequestHandler : IRequestHandler<GetGroupByNameRequest, GroupInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupsService _GroupsService;

		public GetGroupByNameRequestHandler(IMediator mediator, IGroupsService groupsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupsService = groupsService ?? throw new ArgumentNullException(nameof(groupsService));

			_GroupsService.GroupRead += GroupsService_GroupRead;
		}

		public async Task<GroupInfoResponse> Handle(GetGroupByNameRequest request, CancellationToken cancellationToken)
		{
			var result = await _GroupsService.GetGroupByName(request.Name, cancellationToken);

			return new GroupInfoResponse(result);
		}

		private async Task GroupsService_GroupRead(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupReadNotification(eventArgs.Request));
		}
	}
}