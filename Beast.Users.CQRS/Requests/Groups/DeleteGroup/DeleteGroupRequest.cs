﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Groups.DeleteGroup
{
	public class DeleteGroupRequest : IRequest<UnitResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long GroupId { get; }

		public DeleteGroupRequest(long groupId)
		{
			GroupId = groupId;
		}
	}
}
