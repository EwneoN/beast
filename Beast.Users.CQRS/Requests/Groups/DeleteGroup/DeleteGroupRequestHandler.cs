﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Notifications.Groups.GroupDeleted;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Groups.DeleteGroup
{
	public class DeleteGroupRequestHandler : IRequestHandler<DeleteGroupRequest, UnitResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IGroupsService _GroupsService;

		public DeleteGroupRequestHandler(IMediator mediator, IGroupsService groupsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_GroupsService = groupsService ?? throw new ArgumentNullException(nameof(groupsService));

			_GroupsService.GroupDeleted += GroupsService_GroupDeleted;
		}

		public async Task<UnitResponse> Handle(DeleteGroupRequest request, CancellationToken cancellationToken)
		{
			await _GroupsService.DeleteGroup(request.GroupId, cancellationToken);

			return new UnitResponse();
		}

		private async Task GroupsService_GroupDeleted(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new GroupDeletedNotification(eventArgs.Request));
		}
	}
}