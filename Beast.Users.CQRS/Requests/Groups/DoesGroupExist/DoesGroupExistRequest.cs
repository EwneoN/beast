﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Groups.DoesGroupExist
{
	public class DoesGroupExistRequest : IUserRequest<GroupExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long GroupId { get; }

		public DoesGroupExistRequest(long groupId)
		{
			GroupId = groupId;
		}
	}
}
