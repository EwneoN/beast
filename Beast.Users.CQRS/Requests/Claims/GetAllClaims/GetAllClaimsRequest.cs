﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Claims.GetAllClaims
{
	public class GetAllClaimsRequest : IUserRequest<GetAllClaimsResponse>
	{
		public long RequestId { get; set; }
		public long CurrentUserId { get; set; }
		public DateTimeOffset Received { get; set; }
	}
}
