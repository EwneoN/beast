﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Claims.ClaimsRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Claims.GetAllClaims
{
	public class GetAllClaimsRequestHandler : IRequestHandler<GetAllClaimsRequest, GetAllClaimsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IClaimsService _ClaimsService;

		public GetAllClaimsRequestHandler(IMediator mediator, IClaimsService claimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_ClaimsService = claimsService ?? throw new ArgumentNullException(nameof(claimsService));

			_ClaimsService.ClaimsRead += ClaimsService_ClaimsRead;
		}

		public async Task<GetAllClaimsResponse> Handle(GetAllClaimsRequest request, CancellationToken cancellationToken)
		{
			var claims = await _ClaimsService.GetAllClaims(cancellationToken);

			return new GetAllClaimsResponse(claims);
		}

		private async Task ClaimsService_ClaimsRead(object sender, Events.BeastEventArgs eventArgs)
		{
			await _Mediator.Publish(new ClaimsReadNotification());
		}
	}
}