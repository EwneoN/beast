﻿using System;
using System.Collections.Generic;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Claims;

namespace Beast.Users.CQRS.Requests.Claims.GetAllClaims
{
	public class GetAllClaimsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }

		public ICollection<IClaimInfo> Claims { get; }

		public GetAllClaimsResponse(ICollection<IClaimInfo> claims)
		{
			Claims = claims ?? throw new ArgumentNullException(nameof(claims));
		}
	}
}