﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Claims.GetClaim
{
	public class GetClaimRequest : IUserRequest<ClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public long CurrentUserId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long ClaimId { get; }

		public GetClaimRequest(long claimId)
		{
			ClaimId = claimId;
		}
	}
}
