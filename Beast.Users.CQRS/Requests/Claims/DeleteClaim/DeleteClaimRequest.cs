﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Claims.DeleteClaim
{
	public class DeleteClaimRequest : IUserRequest<UnitResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long ClaimId { get; }

		public DeleteClaimRequest(long claimId)
		{
			ClaimId = claimId;
		}
	}
}