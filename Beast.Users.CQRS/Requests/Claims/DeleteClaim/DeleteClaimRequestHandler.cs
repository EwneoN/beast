﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Notifications.Claims.ClaimDeleted;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Claims.DeleteClaim
{
	public class DeleteClaimRequestHandler : IRequestHandler<DeleteClaimRequest, UnitResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IClaimsService _ClaimsService;

		public DeleteClaimRequestHandler(IMediator mediator, IClaimsService claimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_ClaimsService = claimsService ?? throw new ArgumentNullException(nameof(claimsService));

			_ClaimsService.ClaimDeleted += ClaimsService_ClaimDeleted;
		}

		public async Task<UnitResponse> Handle(DeleteClaimRequest request, CancellationToken cancellationToken)
		{
			await _ClaimsService.DeleteClaim(request.ClaimId, cancellationToken);

			return new UnitResponse();
		}

		private async Task ClaimsService_ClaimDeleted(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new ClaimDeletedNotification(eventArgs.Request));
		}
	}
}
