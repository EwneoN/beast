﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Claims;

namespace Beast.Users.CQRS.Requests.Claims
{
	public class ClaimInfoResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IClaimInfo ClaimInfo { get; }

		public ClaimInfoResponse(IClaimInfo claimInfo)
		{
			ClaimInfo = claimInfo ?? throw new ArgumentNullException(nameof(claimInfo));
		}
	}
}