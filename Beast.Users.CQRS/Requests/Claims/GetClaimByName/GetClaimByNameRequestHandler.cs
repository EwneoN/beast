﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Claims.ClaimRead;
using Beast.Users.DTOs.Claims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Claims.GetClaimByName
{
	public class GetClaimByNameRequestHandler : IRequestHandler<GetClaimByNameRequest, ClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IClaimsService _ClaimsService;

		public GetClaimByNameRequestHandler(IMediator mediator, IClaimsService userClaimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_ClaimsService = userClaimsService ?? throw new ArgumentNullException(nameof(userClaimsService));

			_ClaimsService.ClaimRead += ClaimsService_ClaimRead;
		}

		public async Task<ClaimInfoResponse> Handle(GetClaimByNameRequest request, CancellationToken cancellationToken)
		{
			IClaimInfo userInfo = await _ClaimsService.GetClaimByName(request.ClaimName, cancellationToken);

			return new ClaimInfoResponse(userInfo);
		}

		private async Task ClaimsService_ClaimRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new ClaimReadNotification(eventArgs.Request));
		}
	}
}
