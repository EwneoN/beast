﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Claims.GetClaimByName
{
	public class GetClaimByNameRequest : IUserRequest<ClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public long CurrentUserId { get; set; }
		public DateTimeOffset Received { get; set; }
		public string ClaimName { get; }

		public GetClaimByNameRequest(string claimName)
		{
			ClaimName = claimName;
		}
	}
}
