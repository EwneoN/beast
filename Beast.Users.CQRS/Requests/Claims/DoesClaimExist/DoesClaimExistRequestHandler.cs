﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Claims.ClaimRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Claims.DoesClaimExist
{
	public class DoesClaimExistRequestHandler : IRequestHandler<DoesClaimExistRequest, ClaimExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IClaimsService _ClaimsService;

		public DoesClaimExistRequestHandler(IMediator mediator, IClaimsService claimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_ClaimsService = claimsService ?? throw new ArgumentNullException(nameof(claimsService));

			_ClaimsService.ClaimRead += ClaimsService_ClaimRead;
		}

		public async Task<ClaimExistsResponse> Handle(DoesClaimExistRequest request, CancellationToken cancellationToken)
		{
			var result = await _ClaimsService.DoesClaimExist(request.ClaimId, cancellationToken);

			return new ClaimExistsResponse(result);
		}

		private async Task ClaimsService_ClaimRead(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new ClaimReadNotification(eventArgs.Request));
		}
	}
}
