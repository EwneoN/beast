﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Claims.DoesClaimExist
{
	public class DoesClaimExistRequest : IUserRequest<ClaimExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long ClaimId { get; }

		public DoesClaimExistRequest(long claimId)
		{
			ClaimId = claimId;
		}
	}
}