﻿using System;
using Beast.Users.CQRS.Requests.Users;
using Beast.Users.DTOs.Claims;

namespace Beast.Users.CQRS.Requests.Claims.CreateClaim
{
	public class CreateClaimRequest : IUserRequest<ClaimInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public INewClaim NewClaim { get; }

		public CreateClaimRequest(INewClaim newClaim)
		{
			NewClaim = newClaim ?? throw new ArgumentNullException(nameof(newClaim));
		}
	}
}