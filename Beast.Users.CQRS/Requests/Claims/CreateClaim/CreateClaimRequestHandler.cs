﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Claims.ClaimCreated;
using Beast.Users.DTOs.Claims;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Claims.CreateClaim
{
	public class CreateClaimRequestHandler : IRequestHandler<CreateClaimRequest, ClaimInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IClaimsService _ClaimsService;

		public CreateClaimRequestHandler(IMediator mediator, IClaimsService claimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_ClaimsService = claimsService ?? throw new ArgumentNullException(nameof(claimsService));

			_ClaimsService.ClaimCreated += ClaimsService_ClaimDeleted;
		}

		public async Task<ClaimInfoResponse> Handle(CreateClaimRequest request, CancellationToken cancellationToken)
		{
			var result = await _ClaimsService.CreateNewClaim(request.NewClaim, cancellationToken);

			return new ClaimInfoResponse(result);
		}

		private async Task ClaimsService_ClaimDeleted(object sender, BeastEventArgs<INewClaim, IClaimInfo> eventArgs)
		{
			await _Mediator.Publish(new ClaimCreatedNotification(eventArgs.Response));
		}
	}
}
