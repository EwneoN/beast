﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Claims.ClaimRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Claims.DoesClaimExistByName
{
	public class DoesClaimExistByNameRequestHandler : IRequestHandler<DoesClaimExistByNameRequest, ClaimExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IClaimsService _ClaimsService;

		public DoesClaimExistByNameRequestHandler(IMediator mediator, IClaimsService claimsService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_ClaimsService = claimsService ?? throw new ArgumentNullException(nameof(claimsService));

			_ClaimsService.ClaimRead += ClaimsService_ClaimRead;
		}

		public async Task<ClaimExistsResponse> Handle(DoesClaimExistByNameRequest request, CancellationToken cancellationToken)
		{
			var result = await _ClaimsService.DoesClaimExist(request.ClaimName, cancellationToken);

			return new ClaimExistsResponse(result);
		}

		private async Task ClaimsService_ClaimRead(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new ClaimReadNotification(eventArgs.Request));
		}
	}
}
