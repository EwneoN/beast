﻿using System;
using Beast.Users.CQRS.Requests.Users;

namespace Beast.Users.CQRS.Requests.Claims.DoesClaimExistByName
{
	public class DoesClaimExistByNameRequest : IUserRequest<ClaimExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public string ClaimName { get; }

		public DoesClaimExistByNameRequest(string claimName)
		{
			ClaimName = claimName;
		}
	}
}