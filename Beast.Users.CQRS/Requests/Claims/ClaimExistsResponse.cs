﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Claims
{
	public class ClaimExistsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public bool DoesClaimExist { get; }

		public ClaimExistsResponse(bool doesClaimExist)
		{
			DoesClaimExist = doesClaimExist;
		}
	}
}