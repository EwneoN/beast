﻿using System;
using Beast.Users.DTOs.Users;

namespace Beast.Users.CQRS.Requests.Users.SaveSignedUpUser
{
	public class SaveSignedUpUserRequest : IUserRequest<UserInfoResponse>
	{
		public long RequestId { get; set; }
		public long CurrentUserId { get; set; }
		public DateTimeOffset Received { get; set; }
		public ISignedUpUser NewUser { get; }

		public SaveSignedUpUserRequest(ISignedUpUser newUser)
		{
			NewUser = newUser ?? throw new ArgumentNullException(nameof(newUser));
		}
	}
}
