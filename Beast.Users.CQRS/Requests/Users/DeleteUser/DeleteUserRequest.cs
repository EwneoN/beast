﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Users.DeleteUser
{
	public class DeleteUserRequest : IUserRequest<UnitResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }

		public DeleteUserRequest(long userId)
		{
			UserId = userId;
		}
	}
}
