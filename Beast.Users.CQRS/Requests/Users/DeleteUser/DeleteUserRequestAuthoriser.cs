﻿using System;
using System.Collections.Generic;
using System.Text;
using Beast.CQRS.Requests;
using Beast.Requests;
using Beast.Users.Auth;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Behaviours;
using Beast.Users.Services;
using Beast.Users.Utils;

namespace Beast.Users.CQRS.Requests.Users.DeleteUser
{
	public class DeleteUserRequestAuthoriser : UserRequestAuthoriser<DeleteUserRequest, UnitResponse>
	{
		public DeleteUserRequestAuthoriser()
			: base(UsersResourceClaimsEnum.Delete.ToClaimString())
		{
		}
	}
}