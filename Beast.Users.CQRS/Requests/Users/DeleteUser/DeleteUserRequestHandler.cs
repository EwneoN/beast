﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.CQRS.Requests;
using Beast.Users.CQRS.Notifications.Users.UserDeleted;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.DeleteUser
{
	public class DeleteUserRequestHandler : IRequestHandler<DeleteUserRequest, UnitResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public DeleteUserRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserDeleted += UsersService_UserDeleted;
		}

		public async Task<UnitResponse> Handle(DeleteUserRequest request, CancellationToken cancellationToken)
		{
			await _UsersService.DeleteUser(request.UserId, cancellationToken);

			return new UnitResponse();
		}

		private async Task UsersService_UserDeleted(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserDeletedNotification(eventArgs.Request));
		}
	}
}
