﻿using Beast.Users.CQRS.Requests.Users.DeleteUser;
using Beast.Validation;
using Beast.Validation.CQRS.Validators;
using FluentValidation;

namespace Beast.Users.CQRS.Requests.Users.DeleteUser
{
	public class DeleteUserRequestValidator : AbstractValidator<DeleteUserRequest>, IRequestValidator<DeleteUserRequest>
	{
		public DeleteUserRequestValidator()
		{
			RuleFor(r => r.UserId)
				.Required();
		}
	}
}