﻿using System;

namespace Beast.Users.CQRS.Requests.Users.DoesUserExist
{
	public class DoesUserExistRequest : IUserRequest<UserExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }

		public DoesUserExistRequest(long userId)
		{
			UserId = userId;
		}
	}
}
