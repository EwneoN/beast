﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Users.UserRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.DoesUserExist
{
	public class DoesUserExistRequestHandler : IRequestHandler<DoesUserExistRequest, UserExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public DoesUserExistRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserRead += UsersService_UserRead;
		}

		public async Task<UserExistsResponse> Handle(DoesUserExistRequest request, CancellationToken cancellationToken)
		{
			var result = await _UsersService.DoesUserExist(request.UserId, cancellationToken);

			return new UserExistsResponse(result);
		}

		private async Task UsersService_UserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserReadNotification(eventArgs.Request));
		}
	}
}