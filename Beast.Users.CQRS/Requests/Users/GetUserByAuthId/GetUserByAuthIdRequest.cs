﻿using System;

namespace Beast.Users.CQRS.Requests.Users.GetUserByAuthId
{
	public class GetUserByAuthIdRequest : IUserRequest<UserInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public string AuthId { get; }

		public GetUserByAuthIdRequest(string authId)
		{
			AuthId = authId;
		}
	}
}
