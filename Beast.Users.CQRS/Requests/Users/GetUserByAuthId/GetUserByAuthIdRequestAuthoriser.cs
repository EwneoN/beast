﻿using Beast.Requests;
using Beast.Users.Auth;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Behaviours;
using Beast.Users.Services;
using Beast.Users.Utils;

namespace Beast.Users.CQRS.Requests.Users.GetUserByAuthId
{
	public class GetUserByAuthIdRequestAuthoriser: UserRequestAuthoriser<GetUserByAuthIdRequest, UserInfoResponse>
	{
		public GetUserByAuthIdRequestAuthoriser() : base(UsersResourceClaimsEnum.Read.ToClaimString())
		{
		}
	}
}
