﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Users
{
	public class UserExistsResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public bool DoesUserExist { get; }

		public UserExistsResponse(bool doesUserExist)
		{
			DoesUserExist = doesUserExist;
		}
	}
}