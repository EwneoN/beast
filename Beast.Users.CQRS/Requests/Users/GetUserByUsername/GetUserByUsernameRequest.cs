﻿using System;

namespace Beast.Users.CQRS.Requests.Users.GetUserByUsername
{
	public class GetUserByUsernameRequest : IUserRequest<UserInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public string Username { get; }

		public GetUserByUsernameRequest(string username)
		{
			Username = username;
		}
	}
}
