﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Users.UserRead;
using Beast.Users.DTOs.Users;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.GetUserByUsername
{
	public class GetUserByUsernameRequestHandler : IRequestHandler<GetUserByUsernameRequest, UserInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public GetUserByUsernameRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserRead += UsersService_UserRead;
		}

		public async Task<UserInfoResponse> Handle(GetUserByUsernameRequest request, CancellationToken cancellationToken)
		{
			IUserInfo userInfo = await _UsersService.GetUserByUsername(request.Username, cancellationToken);

			return new UserInfoResponse(userInfo);
		}

		private async Task UsersService_UserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserReadNotification(eventArgs.Request));
		}
	}
}
