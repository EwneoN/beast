﻿using Beast.Requests;
using Beast.Users.Auth;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Behaviours;
using Beast.Users.Services;
using Beast.Users.Utils;

namespace Beast.Users.CQRS.Requests.Users.DoesUserExistByUsername
{
	public class
		DoesUserExistByUsernameRequestAuthoriser: UserRequestAuthoriser<DoesUserExistByUsernameRequest,
			UserExistsResponse>
	{
		public DoesUserExistByUsernameRequestAuthoriser()
			: base(UsersResourceClaimsEnum.Read.ToClaimString())
		{
		}
	}
}