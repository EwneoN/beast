﻿using System;

namespace Beast.Users.CQRS.Requests.Users.DoesUserExistByUsername
{
	public class DoesUserExistByUsernameRequest : IUserRequest<UserExistsResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public string Username { get; }

		public DoesUserExistByUsernameRequest(string username)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
			}

			Username = username;
		}
	}
}
