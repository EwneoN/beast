﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Users.CQRS.Notifications.Users.UserRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.DoesUserExistByUsername
{
	public class DoesUserExistByUsernameRequestHandler : IRequestHandler<DoesUserExistByUsernameRequest, UserExistsResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public DoesUserExistByUsernameRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserRead += UsersService_UserRead;
		}

		public async Task<UserExistsResponse> Handle(DoesUserExistByUsernameRequest request, CancellationToken cancellationToken)
		{
			var result = await _UsersService.DoesUserExist(request.Username, cancellationToken);

			return new UserExistsResponse(result);
		}

		private async Task UsersService_UserRead(object sender, Events.BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserReadNotification(eventArgs.Request));
		}
	}
}
