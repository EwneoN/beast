﻿using Beast.Collections;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Users.GetUsers
{
	public class GetUsersRequest : PagedRequest<GetUsersResponse>, IUserRequest<GetUsersResponse>
	{
		public long CurrentUserId { get; set; }

		public GetUsersRequest(IPagedQueryParams queryParams) : base(queryParams) { }
	}
}
