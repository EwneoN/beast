﻿using Beast.Collections;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Users;

namespace Beast.Users.CQRS.Requests.Users.GetUsers
{
	public class GetUsersResponse : PagedResponse<IUserInfo>
	{
		public GetUsersResponse(int pageNumber, int pageSize, int totalItems, int totalPages, IUserInfo[] items) 
			: base(pageNumber, pageSize, totalItems, totalPages, items) { }

		public GetUsersResponse(IPagedCollection<IUserInfo> pagedCollection) : base(pagedCollection) { }
	}
}