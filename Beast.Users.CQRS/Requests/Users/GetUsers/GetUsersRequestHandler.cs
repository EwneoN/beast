﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Collections;
using Beast.Users.CQRS.Notifications.Users.UsersRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.GetUsers
{
	public class GetUsersRequestHandler : IRequestHandler<GetUsersRequest, GetUsersResponse>
	{
		private readonly IUsersService _UsersService;
		private readonly IMediator _Mediator;

		public GetUsersRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));

			_UsersService.UsersRead += UsersService_UsersRead;
		}
		
		public async Task<GetUsersResponse> Handle(GetUsersRequest request, CancellationToken cancellationToken)
		{
			var users = await _UsersService.GetUsers(request.QueryParams, cancellationToken);

			return new GetUsersResponse(users);
		}

		private async Task UsersService_UsersRead(object sender, Events.BeastEventArgs<IPagedQueryParams> eventArgs)
		{
			await _Mediator.Publish(new UsersReadNotification(eventArgs.Request));
		}
	}
}