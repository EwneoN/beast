﻿using System;
using Beast.Users.DTOs.Users;

namespace Beast.Users.CQRS.Requests.Users.CreateNewUser
{
	public class CreateNewUserRequest : IUserRequest<UserInfoResponse>
	{
		public long RequestId { get; set; }
		public long CurrentUserId { get; set; }
		public DateTimeOffset Received { get; set; }
		public INewUser NewUser { get; }

		public CreateNewUserRequest(INewUser newUser)
		{
			NewUser = newUser ?? throw new ArgumentNullException(nameof(newUser));
		}
	}
}
