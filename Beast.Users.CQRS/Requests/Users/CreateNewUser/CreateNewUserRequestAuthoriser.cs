﻿using Beast.Users.Auth;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.Utils;

namespace Beast.Users.CQRS.Requests.Users.CreateNewUser
{
	public class CreateNewUserRequestAuthoriser : UserRequestAuthoriser<CreateNewUserRequest, UserInfoResponse>
	{
		public CreateNewUserRequestAuthoriser() : base(UsersResourceClaimsEnum.Create.ToClaimString()) { }
	}
}