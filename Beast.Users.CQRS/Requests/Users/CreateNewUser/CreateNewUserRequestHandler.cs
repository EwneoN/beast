﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Users.UserCreated;
using Beast.Users.DTOs.Users;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.CreateNewUser
{
	public class CreateNewUserRequestHandler : IRequestHandler<CreateNewUserRequest, UserInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public CreateNewUserRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserCreated += UsersService_UserCreated;
		}

		public async Task<UserInfoResponse> Handle(CreateNewUserRequest request, CancellationToken cancellationToken)
		{
			IUserInfo userInfo = await _UsersService.CreateNewUser(request.NewUser, cancellationToken);

			return new UserInfoResponse(userInfo);
		}

		private async Task UsersService_UserCreated(object sender, BeastEventArgs<INewUser, IUserInfo> eventArgs)
		{
			await _Mediator.Publish(new UserCreatedNotification(eventArgs.Response));
		}
	}
}