﻿using Beast.Users.CQRS.Requests.Users.CreateNewUser;
using Beast.Validation;
using FluentValidation;

namespace Beast.Users.CQRS.Requests.Users.CreateNewUser
{
	public class CreateNewUserRequestValidator : AbstractValidator<CreateNewUserRequest>, 
	                                             Validation.CQRS.Validators.IRequestValidator<CreateNewUserRequest>
	{
		public CreateNewUserRequestValidator()
		{
			RuleFor(r => r.NewUser)
				.RequiredObject();
			RuleFor(r => r.NewUser.Username)
				.RequiredEmailAddress();
			RuleFor(r => r.NewUser.EmailAddress)
				.RequiredEmailAddress();
			RuleFor(r => r.NewUser.DisplayName)
				.Required();
			RuleFor(r => r.NewUser.FirstName)
				.Required();
			RuleFor(r => r.NewUser.MiddleName)
				.Required();
			RuleFor(r => r.NewUser.LastName)
				.Required();
			RuleFor(r => r.NewUser.Password)
				.Required();
			RuleFor(r => r.NewUser.DateOfBirth)
				.Required();
		}
	}
}
