﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Users.UserRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.GetUsername
{
	public class GetUsernameRequestHandler : IRequestHandler<GetUsernameRequest, GetUsernameResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public GetUsernameRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserRead += UsersService_UserRead;
		}

		public async Task<GetUsernameResponse> Handle(GetUsernameRequest request, CancellationToken cancellationToken)
		{
			string username = await _UsersService.GetUsername(request.UserId, cancellationToken);

			return new GetUsernameResponse(username);
		}

		private async Task UsersService_UserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserReadNotification(eventArgs.Request));
		}
	}
}