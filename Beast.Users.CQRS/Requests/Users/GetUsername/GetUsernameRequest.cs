﻿using System;

namespace Beast.Users.CQRS.Requests.Users.GetUsername
{
	public class GetUsernameRequest : IUserRequest<GetUsernameResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }

		public GetUsernameRequest(long userId)
		{
			UserId = userId;
		}
	}
}
