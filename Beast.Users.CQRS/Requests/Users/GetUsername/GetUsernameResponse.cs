﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Users.GetUsername
{
	public class GetUsernameResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public string Username { get; }

		public GetUsernameResponse(string username)
		{
			Username = username;
		}
	}
}