﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Users;

namespace Beast.Users.CQRS.Requests.Users
{
	public class UserInfoResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }

		public IUserInfo UserInfo { get; }

		public UserInfoResponse(IUserInfo userInfo)
		{
			UserInfo = userInfo;
		}
	}
}