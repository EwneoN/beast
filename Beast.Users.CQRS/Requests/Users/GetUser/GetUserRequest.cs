﻿using System;

namespace Beast.Users.CQRS.Requests.Users.GetUser
{
	public class GetUserRequest : IUserRequest<UserInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }

		public GetUserRequest(long userId)
		{
			UserId = userId;
		}
	}
}
