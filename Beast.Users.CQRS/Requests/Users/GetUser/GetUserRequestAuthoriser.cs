﻿using System;
using System.Collections.Generic;
using System.Text;
using Beast.Requests;
using Beast.Users.Auth;
using Beast.Users.CQRS.Authorisers;
using Beast.Users.CQRS.Behaviours;
using Beast.Users.Services;
using Beast.Users.Utils;

namespace Beast.Users.CQRS.Requests.Users.GetUser
{
	public class GetUserRequestAuthoriser: UserRequestAuthoriser<GetUserRequest, UserInfoResponse>
	{
		public GetUserRequestAuthoriser() : base(UsersResourceClaimsEnum.Read.ToClaimString())
		{
		}
	}
}
