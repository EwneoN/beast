﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Users.GetUserAuthId {
	public class GetUserAuthIdResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public string AuthId { get; }

		public GetUserAuthIdResponse(string authId)
		{
			AuthId = authId;
		}
	}
}