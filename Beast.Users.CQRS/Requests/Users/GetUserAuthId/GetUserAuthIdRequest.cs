﻿using System;

namespace Beast.Users.CQRS.Requests.Users.GetUserAuthId
{
	public class GetUserAuthIdRequest : IUserRequest<GetUserAuthIdResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public long UserId { get; }

		public GetUserAuthIdRequest(long userId)
		{
			UserId = userId;
		}
	}
}
