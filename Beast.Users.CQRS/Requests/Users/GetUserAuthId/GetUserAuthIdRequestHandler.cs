﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Users.UserRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.GetUserAuthId
{
	public class GetUserAuthIdRequestHandler : IRequestHandler<GetUserAuthIdRequest, GetUserAuthIdResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public GetUserAuthIdRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserRead += UsersService_UserRead;
		}

		public async Task<GetUserAuthIdResponse> Handle(GetUserAuthIdRequest request, CancellationToken cancellationToken)
		{
			string authId = await _UsersService.GetUserAuthId(request.UserId, cancellationToken);

			return new GetUserAuthIdResponse(authId);
		}

		private async Task UsersService_UserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserReadNotification(eventArgs.Request));
		}
	}
}