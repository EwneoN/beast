﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Users.UserRead;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.GetUserId
{
	public class GetUserIdRequestHandler : IRequestHandler<GetUserIdRequest, GetUserIdResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public GetUserIdRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserRead += UsersService_UserRead;
		}

		public async Task<GetUserIdResponse> Handle(GetUserIdRequest request, CancellationToken cancellationToken)
		{
			long userId = await _UsersService.GetUserId(request.Username, cancellationToken);

			return new GetUserIdResponse(userId);
		}

		private async Task UsersService_UserRead(object sender, BeastEventArgs<long> eventArgs)
		{
			await _Mediator.Publish(new UserReadNotification(eventArgs.Request));
		}
	}
}