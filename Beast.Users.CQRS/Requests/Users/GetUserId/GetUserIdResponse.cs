﻿using System;
using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Users.GetUserId
{
	public class GetUserIdResponse : IResponse
	{
		public long RequestId { get; set; }
		public long ResponseId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long UserId { get; }

		public GetUserIdResponse(long userId)
		{
			UserId = userId;
		}
	}
}