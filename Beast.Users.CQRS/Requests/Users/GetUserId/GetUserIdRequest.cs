﻿using System;

namespace Beast.Users.CQRS.Requests.Users.GetUserId
{
	public class GetUserIdRequest : IUserRequest<GetUserIdResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public long CurrentUserId { get; set; }
		public string Username { get; }

		public GetUserIdRequest(string username)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
			}

			Username = username;
		}
	}
}
