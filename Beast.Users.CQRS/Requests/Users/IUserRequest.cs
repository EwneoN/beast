﻿using Beast.CQRS.Requests;

namespace Beast.Users.CQRS.Requests.Users
{
	public interface IUserRequest<out TResponse> : IRequest<TResponse> 
		where TResponse : IResponse
	{
		long CurrentUserId { get; set; }
	}
}
