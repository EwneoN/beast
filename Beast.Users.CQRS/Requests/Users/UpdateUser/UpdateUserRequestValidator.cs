﻿using Beast.Users.CQRS.Requests.Users.UpdateUser;
using Beast.Validation;
using FluentValidation;

namespace Beast.Users.CQRS.Requests.Users.UpdateUser
{
	public class UpdateUserRequestValidator : AbstractValidator<UpdateUserRequest>, Validation.CQRS.Validators.IRequestValidator<UpdateUserRequest>
	{
		public UpdateUserRequestValidator()
		{
			RuleFor(r => r.UserUpdate)
				.RequiredObject();
			RuleFor(r => r.UserUpdate.UserId)
				.Required();
			RuleFor(r => r.UserUpdate.Username)
				.RequiredEmailAddress();
			RuleFor(r => r.UserUpdate.EmailAddress)
				.RequiredEmailAddress();
			RuleFor(r => r.UserUpdate.DisplayName)
				.Required();
			RuleFor(r => r.UserUpdate.FirstName)
				.Required();
			RuleFor(r => r.UserUpdate.MiddleName)
				.Required();
			RuleFor(r => r.UserUpdate.LastName)
				.Required();
			RuleFor(r => r.UserUpdate.DateOfBirth)
				.Required();
		}
	}
}
