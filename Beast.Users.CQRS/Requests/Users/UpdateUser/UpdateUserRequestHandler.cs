﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Beast.Events;
using Beast.Users.CQRS.Notifications.Users.UserUpdated;
using Beast.Users.DTOs.Users;
using Beast.Users.Services;
using MediatR;

namespace Beast.Users.CQRS.Requests.Users.UpdateUser
{
	public class UpdateUserRequestHandler : IRequestHandler<UpdateUserRequest, UserInfoResponse>
	{
		private readonly IMediator _Mediator;
		private readonly IUsersService _UsersService;

		public UpdateUserRequestHandler(IMediator mediator, IUsersService usersService)
		{
			_Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
			_UsersService = usersService ?? throw new ArgumentNullException(nameof(usersService));

			_UsersService.UserUpdated += UsersService_UserUpdated;
		}

		public async Task<UserInfoResponse> Handle(UpdateUserRequest request, CancellationToken cancellationToken)
		{
			IUserInfo userInfo = await _UsersService.UpdateUser(request.UserUpdate, cancellationToken);

			return new UserInfoResponse(userInfo);
		}

		private async Task UsersService_UserUpdated(object sender, BeastEventArgs<IUserUpdate, IUserInfo> eventArgs)
		{
			await _Mediator.Publish(new UserUpdatedNotification(eventArgs.Request, eventArgs.Response));
		}
	}
}