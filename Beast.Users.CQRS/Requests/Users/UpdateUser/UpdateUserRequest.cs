﻿using System;
using Beast.CQRS.Requests;
using Beast.Users.DTOs.Users;

namespace Beast.Users.CQRS.Requests.Users.UpdateUser
{
	public class UpdateUserRequest : IUserRequest<UserInfoResponse>
	{
		public long RequestId { get; set; }
		public DateTimeOffset Received { get; set; }
		public IUserUpdate UserUpdate { get; }
		public long CurrentUserId { get; set; }

		public UpdateUserRequest(IUserUpdate userUpdate)
		{
			UserUpdate = userUpdate ?? throw new ArgumentNullException(nameof(userUpdate));
		}
	}
}
